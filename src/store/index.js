import {createStore, applyMiddleware} from "redux";
import {persistStore, persistReducer} from "redux-persist";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import {composeWithDevTools} from "redux-devtools-extension";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import rootReducer from "../reducers";
import createFilter from 'redux-persist-transform-filter';


export const initialState = {
    auth: {
        alertEmailConfirmation: false,
        register: {
            data: {
                email: "",
                password: "",
                confirmPassword: "",
                nickname: "",
                agreement: false
            },
            errors: {
                email: null,
                nickname: null,
                password: null,
                confirmPassword: null,
                agreement: null
            }
        },
        login: {
            data: {
                email: "",
                password: ""
            },
            errors: {
                email: null,
                password: null,
                blocked: false,
                emailNotVerified: false
            }
        }
    },
    account: {
        id: null,
        firstName: "",
        lastName: "",
        email: "",
        nickname: "",
        city: "",
        country: "",
        planet: "",
        token: null,
        avatar: "",
        background: "",
    },
    user: {
        id: null,
        firstName: "",
        lastName: "",
        email: "",
        nickname: "",
        city: "",
        country: "",
        planet: "",
        token: null,
        avatar: "",
        background: "",
        coords: {
            data: {
                latitude: 0,
                longitude: 0
            },
            isSend: false,
            isDeniedPermission: null,
            isPresent: false
        },
        followers: [
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/1",
                name: "Follower Folowerenko"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/2",
                name: "User Userenko"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/3",
                name: "Friend Friendenko"
            }
        ],
        following: [
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/1",
                name: "SOme guy!!"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/2",
                name: "Hell Yeah!!"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/3",
                name: "That's me"
            }
        ],
        post: {
            posts: [],
            loadingAllPosts: true,
            loadingAddPosts: false,
            isLoadedPosts: false
        },
        new: {
            news: [],
            loadingNews: true,
            isLoaded: false
        },
        top: {
            top_news: [],
            loadingTopNews: true,
            isLoadedTopNews: false
        },
        liked: {
            liked_news: [],
            loadingLikedNews: true,
            isLoadedLikedNews: false
        },
        likedFollowing: {
            liked_following_news: [],
            loadingLikedFollowingNews: true,
            isLoadedLikedFollowingNews: false
        },
        friendsNews: {
            news: [],
            isLoaded: false,
            loadingNews: true
        },
        groups: [
            {
                name: "name1",
                id: 1,
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/4",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto beatae ea obcaecati quos recusandae repudiandae?"
            }
        ]
    },

    user_info: {
        id: null,
        firstName: "",
        lastName: "",
        email: "",
        nickname: "",
        city: "",
        country: "",
        planet: "",
        token: null,
        avatar: "",
        background: "",
        followers: [
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/1",
                name: "Follower Folowerenko"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/2",
                name: "User Userenko"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/3",
                name: "Friend Friendenko"
            }
        ],
        following: [
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/1",
                name: "SOme guy!!"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/2",
                name: "Hell Yeah!!"
            },
            {
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/3",
                name: "That's me"
            }
        ],
        post: {
            posts: [],
            loadingAllPosts: true,
            loadingAddPosts: false,
        },
        new: {
            news: [],
            loadingNews: true,
            isLoaded: false
        },
        groups: [
            {
                name: "name1",
                id: 1,
                img: "https://www.certmetrics.com/api/ob/image/amazon/c/4",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto beatae ea obcaecati quos recusandae repudiandae?"
            }
        ]
    },

    newGroup: {
        name: "",
        nickname: "",
        description: "",
        creatorId: 0,
        avatar_url: "",
        background_url: ""
    },

    follower: {
        firstName: "",
        lastName: "",
        nickname: ""
    },

  group: {
    name: "",
    nickname: "",
    description: "",
    creatorId: 0,
    avatar_url: "",
    background_url: "",
    isSubscribed: false,
    isCreator: false,
    members: [],
    posts: []
  },

  groupsLoading: false,

  groups: [],

  groupsPage: {
    groups: []
  },

  followed: {
    clicked: false
  },

    chat: {
        userId: null,
        conversations: [],
        messages: [],
        friends: [],
        channels: [],
        loadingConversations: true,
        loadingMessages: true,
        loadingFriends: true,
        loadingChannels: true,
        messageLabel: "",
        conversationNameLabel: "",
        channelNameLabel: "",
        searchFriendsLabel: "",
        showModal: false,
        currentChannelId: 4
    },
    dontRemoveThis: null
};

const middleware = [thunk];

const saveSubsetFilter = createFilter('root', [
    'user.id',
    'user.name',
    'user.email',
    'user.token',
    'user.firstName',
    'user.city',
    'user.country',
    'user.planet',
    'user.avatar',
    'user.background',
    'chat.userId'
]);

const persistConfig = {
    key: "root",
    storage: storage,
    stateReconciler: autoMergeLevel2,
    whitelist: ["user", "chat"],
    transforms: [saveSubsetFilter]
};

const pReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
    pReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
);

export const persistor = persistStore(store);

//export default store;
