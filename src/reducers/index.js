import {
    ADD_USER_REGISTER_DATA,
    ADD_USER_LOGIN_DATA,
    ADD_AUTIFICATED_USER_DATA,
    ADD_USER_DATA,
    ADD_USER_REGISTER_ERRORS,
    ADD_USER_LOGIN_ERRORS,
    CHANGE_ALERT_EMAIL_CONFIRMATION,
    ADD_POST_COMMENTS,
    REINIT_STORE,
    ADD_GROUP,
    FETCH_GROUP,
    EDIT_GROUP,
    SPINNER_FOR_GROUPS,
    ADD_NEWS,
    FETCH_TOP_NEWS,
    FETCH_LIKED_NEWS,
    FETCH_LIKED_FOLLOWING_NEWS,
    NEWS_TOP_LOADED,
    NEWS_LIKED_LOADED,
    NEWS_LIKED_FOLLOWING_LOADED,
    CLEAR_TOP_NEWS,
    CLEAR_LIKED_NEWS,
    CLEAR_LIKED_FOLLOWING_NEWS,
    ADD_TOP_NEWS,
    ADD_LIKED_NEWS,
    ADD_LIKED_FOLLOWING_NEWS,
    POSTS_LOADED,
    CLEAR_POSTS,
    ADD_POSTS,
    FETCH_GROUPS,
    FETCH_FOLLOWERS,
    ADD_NEWS_COMMENTS,
    CHANGE_GROUP_IS_SUBSCRIBED,
    CHANGE_GROUP_IS_CREATOR,

    FETCH_POSTS,
    FETCH_NEWS,

    ADD_POST,

    EDIT_POST,
    EDIT_TOP,
    EDIT_LIKED,
    EDIT_LIKED_FOLLOWING,
    EDIT_NEWS,

    DELETE_POST,
    DELETE_TOP,
    DELETE_LIKED,
    DELETE_LIKED_FOLLOWING,
    DELETE_NEWS,

    UPDATE_POST,
    UPDATE_NEWS,
    UPDATE_TOP,
    UPDATE_LIKED,
    UPDATE_LIKED_FOLLOWING,

    CLEAR_NEWS,

    NEWS_LOADED,
    LOADED_POST,
    CHANGE_USER_LOGIN_BLOCKED,
    GET_LOCATION, GET_LOCATION_PERMISSION_DENIED,
    UPDATE_USER_IS_ONLINE_AND_LOCATION

} from "../constants/actionTypes";
import {initialState} from "../store";

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case REINIT_STORE:
            return Object.assign({}, initialState, {});
        case ADD_USER_REGISTER_DATA:
            return Object.assign({}, state, {
                auth: {
                    login: Object.assign({}, state.auth.login, {}),
                    register: {
                        data: action.payload,
                        errors: Object.assign({}, state.auth.register.errors, {})
                    }
                }
            });
        case ADD_USER_LOGIN_DATA:
            return Object.assign({}, state, {
                auth: {
                    login: {
                        data: action.payload,
                        errors: Object.assign({}, state.auth.login.errors)
                    },
                    register: Object.assign({}, state.auth.register, {})
                }
            });
        case ADD_USER_LOGIN_ERRORS:
            return Object.assign({}, state, {
                auth: {
                    login: {
                        data: Object.assign({}, state.auth.login.data),
                        errors: {
                            ...state.auth.login.errors,
                            ...action.payload
                        }
                    },
                    register: Object.assign({}, state.auth.register, {})
                }
            });
        case CHANGE_USER_LOGIN_BLOCKED:
            return Object.assign({}, state, {
                auth: {
                    login: {
                        data: Object.assign({}, state.auth.login.data),
                        errors: {
                            ...state.auth.login.errors,
                            blocked: action.payload
                        }
                    },
                    register: Object.assign({}, state.auth.register, {})
                }
            });
        case ADD_AUTIFICATED_USER_DATA:
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    ...action.payload
                },
                chat: {
                    ...state.chat,
                    userId: action.payload.id
                }
            });
        case ADD_USER_DATA:
            return Object.assign({}, state, {
                user_info: {
                    ...state.user_info,
                    ...action.payload,
                },
            });
        case ADD_POST_COMMENTS:
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.map(post => {
                            if (post.id === action.payload.postId) {
                                // const newPost = Object.assign({}, post, {});
                                // newPost.comments = action.payload.comments;
                                // return newPost;
                                post.comments = action.payload.comments;
                            }
                            return post;
                        })
                    }
                }
            });
        case ADD_NEWS_COMMENTS:
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: state.user.new.news.map(post => {
                            if (post.id === action.payload.postId) {
                                // const newPost = Object.assign({}, post, {});
                                // newPost.comments = action.payload.comments;
                                // return newPost;
                                post.comments = action.payload.comments;
                            }
                            return post;
                        })
                    }
                }
            });
        case ADD_USER_REGISTER_ERRORS:
            return Object.assign({}, state, {
                auth: {
                    login: Object.assign({}, state.auth.login, {}),
                    register: {
                        data: Object.assign({}, state.auth.register.data, {}),
                        errors: action.payload
                    }
                }
            });

        case FETCH_GROUPS:
            return {
                ...state,
                groupsPage: action.payload
            };

        case ADD_GROUP:
            return {
                ...state,
                group: action.payload,
                groupsPage: {
                    ...state.groupsPage,
                    groups: [action.payload].concat(state.groupsPage.groups)

                }
            };

        case EDIT_GROUP:
            return {
                ...state,
                group: {
                    ...state.group,
                    ...action.payload
                }
            };

        case FETCH_GROUP:
            return Object.assign({}, state, {
                group: {
                    ...state.group,
                    ...action.payload
                },
            });
       /*     return {
                ...state,

                group: {
                    ...state.group,
                    ...action.payload
                }
            };*/

        case SPINNER_FOR_GROUPS:
            return {
                ...state,
                groupsLoading: action.payload

            };

        case FETCH_FOLLOWERS:
            return {
                ...state,

                group: {
                    ...state.group,
                    members: action.payload
                },




            };

        case CHANGE_GROUP_IS_SUBSCRIBED:

            return {
                ...state,
                group: {
                    ...state.group,
                    isSubscribed: action.payload
                }
            };

        case CHANGE_GROUP_IS_CREATOR:

            return {
                ...state,
                group: {
                    ...state.group,
                    isCreator: action.payload
                }
            };

        case CHANGE_ALERT_EMAIL_CONFIRMATION:
            return Object.assign({}, state, {
                alertEmailConfirmation: action.payload
            });
        case "FETCH_CONVERSATIONS_REQUEST":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    conversations: [],
                    loadingConversations: true
                }
            };
        case "FETCH_CONVERSATIONS_SUCCESS":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    conversations: action.payload,
                    loadingConversations: false
                }
            };
        case "FETCH_MESSAGES_REQUEST":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [],
                    loadingMessages: true
                }
            };
        case "FETCH_MESSAGES_SUCCESS":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: action.payload,
                    loadingMessages: false
                }
            };
        case "FETCH_CHANNELS_REQUEST":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    channels: [],
                    loadingChannels: true
                }
            };
        case "FETCH_CHANNELS_SUCCESS":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    channels: action.payload,
                    loadingChannels: false,
                    currentChannelId: action.payload[0].id
                }
            };
        case "FETCH_FRIENDS_REQUEST":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    friends: [],
                    loadingFriends: true
                }
            };
        case "FETCH_FRIENDS_SUCCESS":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    friends: action.payload,
                    loadingFriends: false
                }
            };
        case "MESSAGE_ADDED":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...state.chat.messages, action.payload]
                }
            };
        case "MESSAGE_INPUT_CHANGE":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messageLabel: action.payload
                }
            };
        case "CONVERSATION_NAME_INPUT_CHANGE":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    conversationNameLabel: action.payload
                }
            };
        case "SEARCH_FRIENDS_INPUT_CHANGE":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    searchFriendsLabel: action.payload
                }
            };
        case "CHANNEL_NAME_INPUT_CHANGE":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    channelNameLabel: action.payload
                }
            };

        case "MESSAGE_INPUT_CLEAR":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messageLabel: ""
                }
            };
        case "SHOW_MODAL":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    showModal: true
                }
            };
        case "HIDE_MODAL":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    showModal: false
                }
            };
        case "CURRENT_CHANNEL_ID_CHANGE":
            return {
                ...state,
                chat: {
                    ...state.chat,
                    currentChannelId: action.payload
                }
            };
        case "TOGGLE_CHECK_FRIEND":
            const changedFriends = state.chat.friends.map(friend =>
                friend.id === action.payload.id
                    ? {...friend, checked: !friend.checked}
                    : friend
            );
            return {
                ...state,
                chat: {
                    ...state.chat,
                    friends: changedFriends
                }
            };
        /**********posts******************/

        case FETCH_POSTS:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: action.payload,
                        loadingAllPosts: false
                    }
                }
            };
        case FETCH_TOP_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: action.payload,
                        loadingTopNews: false
                    }
                }
            };
            case FETCH_LIKED_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    liked: {
                        ...state.user.liked,
                        liked_news: action.payload,
                        loadingLikedNews: false
                    }
                }
            };
            case FETCH_LIKED_FOLLOWING_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    likedFollowing: {
                        ...state.user.likedFollowing,
                        liked_following_news: action.payload,
                        loadingLikedFollowingNews: false
                    }
                }
            };
        case FETCH_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: action.payload,
                        loadingNews: false
                    }
                }
            };
        /********************add****************/
        case ADD_POST:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: [action.payload].concat(state.user.post.posts),
                        loadingAddPosts: false
                    }
                }
            };
        case ADD_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: state.user.new.news.concat(action.payload)
                    }
                }
            };
        case ADD_TOP_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: state.user.top.top_news.concat(action.payload)
                    }
                }
            };
            case ADD_LIKED_NEWS:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        liked: {
                            ...state.user.liked,
                            liked_news: state.user.liked.liked_news.concat(action.payload)
                        }
                    }
                };
                case ADD_LIKED_FOLLOWING_NEWS:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        likedFollowing: {
                            ...state.user.likedFollowing,
                            liked_following_news: state.user.likedFollowing.liked_following_news.concat(action.payload)
                        }
                    }
                };
        case ADD_POSTS:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.concat(action.payload)
                    }
                }
            };
        /*********edit*****************/
        case EDIT_POST:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    editing: true
                                }
                                : post
                        )
                    }
                }
            };
        case EDIT_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: state.user.post.news.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    editing: true
                                }
                                : post
                        )
                    }
                }
            };
        case EDIT_TOP:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: state.user.top.top_news.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    editing: true
                                }
                                : post
                        )
                    }
                }
            };
            case EDIT_LIKED:
            return {
                ...state,
                user: {
                    ...state.user,
                    liked: {
                        ...state.user.liked,
                        liked_news: state.user.liked.liked_news.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    editing: true
                                }
                                : post
                        )
                    }
                }
            };
            case EDIT_LIKED_FOLLOWING:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        likedFollowing: {
                            ...state.user.likedFollowing,
                            liked_following_news: state.user.liked.liked_following_news.map(post =>
                                post.id === action.payload.id
                                    ? {
                                        ...post,
                                        editing: true
                                    }
                                    : post
                            )
                        }
                    }
                };
        /***********delete******************/
        case DELETE_POST:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.filter(
                            post => post.id !== action.payload
                        )
                    }
                }
            };
        case DELETE_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        posts: state.user.new.news.filter(
                            post => post.id !== action.payload
                        )
                    }
                }
            };
        case DELETE_TOP:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: state.user.top.top_news.filter(
                            post => post.id !== action.payload
                        )
                    }
                }
            };
            case DELETE_LIKED:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        liked: {
                            ...state.user.liked,
                            liked_news: state.user.liked.liked_news.filter(
                                post => post.id !== action.payload
                            )
                        }
                    }
                };

                case DELETE_LIKED_FOLLOWING:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        likedFollowing: {
                            ...state.user.likedFollowing,
                            liked_following_news: state.user.likedFollowing.liked_following_news.filter(
                                post => post.id !== action.payload
                            )
                        }
                    }
                };

        /****************update******************/
        case UPDATE_POST:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    text: action.payload.text,
                                    images: action.payload.images,
                                    editing: false
                                }
                                : post
                        )
                    }
                }
            };
        case UPDATE_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: state.user.new.news.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    text: action.payload.text,
                                    images: action.payload.images,
                                    editing: false
                                }
                                : post
                        )
                    }
                }
            };
        case UPDATE_TOP:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: state.user.top.top_news.map(post =>
                            post.id === action.payload.id
                                ? {
                                    ...post,
                                    text: action.payload.text,
                                    images: action.payload.images,
                                    editing: false
                                }
                                : post
                        )
                    }
                }
            };
            case UPDATE_LIKED:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        liked: {
                            ...state.user.liked,
                            liked_news: state.user.liked.liked_news.map(post =>
                                post.id === action.payload.id
                                    ? {
                                        ...post,
                                        text: action.payload.text,
                                        images: action.payload.images,
                                        editing: false
                                    }
                                    : post
                            )
                        }
                    }
                };

                case UPDATE_LIKED_FOLLOWING:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        likedFollowing: {
                            ...state.user.likedFollowing,
                            liked_following_news: state.user.likedFollowing.liked_following_news.map(post =>
                                post.id === action.payload.id
                                    ? {
                                        ...post,
                                        text: action.payload.text,
                                        images: action.payload.images,
                                        editing: false
                                    }
                                    : post
                            )
                        }
                    }
                };
        /*********************clear****************************/
        case CLEAR_POSTS:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.slice(0, 3),
                        isLoadedPosts: action.payload
                    }
                }
            };

        case CLEAR_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        news: state.user.new.news.slice(0, 3),
                        isLoaded: action.payload
                    }
                }
            };
        case CLEAR_TOP_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        top_news: state.user.top.top_news.slice(0, 3),
                        isLoadedTopNews: action.payload
                    }
                }
            };
            case CLEAR_LIKED_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    liked: {
                        ...state.user.liked,
                        liked_news: state.user.liked.liked_news.slice(0, 3),
                        isLoadedLikedNews: action.payload
                    }
                }
            };

            case CLEAR_LIKED_FOLLOWING_NEWS:
            return {
                ...state,
                user: {
                    ...state.user,
                    likedFollowing: {
                        ...state.user.likedFollowing,
                        liked_news: state.user.likedFollowing.liked_following_news.slice(0, 3),
                        isLoadedLikedFollowingNews: action.payload
                    }
                }
            };
        /***********loaded*************/
        case POSTS_LOADED:
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        isLoadedPosts: true,
                    }
                }
            };
        case NEWS_LOADED:
            return {
                ...state,
                user: {
                    ...state.user,
                    new: {
                        ...state.user.new,
                        isLoaded: true,
                    }
                }
            };

        case NEWS_TOP_LOADED:
            return {
                ...state,
                user: {
                    ...state.user,
                    top: {
                        ...state.user.top,
                        isLoadedTopNews: true,
                    }
                }
            };
            case NEWS_LIKED_LOADED:
                return {
                    ...state,
                    user: {
                        ...state.user,
                        liked: {
                            ...state.user.liked,
                            isLoadedLikedNews: true,
                        }
                    }
                };

                case NEWS_LIKED_FOLLOWING_LOADED:
                    return {
                        ...state,
                        user: {
                            ...state.user,
                            likedFollowing: {
                                ...state.user.likedFollowing,
                                isLoadedLikedFollowingNews: true,
                            }
                        }
                    };

        case LOADED_POST: {
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        isLoadedTopNews: false,
                        posts: []
                    }
                }
            };

        }

        case GET_LOCATION:
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    coords: {
                        data: action.payload,
                        isSend: true,
                        isDeniedPermission: false,
                        isPresent: true
                    }
                }
            });
        case GET_LOCATION_PERMISSION_DENIED:
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    coords: {
                        ...state.user.coords,
                        isDeniedPermission: true,
                        isPresent: false
                    }
                }
            });
        case UPDATE_USER_IS_ONLINE_AND_LOCATION:
            return Object.assign({}, state, {
               user_info: {
                   ...state.user_info,
                   isOnline: action.payload.isOnline,
                   lastTimeOnline: action.payload.lastTimeOnline,
                   distance: action.payload.distance
               }
            });
        case 'FETCH_POLLS':
            console.log("fetch");
            return {
                ...state,
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.map(post => {
                            if (post.id === action.payload.post_id) {
                                post.polls = action.payload.polls;
                            }
                            return post;
                        })}
                }
            };

        case 'ADD_POLL':
            console.log("add poll")
            return Object.assign({}, state, {
                user: {
                    ...state.user,
                    post: {
                        ...state.user.post,
                        posts: state.user.post.posts.map(post => {
                            if (post.id === action.payload.post_id) {
                                // const newPost = Object.assign({}, post, {});
                                // newPost.comments = action.payload.comments;
                                // return newPost;
                                post.polls = action.payload.polls;
                            }
                            return post;
                        })
                    }
                }
            });
        default:
            return state;
    }

};

export default rootReducer;
