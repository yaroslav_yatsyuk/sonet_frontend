import { CHANGE_GROUP_IS_CREATOR } from "../constants/actionTypes";

const changeGroupIsCreator = payload => {
  return {
    payload: payload,
    type: CHANGE_GROUP_IS_CREATOR
  };
};

export default changeGroupIsCreator;
