
const fetchPost = payload => {
    return {
        type: 'FETCH_POST',
        payload: payload
    };
};

export default fetchPost;
