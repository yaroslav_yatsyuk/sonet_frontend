import {DELETE_POST} from "../constants/actionTypes";

export const deletePost = payload => {
    return {
        type: DELETE_POST,
        payload: payload
    };
};

