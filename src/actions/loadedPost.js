import {LOADED_POST} from "../constants/actionTypes";

const loadedPosts = (payload) => {
    console.log("action");
    return {
        type: LOADED_POST,
        payload: payload
    };
};

export default loadedPosts;
