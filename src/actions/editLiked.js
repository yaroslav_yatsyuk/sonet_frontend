import {EDIT_LIKED} from "../constants/actionTypes";

const editLiked = payload => {
    return {
        type: EDIT_LIKED,
        payload: payload
    };
};

export default editLiked;
