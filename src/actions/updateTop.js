import {UPDATE_TOP} from "../constants/actionTypes";

const updateTopPost = payload => {
    console.log("action update");
    return {
        type: UPDATE_TOP,
        payload: payload
    };
};

export default updateTopPost;
