import { FETCH_GROUP_FAILURE } from "../constants/actionTypes";
export function fetchGroupFailure(error) {
  return {
    type: FETCH_GROUP_FAILURE,
    payload: error
  };
}
