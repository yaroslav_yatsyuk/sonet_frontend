import {ADD_TOP_NEWS} from "../constants/actionTypes";

const addTopNews = (payload) => {
    return{
        type: ADD_TOP_NEWS,
        payload: payload
    }
};

export default addTopNews;
