import { ADD_GROUP } from "../constants/actionTypes";
const addGroup = payload => ({
  type: ADD_GROUP,
  payload: payload
});
export { addGroup };
