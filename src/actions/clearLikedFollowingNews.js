import {CLEAR_LIKED_FOLLOWING_NEWS} from "../constants/actionTypes";

const  clearLikedFollowingNews = (payload) => {
    return {
        type: CLEAR_LIKED_FOLLOWING_NEWS,
        payload: payload
    }
};

export default clearLikedFollowingNews;
