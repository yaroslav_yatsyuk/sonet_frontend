import {FETCH_TOP_NEWS} from "../constants/actionTypes";

export const fetchAllTopNews = (payload) => {
    return {
        type: FETCH_TOP_NEWS,
        payload: payload
    }
};

export default fetchAllTopNews;

