import {FETCH_LIKED_FOLLOWING_NEWS} from "../constants/actionTypes";

export const fetchLikedFollowingNews = (payload) => {
    return {
        type: FETCH_LIKED_FOLLOWING_NEWS,
        payload: payload
    }
};

export default fetchLikedFollowingNews;

