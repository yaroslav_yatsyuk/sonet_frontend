
import {ADD_POST_LIKES} from "../constants/actionTypes";

const addPostLikes = payload => {
    return {
        payload: payload,
        type: ADD_POST_LIKES
    };
};

export default addPostLikes;
