
export const fetchAllNews = (payload) => {
    return {
        type: 'FETCH_NEWS',
        payload: payload
    }
};

export default fetchAllNews;

