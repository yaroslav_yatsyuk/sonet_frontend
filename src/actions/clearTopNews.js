import {CLEAR_TOP_NEWS} from "../constants/actionTypes";

const  clearTopNews = (payload) => {
    return {
        type: CLEAR_TOP_NEWS,
        payload: payload
    }
};

export default clearTopNews;
