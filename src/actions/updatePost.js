import {UPDATE_POST} from "../constants/actionTypes";

export const updatePost = payload => {
    console.log("action update");
    return {
        type: UPDATE_POST,
        payload: payload
    };
};
