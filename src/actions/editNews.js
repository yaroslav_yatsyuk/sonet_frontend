import {EDIT_NEWS} from "../constants/actionTypes";

const editNews = payload => {
    return {
        type: EDIT_NEWS,
        payload: payload
    };
};

export default editNews;
