import {CLEAR_NEWS} from "../constants/actionTypes";

const  clearNews = (payload) => {
    return {
        type: CLEAR_NEWS,
        payload: payload
    }
};

export default clearNews;
