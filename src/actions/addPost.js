import {ADD_POST} from "../constants/actionTypes";

const addPost = payload => {
    return {
        type: 'ADD_POST',
        payload: payload
    };
};

export default addPost;