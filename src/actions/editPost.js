import {EDIT_POST} from "../constants/actionTypes";

export const editPost = payload => {
    return {
        type: 'EDIT_POST',
        payload: payload
    };
};
