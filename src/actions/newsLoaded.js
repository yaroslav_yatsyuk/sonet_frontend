import {NEWS_LOADED} from "../constants/actionTypes";

const newsLoaded = (payload) => {
    console.log("action");
    return {
        type: NEWS_LOADED,
        payload: payload
    };
};

export default newsLoaded;
