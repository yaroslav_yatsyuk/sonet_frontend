import {ADD_USER_DATA} from "../constants/actionTypes";

const addUserData = payload => {

    return {
        type: ADD_USER_DATA,
        payload: payload
    };

};

export default addUserData;