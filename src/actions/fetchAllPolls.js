
const fetchAllPolls = (payload) => {
    return {
        type: 'FETCH_POLLS',
        payload: payload,
    };
};

export default fetchAllPolls;
