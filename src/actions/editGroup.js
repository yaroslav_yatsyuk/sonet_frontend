import { EDIT_GROUP } from "../constants/actionTypes";

const editGroup = payload => {
  return {
    type: EDIT_GROUP,
    payload: payload
  };
};

export default editGroup;
