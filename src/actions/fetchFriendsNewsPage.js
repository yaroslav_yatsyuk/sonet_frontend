import {FEATCH_FRIENDS_NEWS_PAGE} from "../constants/actionTypes";

const fetchFriendsNewsPage = payload => ({
   payload: payload,
   type: FEATCH_FRIENDS_NEWS_PAGE
});

export default fetchFriendsNewsPage;
