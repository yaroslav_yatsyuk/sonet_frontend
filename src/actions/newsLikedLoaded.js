import {NEWS_LIKED_LOADED} from "../constants/actionTypes";

const newsLikedLoaded = (payload) => {
    console.log("action");
    return {
        type: NEWS_LIKED_LOADED,
        payload: payload
    };
};

export default newsLikedLoaded;
