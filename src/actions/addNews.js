import {ADD_NEWS} from "../constants/actionTypes";

const addNews = (payload) => {
    return {
        type: ADD_NEWS,
        payload: payload
    }
};

export default addNews;
