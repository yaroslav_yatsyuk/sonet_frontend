import { CHANGE_GROUP_IS_SUBSCRIBED } from "../constants/actionTypes";

const chageGroupIsSubscribed = payload => {
  return {
    payload: payload,
    type: CHANGE_GROUP_IS_SUBSCRIBED
  };
};

export default chageGroupIsSubscribed;
