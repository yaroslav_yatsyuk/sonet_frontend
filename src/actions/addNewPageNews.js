
import {ADD_NEW_PAGE_NEWS} from "../constants/actionTypes";


const addNewPageNews = () => {
    return {
        type: ADD_NEW_PAGE_NEWS
    }
};

export {addNewPageNews};
