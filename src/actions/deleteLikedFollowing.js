import {DELETE_LIKED_FOLLOWING} from "../constants/actionTypes";

const deleteLikedFollowing = payload => {
    return {
        type: DELETE_LIKED_FOLLOWING,
        payload: payload
    };
};

export default deleteLikedFollowing;
