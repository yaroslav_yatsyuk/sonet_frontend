import {POSTS_LOADED} from "../constants/actionTypes";

const postsLoaded = (payload) => {
    console.log("action");
    return {
        type: POSTS_LOADED,
        payload: payload
    };
};

export default postsLoaded;
