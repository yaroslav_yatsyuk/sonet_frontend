import { FETCH_GROUP_SUCCESS } from "../constants/actionTypes";
export function fetchGroupSuccess(activeGroup) {
  return {
    type: FETCH_GROUP_SUCCESS,
    payload: activeGroup
  };
}
