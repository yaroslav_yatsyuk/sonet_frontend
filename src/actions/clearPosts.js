import {CLEAR_POSTS} from "../constants/actionTypes";

const  clearPosts = (payload) => {
    return {
        type: CLEAR_POSTS,
        payload: payload
    }
};

export default clearPosts;
