import {ADD_LIKED_FOLLOWING_NEWS} from "../constants/actionTypes";

const addLikedFollowingNews = (payload) => {
    return{
        type: ADD_LIKED_FOLLOWING_NEWS,
        payload: payload
    }
};

export default addLikedFollowingNews;
