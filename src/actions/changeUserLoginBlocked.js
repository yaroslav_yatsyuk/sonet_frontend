import {CHANGE_USER_LOGIN_BLOCKED} from "../constants/actionTypes";

const changeUserLoginBlocked = payload => ({
    type: CHANGE_USER_LOGIN_BLOCKED,
    payload: payload
});

export default changeUserLoginBlocked;