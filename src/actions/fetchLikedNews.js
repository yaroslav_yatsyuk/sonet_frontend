import {FETCH_LIKED_NEWS} from "../constants/actionTypes";

export const fetchLikedNews = (payload) => {
    return {
        type: FETCH_LIKED_NEWS,
        payload: payload
    }
};

export default fetchLikedNews;

