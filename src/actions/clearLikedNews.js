import {CLEAR_LIKED_NEWS} from "../constants/actionTypes";

const  clearLikedNews = (payload) => {
    return {
        type: CLEAR_LIKED_NEWS,
        payload: payload
    }
};

export default clearLikedNews;
