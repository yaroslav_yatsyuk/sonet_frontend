import {NEWS_TOP_LOADED} from "../constants/actionTypes";

const newsTopLoaded = (payload) => {
    console.log("action");
    return {
        type: NEWS_TOP_LOADED,
        payload: payload
    };
};

export default newsTopLoaded;
