
export const fetchAllPost = (payload) => {
    return {
        type: 'FETCH_POSTS',
        payload: payload
    }
};

export default fetchAllPost;

