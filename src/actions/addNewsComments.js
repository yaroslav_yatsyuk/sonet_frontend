
import {ADD_NEWS_COMMENTS} from "../constants/actionTypes";

const addNewsComments = payload => {
    return {
        payload: payload,
        type: ADD_NEWS_COMMENTS
    };
};

export default addNewsComments;
