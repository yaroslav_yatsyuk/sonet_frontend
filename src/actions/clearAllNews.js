import {CLEAR_ALL_NEWS} from "../constants/actionTypes";

const  clearAllNews = (payload) => {
    return {
        type: CLEAR_ALL_NEWS,
        payload: payload
    }
};

export default clearAllNews;
