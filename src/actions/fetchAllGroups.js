import { FETCH_GROUPS } from "../constants/actionTypes";
const fetchAllGroups = payload => {
  return {
    type: FETCH_GROUPS,
    payload: payload
  };
};

export { fetchAllGroups };
