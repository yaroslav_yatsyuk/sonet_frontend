import {DELETE_TOP} from "../constants/actionTypes";

const deleteTop = payload => {
    return {
        type: DELETE_TOP,
        payload: payload
    };
};

export default deleteTop;
