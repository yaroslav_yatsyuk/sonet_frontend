import {GET_LOCATION, GET_LOCATION_PERMISSION_DENIED} from "../constants/actionTypes";
import {getLocation as getLocationUtil} from "../utills";
import API from "../utills/API";
import {store} from "../store";

export const getLocation = () => dispatch  => {
    getLocationUtil().then(({coords}) => {

        if(coords.accuracy > 500) {
            return
        }


        console.log("get coords", coords);

        const data = {
            latitude: coords.latitude.toFixed(6),
            longitude: coords.longitude.toFixed(6),
            userId: store.getState().user.id
        };

        console.log(JSON.stringify(data),data, "geoApiData");
        API().post("/locations/users/", data).then(resp => {
            dispatch({
                type: GET_LOCATION,
                payload: coords
            });
            console.log("send geo data success", data);
        }).catch(error => {
            console.log("error during post /user/coords ", coords, error);
        });

    }).catch(error => {
        if(error.message === "Permission denied") {
            dispatch({
                type: GET_LOCATION_PERMISSION_DENIED,
            });
        }
        console.log("error during getting coords", error);
    });
};

