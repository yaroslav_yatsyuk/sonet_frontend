import { FETCH_GROUP } from "../constants/actionTypes";

const fetchGroup = payload => {
  return {
    type: FETCH_GROUP,
    payload: payload
  };
};
export { fetchGroup };
