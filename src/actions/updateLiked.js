import {UPDATE_LIKED} from "../constants/actionTypes";

const updateLikedPost = payload => {
    console.log("action update");
    return {
        type: UPDATE_LIKED,
        payload: payload
    };
};

export default updateLikedPost;
