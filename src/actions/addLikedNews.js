import {ADD_LIKED_NEWS} from "../constants/actionTypes";

const addLikedNews = (payload) => {
    return{
        type: ADD_LIKED_NEWS,
        payload: payload
    }
};

export default addLikedNews;
