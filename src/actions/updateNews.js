import {UPDATE_NEWS} from "../constants/actionTypes";

const updateNews = payload => {
    console.log("action update");
    return {
        type: UPDATE_NEWS,
        payload: payload
    };
};

export default updateNews;
