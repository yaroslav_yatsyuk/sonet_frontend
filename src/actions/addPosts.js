import {ADD_POSTS} from "../constants/actionTypes";

const addPosts = (payload) => {
    return{
        type: ADD_POSTS,
        payload: payload
    }
};

export default addPosts;
