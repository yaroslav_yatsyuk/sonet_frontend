import {EDIT_TOP} from "../constants/actionTypes";

const editTop = payload => {
    return {
        type: EDIT_TOP,
        payload: payload
    };
};

export default editTop;
