import { FETCH_FOLLOWERS } from "../constants/actionTypes";

const fetchFollowers = payload => {
  return {
    type: FETCH_FOLLOWERS,
    payload: payload
  };
};
export { fetchFollowers };
