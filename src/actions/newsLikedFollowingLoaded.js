import {NEWS_LIKED_FOLLOWING_LOADED} from "../constants/actionTypes";

const newsLikedFollowingLoaded = (payload) => {
    console.log("action");
    return {
        type: NEWS_LIKED_FOLLOWING_LOADED,
        payload: payload
    };
};

export default newsLikedFollowingLoaded;
