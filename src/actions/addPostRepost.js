import {ADD_POST_REPOST} from "../constants/actionTypes";

const addPostRepost = payload => {
    return {
        payload: payload,
        type: ADD_POST_REPOST
    };
};

export default addPostRepost;
