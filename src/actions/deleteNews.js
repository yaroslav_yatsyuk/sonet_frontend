import {DELETE_NEWS} from "../constants/actionTypes";

const deleteNews = payload => {
    return {
        type: DELETE_NEWS,
        payload: payload
    };
};

export default deleteNews;
