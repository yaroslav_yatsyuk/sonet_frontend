import {UPDATE_USER_IS_ONLINE_AND_LOCATION} from "../constants/actionTypes";

const updateUserIsOnlineAndLocation = payload => {
    return {
      type: UPDATE_USER_IS_ONLINE_AND_LOCATION,
      payload: payload
    };
};

export default updateUserIsOnlineAndLocation;