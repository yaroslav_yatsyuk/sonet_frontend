import gotMessageSound from "../components/chat/sounds/alarmed-thrill.mp3";
const gotMessage = new Audio(gotMessageSound);

const conversationsLoaded = newConversations => {
  return {
    type: "FETCH_CONVERSATIONS_SUCCESS",
    payload: newConversations
  };
};

const conversationsRequested = () => {
  return {
    type: "FETCH_CONVERSATIONS_REQUEST"
  };
};

const friendsLoaded = newConversations => {
  return {
    type: "FETCH_FRIENDS_SUCCESS",
    payload: newConversations
  };
};

const friendsRequested = () => {
  return {
    type: "FETCH_FRIENDS_REQUEST"
  };
};

const messagesLoaded = newMessages => {
  return {
    type: "FETCH_MESSAGES_SUCCESS",
    payload: newMessages
  };
};

const messagesRequested = () => {
  return {
    type: "FETCH_MESSAGES_REQUEST"
  };
};

const messageAdded = (content, sender, sendTime, senderId, avatar, id) => {
  return {
    type: "MESSAGE_ADDED",
    payload: { content, sender, sendTime, senderId, avatar, id }
  };
};

const channelsRequested = () => {
  return {
    type: "FETCH_CHANNELS_REQUEST"
  };
};

const channelsLoaded = newMessages => {
  return {
    type: "FETCH_CHANNELS_SUCCESS",
    payload: newMessages
  };
};

const messageInputChange = symbol => {
  return {
    type: "MESSAGE_INPUT_CHANGE",
    payload: symbol
  };
};

const currentChannelIdChanged = newId => {
  return {
    type: "CURRENT_CHANNEL_ID_CHANGE",
    payload: newId
  };
};

const conversationNameInputChange = symbol => {
  return {
    type: "CONVERSATION_NAME_INPUT_CHANGE",
    payload: symbol
  };
};

const channelNameInputChange = symbol => {
  return {
    type: "CHANNEL_NAME_INPUT_CHANGE",
    payload: symbol
  };
};

const searchFriendsInputChange = symbol => {
  return {
    type: "SEARCH_FRIENDS_INPUT_CHANGE",
    payload: symbol
  };
};

const toggleCheckFriend = friend => {
  return {
    type: "TOGGLE_CHECK_FRIEND",
    payload: friend
  };
};

const messageInputClear = () => {
  return {
    type: "MESSAGE_INPUT_CLEAR"
  };
};

const showModal = () => {
  return {
    type: "SHOW_MODAL"
  };
};

const hideModal = () => {
  return {
    type: "HIDE_MODAL"
  };
};

const currentChannelIdChange = (
  sonetService,
  dispatch,
  channelId,
  token,
  userId
) => {
  dispatch(messagesRequested());
  dispatch(currentChannelIdChanged(channelId));
  sonetService
    .getMessages(channelId, token)
    .then(data => dispatch(messagesLoaded(data)));
  sonetService.socketDisconnect();
  sonetService.socketConnect();
  sonetService.socketSubscribe(
    channelId,
    (content, sender, sendTime, senderId, avatar, id) => {
      dispatch(messageAdded(content, sender, sendTime, senderId, avatar, id));
      if (senderId !== userId) {
        gotMessage.play();
      }
    },
    token
  );
};

const fetchConversations = (sonetService, dispatch, userId, token) => () => {
  dispatch(conversationsRequested());
  return sonetService.getConversations(userId, token).then(data => {
    dispatch(conversationsLoaded(data));
    return data;
  });
};

const fetchFriends = (sonetService, dispatch, userId, token) => () => {
  dispatch(friendsRequested());
  sonetService
    .getFriends(userId, token)
    .then(data => dispatch(friendsLoaded(data)));
};

const fetchMessages = (sonetService, dispatch, channelId, token) => () => {
  dispatch(messagesRequested());
  sonetService
    .getMessages(channelId, token)
    .then(data => dispatch(messagesLoaded(data)));
};

const fetchChannels = (sonetService, dispatch, chatId, token) => () => {
  dispatch(messagesRequested());
  dispatch(channelsRequested());
  return sonetService.getChannels(chatId, token).then(data => {
    dispatch(channelsLoaded(data));
    return data;
  });
};

export {
  fetchConversations,
  fetchMessages,
  fetchFriends,
  fetchChannels,
  messageAdded,
  messageInputChange,
  messageInputClear,
  conversationNameInputChange,
  searchFriendsInputChange,
  channelNameInputChange,
  currentChannelIdChange,
  toggleCheckFriend,
  showModal,
  hideModal,
  conversationsRequested
};
