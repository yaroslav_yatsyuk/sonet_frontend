import {DELETE_LIKED} from "../constants/actionTypes";

const deleteLiked = payload => {
    return {
        type: DELETE_LIKED,
        payload: payload
    };
};

export default deleteLiked;
