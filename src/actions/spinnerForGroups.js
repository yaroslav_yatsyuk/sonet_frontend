import { SPINNER_FOR_GROUPS } from "../constants/actionTypes";

const spinnerForGroup = payload => {
  return {
    type: SPINNER_FOR_GROUPS,
    payload: payload
  };
};
export { spinnerForGroup };
