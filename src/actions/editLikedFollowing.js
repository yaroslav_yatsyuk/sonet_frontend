import {EDIT_LIKED_FOLLOWING} from "../constants/actionTypes";

const editLikedFollowing = payload => {
    return {
        type: EDIT_LIKED_FOLLOWING,
        payload: payload
    };
};

export default editLikedFollowing;
