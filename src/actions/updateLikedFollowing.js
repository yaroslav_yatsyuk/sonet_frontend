import {UPDATE_LIKED_FOLLOWING} from "../constants/actionTypes";

const updateLikedFollowingPost = payload => {
    console.log("action update");
    return {
        type: UPDATE_LIKED_FOLLOWING,
        payload: payload
    };
};

export default updateLikedFollowingPost;
