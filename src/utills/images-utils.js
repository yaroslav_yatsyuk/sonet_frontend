
const makeStyleToDiv = (length) => {
    switch (length) {
        case 1:
            return ("list-images one");
        case 2:
            return ("list-images two");
        case 3:
            return ("list-images three");
        case 4:
            return ("list-images four");
        case 5:
            return ("list-images five");
        case 6:
            return ("list-images six");
        case 7:
            return ("list-images seven");
        case 8:
            return ("list-images eight");
        case 9:
            return ("list-images nine");
        case 10:
            return ("list-images ten");
        default: return ("list-images-one");

    }

};

const makeStyleToImage = (ids) => {
    switch (ids) {
        case 1:
            return ("one");
        case 2:
            return  ("two");
        case 3:
            return ("three");
        case 4:
            return ("four");
        case 5:
            return ("five");
        case 6:
            return ("six");
        case 7:
            return ("seven");
        case 8:
            return ("eight");
        case 9:
            return ("nine");
        case 10:
            return ("ten");
        default:
            return ("one");
    }};

export {makeStyleToDiv, makeStyleToImage};
