import axios from "axios";
import {store} from "../store";

const getAutorizationTiken = () => {
    const state = store.getState();
    const authToken = state.user.token;

    const Authorization = authToken ? `Bearer_${authToken}` : null;
    return Authorization;
};

var baseUrl;
if (process.env.NODE_ENV === "production") {
    baseUrl = "https://api.sonet-social.net/api/";
} else {
    baseUrl = process.env.REACT_APP_LOCAL_API_URL;
}

const API = (config = {security: true}) => {
    let security = config.security;
    let headers;

    if (security) {
        headers = {
            Authorization: getAutorizationTiken()
        };
    } else {
        headers = {};
    }

    return axios.create({
        baseURL: baseUrl,
        contentType: "application/json",
        headers: headers
    });

};

export default API;
