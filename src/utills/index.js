const blankValidator = (value, name) => {
    if (value != null && value.length) {
        return null;
    }
    return `${name} can't be blank`;
};

const lengthValidator = (value, length) => {
    if (value == null) return "can't be blank";
    if (value.length >= length) {
        return null;
    }
    return `is too short (minimum is ${length} characters)`;
};

const maxLengthValidator = (name, value, length) => {
    if (value.length <= length) {
        return null;
    }
    return `${name} is too long`;
};

const emailValidator = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase()) ? null : "Email Address is invalid";
};

const passwordConfirmationValidator = (password, confirmPassword) => {
    return password === confirmPassword ? null : "doesn't match Password";
};

export const validateEmail = value => {
    const blanckError = blankValidator(value, "Email Address");
    const emailError = emailValidator(value);
    if (blanckError) {
        return blanckError;
    }

    if (emailError) {
        return emailError;
    }

    return null;
};

export const validateNickname = value => {
    const blankError = blankValidator(value, "Username");
    if (blankError) {
        return blankError;
    }

    return maxLengthValidator("Username", value, 50);
};

export const passwordValidator = value => {
    const blankError = blankValidator(value, "Password");

    if (blankError) {
        return blankError;
    }
    const lengthError = lengthValidator(value, 8);

    if (lengthError) {
        return lengthError;
    }

    return maxLengthValidator("Password", value, 50);
};

export const confirmPasswordValidator = (password, confirmPassword) => {
    const blanckError = blankValidator(confirmPassword, "Confirm password");

    if (blanckError) {
        return blanckError;
    }

    const maxLengthError = maxLengthValidator("Confirm password", password, 50);

    if (maxLengthError) {
        return maxLengthError;
    }

    return password === confirmPassword ? null : "doesn't match Password";
};

export const validateAgreement = agreement => {
    return agreement ? null : "You must agree";
};

export const getLocation = () => {
    const geolocation = navigator.geolocation;
    return new Promise((resolve, reject) => {
        if (!geolocation) {
            reject(new Error('Not Supported'));
        }
        geolocation.watchPosition((position) => {
            resolve(position);
        }, () => {
            reject(new Error('Permission denied'));
        });
    });
};


