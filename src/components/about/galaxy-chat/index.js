import React from "react";
import {Link} from "react-router-dom";

const GalaxyChat = () => {
    return (
        <Link to="/home">
            <h4>
                <i className="fa fa-address-book fa-fw"> </i>
                Galaxy
                chat
                <small> Tr
                    intergalactic
                    chat :)
                </small>
            </h4>
            <div className='avatar-stack'>
                <img width="48" height="48" alt="" className="account__avatar" src="/img/dart-avatar.png"/>
                <img width="48" height="48" alt="" className="account__avatar" src="/img/avatar-losder.jpg"/>
                <img width="48" height="48" alt="" className="account__avatar" src="/img/chubarka-avatar.jpg"/>
                <img width="48" height="48" alt="" className="account__avatar" src="/img/luk-avatar.jpg"/>
            </div>
        </Link>
    );
};

export default GalaxyChat;