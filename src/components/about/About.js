import React from "react";

import Register from './registerForm/RegisterFormContainer';
import Login from './login/LoginContainer';
import Post from './post/Post';
import Footer from './footer/Footer';
import GalaxyChat from "./galaxy-chat";
import Logo from "../common/logo";
import "./About.css";
import SpinnerProvider from "../common/spinner-provider";

class About extends React.Component {

    render() {
        return (
            <div className="stars">
                <div className="twinkling">
                    <div className='public-layout' style={{display: "flex", flexDirection: "column"}}>
                        <div className='container'>
                            <div className='landing'>
                                <Logo/>
                                <div className='landing__grid'>
                                    <div className='landing__grid__column landing__grid__column-registration'>
                                        <div className='box-widget'>
                                            <SpinnerProvider>
                                                <Register/>
                                            </SpinnerProvider>
                                        </div>
                                    </div>
                                    <div className='landing__grid__column landing__grid__column-login'>
                                        <div className='box-widget'>
                                            <SpinnerProvider>
                                                <Login/>
                                            </SpinnerProvider>
                                        </div>
                                        <div className='directory'>
                                            <div className='directory__tag'>
                                                <GalaxyChat/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default About;