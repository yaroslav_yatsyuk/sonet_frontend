import React, {Component} from "react";
import {Redirect, Link} from "react-router-dom";

import "./Login.css";
import {passwordValidator, validateEmail} from "../../../utills";
import API from "../../../utills/API";
import GoogleLogin from "../google-login";
import FacebookLogin from "../facebook-login";
import FullScreenSpinner from "../../common/full-screen-spinner";

class Login extends Component {

    state = {
        redirectToLogin: false,
        redirectToAccount: false,
    };

    handleInputChange = e => {
        this.setInputState(
            e.target.name,
            e.target.value
        )
    };

    setInputState = async (name, value) => {
        await this.props.addUserLoginData({
            ...this.props.data,
            [name]: value
        });
        this.validate();
    };

    validate() {
        this.props.addUserLoginErrors({
            email: validateEmail(this.props.data.email),
            password: passwordValidator(this.props.data.password)
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        if (this.props.errors.email == null && this.props.errors.password == null) {
            this.props.loading(true);
            console.log("before api call");
            //console.log(JSON.stringify(this.props.data));
            API({security: false}).post("/auth/login", this.props.data)
                .then(resp => {

                    console.log("after api call got response ", resp);
                    this.props.addUserAuntificatedData({
                        id: resp.data.id,
                        firstName: resp.data.firstName,
                        lastName: resp.data.lastName,
                        email: resp.data.email,
                        nickname: resp.data.nickname,
                        avatar: resp.data.avatar.url,
                        background: resp.data.background.url,
                        token: resp.data.token,
                    });
                    console.log("after addUserAuntificatedData dispatch");
                    this.props.addUserLoginData({
                        email: "",
                        password: ""
                    });
                    console.log("after addUserLoginData dispatch");
                    this.setState({
                        redirectToAccount: true,
                    });
                    console.log("after redirect");
                    this.props.loading(false);
                    console.log("after loading false");


                }).catch(error => {
                console.log("login error ", error);
                if (error.status === 403) {
                    this.props.changeUserLoginBlocked(true);
                    console.log("login error status 403");
                } else {
                    if (error.response !== undefined && error.response.message !== "Email not verified") {
                        this.props.addUserLoginErrors({
                            emailNotVerified: true
                        });
                        this.props.changeAlertEmailConfirmation(false);
                    } else {
                        this.props.addUserLoginErrors({
                            email: "Invalid Email or password",
                            password: null
                        });
                        console.log("login error Invalid Email or password")
                    }
                }
                console.log("login error status ", error.status);

                this.setState({
                    redirectToLogin: true,
                });
                console.log("after redirect to login");
                this.props.loading(false);
                console.log("after loading false");
            });
        } else {
            this.setState({
                redirectToLogin: true
            });
            this.props.loading(false);
        }
    };

    render() {
        if (this.state.redirectToAccount) {
            return <Redirect push to="/home"/>
        }
        if (this.state.redirectToLogin) {
            return <Redirect push to="/auth/login"/>
        }

        return (

            <form className="simple_form new_user"
                  id="new_user"
                  noValidate="novalidate"
                  action="/auth/sign_in"
                  onSubmit={this.handleSubmit}
                  method="post">
                <div className='fields-group'>
                    <div className="input email optional user_email">
                        <input aria-label="E-mail address"
                               className="string email optional"
                               placeholder="E-mail address"
                               type="email"
                               name="email"
                               value={this.props.data.email}
                               onChange={this.handleInputChange}
                               id="user_email"/>
                    </div>
                    <div className="input password optional user_password">
                        <input aria-label="Password"
                               className="password optional"
                               placeholder="Password"
                               type="password"
                               name="password"
                               onChange={this.handleInputChange}
                               value={this.props.data.password}
                               id="user_password"/>
                    </div>
                </div>
                <div className='actions'>
                    <button name="button" type="submit" className="btn button button-primary">
                        LOG IN
                    </button>
                </div>
                <div className="actions social-actions">
                    <GoogleLogin text="LOG IN WITH GOOGLE"/>
                    <FacebookLogin text="LOG IN WITH FACEBOOk"/>
                </div>
                <p className='hint subtle-hint'>
                    <Link to="/auth/password-restore">
                        Trouble logging in?
                    </Link>
                </p>
            </form>
        );
    };
}

export default Login;