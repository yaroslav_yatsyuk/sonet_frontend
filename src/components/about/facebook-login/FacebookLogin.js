import React from "react";
import API from "../../../utills/API";
import FacebookLogin from 'react-facebook-login';
import "./FacebookLogin.css";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
import addUserAuntificatedData from "../../../actions/addUserAuntificatedData";

class FacebookLoginComponent extends React.Component {

    state = {
        redirect: false,
        error: false
    };

    render() {
        let redirectUrl;

        if (process.env.NODE_ENV === "production") {
            redirectUrl = "https://sonet-social.net/auth/facebook/callback";
        } else {
            redirectUrl = "http://localhost:3000/auth/facebook/callback";
        }

        const responseFacebook = (response) => {
            console.log(response, "facebook-response");
            if(response.status === undefined) {
                console.log("calling api to log in by facebook");
                API().post("/auth/facebook", {
                    token: response.accessToken
                }).then(resp => {
                    this.props.addUserAuntificatedData({
                        id: resp.data.id,
                        firstName: resp.data.firstName,
                        lastName: resp.data.lastName,
                        email: resp.data.email,
                        nickname: resp.data.nickname,
                        token: resp.data.token,
                    });
                    console.log("facebook login success");
                    this.setState({
                        success: true
                    });
                }).catch(err => {
                    console.log("facebook login error");
                    this.setState({
                        error: true
                    });
                });
            }  else {
                console.log("facebook error", response.status);
                this.setState({
                    error: true
                });
            }

        };

        /*if (this.state.error) {
            return <Redirect to="/about"/>
        }*/

        if (this.state.success) {
            console.log("redirect to home");
            return <Redirect push to="/home"/>
        }

        return (
            <FacebookLogin
                appId="2370641763218797"
                redirectUri={redirectUrl}
                autoLoad={false}
                fields="name,email,picture"
                textButton={this.props.text}
                cssClass="login-with-facebook-btn"
                icon="fa fa-facebook"
                callback={responseFacebook}
                disableMobileRedirect={true}
           />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addUserAuntificatedData : payload => dispatch(addUserAuntificatedData(payload))
});

export default connect(null, mapDispatchToProps)(FacebookLoginComponent);