import React from "react";
import { Nav, NavLink, NavItem } from "reactstrap";
import classNames from "classnames";
import "./profile-tabs.css";

class ProfileTabs extends React.Component {
  render() {
    return (
      <Nav tabs className="tabs">
        <NavItem>
          <NavLink
            className={classNames({ active: this.props.activeTab === "1" })}
            onClick={() => {
              this.props.toggle("1");
            }}
          >
            <span className="tabs__text">Posts</span>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classNames({ active: this.props.activeTab === "2" })}
            onClick={() => {
              this.props.toggle("2");
            }}
          >
            <span className="tabs__text">Followers</span>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classNames({ active: this.props.activeTab === "3" })}
            onClick={() => {
              this.props.toggle("3");
            }}
          >
            <span className="tabs__text">Following</span>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classNames({ active: this.props.activeTab === "4" })}
            onClick={() => {
              this.props.toggle("4");
            }}
          >
            <span className="tabs__text">Groups</span>
          </NavLink>
        </NavItem>
      </Nav>
    );
  }
}

export default ProfileTabs;
