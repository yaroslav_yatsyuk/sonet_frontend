import React from "react";
import API from "../../../utills/API";
import {connect} from "react-redux";
import "./follow-button.css";

class FollowButton extends React.Component {

    state = {
        isFollowed: false,
        loading: false
    };

    componentDidMount() {
        API().get(`/users/follow?followerId=${this.props.user.id}&followingId=${this.props.user_info.id}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                isFollowed: true
            });
        }).catch(error => {
            console.log("error", error);
        });
    }

    follow = () => {
        this.setState({
            loading: true
        });
        API().post(`/users/follow?`, {
            followerId: this.props.user.id,
            followingId: this.props.user_info.id
        }, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                isFollowed: true,
                loading: false
            });
        }).catch(error => {
            console.log("error", error);
        });
    };


    unfollow = () => {
        this.setState({
            loading: true
        });
        API().delete(`/users/unfollow`, {
            data: {
                followerId: this.props.user.id,
                followingId: this.props.user_info.id
            },
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                isFollowed: false,
                loading: false
            });
        }).catch(error => {
            console.log("error", error);
        });
    };

    render() {

        if (this.state.loading) {
            return <button className="follow-button">Processing...</button>;
        } else if(this.state.isFollowed) {
            return <button className="follow-button" onClick={this.unfollow}>Unfollow</button>;
        } else {
            return <button className="follow-button" onClick={this.follow}>Follow</button>;
        }
    }
}


const mapStateToProps = state => ({
    user: state.user,
    user_info: state.user_info
});

export default connect(mapStateToProps, null)(FollowButton);