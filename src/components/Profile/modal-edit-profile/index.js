import React from "react";
import Spinner from "../../common/spinner";
import {Modal} from "reactstrap";
import addUserAuntificatedData from "../../../actions/addUserAuntificatedData";
import addUserData from "../../../actions/addUserData";
import {connect} from "react-redux";
import API from "../../../utills/API";

class ModalEditProfile extends React.Component {


    state = {
        nickname: this.props.user_info.nickname,
        firstName: this.props.user_info.firstName,
        lastName: this.props.user_info.lastName,
        city: this.props.user_info.city,
        country: this.props.user_info.country,
        planet: this.props.user_info.planet,
        hasError: false,
        background: this.props.user_info.background,
        backgroundFile: '',
        backgroundPreviewUrl: '',
        backgroundLoaded: false,
        avatar: this.props.user_info.avatar,
        avatarFile: '',
        avatarPreviewUrl: '',
        avatarLoaded: false,
        editProfileLoading: false
    };

    saveBackground() {
        let formData = new FormData();
        formData.append('file', this.state.backgroundFile);

        API().post(`/users/${this.props.user.id}/files`, formData, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                backgroundLoaded: true,
                background: resp.data.message
            });
            if (this.state.avatarLoaded) {
                this.handleSubmit();
            }
        });

    }

    _handleBackgroundChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let background = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                backgroundFile: background,
                backgroundPreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(background)
    }

    saveAvatar() {
        let formData = new FormData();
        formData.append('file', this.state.avatarFile);

        API().post(`/users/${this.props.user.id}/files`, formData, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                avatarLoaded: true,
                avatar: resp.data.message
            });
            if (this.state.backgroundLoaded) {
                this.handleSubmit();
            }
        });
    }

    _handleAvatarChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let avatar = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                avatarFile: avatar,
                avatarPreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(avatar)
    }


    nicknameChange(e) {
        this.setState({
            nickname: e.target.value,
            hasError: false
        });
    }

    firstNameChange(e) {
        this.setState({
            firstName: e.target.value
        });
    }

    lastNameChange(e) {
        this.setState({
            lastName: e.target.value
        });
    }

    cityNameChange(e) {
        this.setState({
            city: e.target.value
        });
    }

    countryNameChange(e) {
        this.setState({
            country: e.target.value
        });
    }

    planetNameChange(e) {
        this.setState({
            planet: e.target.value
        });
    }

    preHandleSubmit = (event) => {
        event.preventDefault();
        this.setState({editProfileLoading: true});
        if ((this.state.avatarFile !== '') && (this.state.backgroundFile !== '')) {
            this.saveAvatar();
            this.saveBackground();
        } else if ((this.state.avatarFile !== '') && (this.state.backgroundFile === '')) {
            this.setState({backgroundLoaded: true});
            this.saveAvatar();
        } else if ((this.state.avatarFile === '') && (this.state.backgroundFile !== '')) {
            this.setState({avatarLoaded: true});
            this.saveBackground();
        } else {
            this.handleSubmit();
        }
    };

    handleSubmit = () => {
        API().patch(`/users/${this.props.user.id}`, {
            nickname: this.state.nickname,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            city: this.state.city,
            country: this.state.country,
            planet: this.state.planet,
            avatar: {
                url: this.state.avatar
            },
            background: {
                url: this.state.background
            }
        }, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({editProfileLoading: false});
            this.props.addUserAuntificatedData({
                id: resp.data.id,
                firstName: resp.data.firstName,
                lastName: resp.data.lastName,
                email: resp.data.email,
                nickname: resp.data.nickname,
                city: resp.data.city,
                country: resp.data.country,
                planet: resp.data.planet,
                avatar: this.state.avatar,
                background: this.state.background,
                token: this.props.user.token,
            });
            this.props.addUserData({
                id: resp.data.id,
                firstName: resp.data.firstName,
                lastName: resp.data.lastName,
                email: resp.data.email,
                nickname: resp.data.nickname,
                avatar: resp.data.avatar.url,
                background: resp.data.background.url
            });
        }).then((data) => {
                this.closeModal();
            }
        ).catch((error) => {
            console.log(error);
            console.log(error.response);
            this.setState({
                editProfileLoading: false,
                hasError: true
            });
        });
    };

    closeModal = () => {
        if(this.state.hasError) {
            this.setState({
                hasError: false
            });
        }

        this.props.onClose();
    };

    render() {
        let {backgroundPreviewUrl, avatarPreviewUrl} = this.state;
        let $backgroundPreview = null;
        if (backgroundPreviewUrl) {
            $backgroundPreview = (<img className="editBackgroundProfileImg" src={backgroundPreviewUrl}/>);
        } else {
            $backgroundPreview = (<img className="editBackgroundProfileImg" src={this.props.user_info.background}/>);
        }

        let $avatarPreview = null;
        if (avatarPreviewUrl) {
            $avatarPreview = (<img className="editProfile_logo" src={avatarPreviewUrl}/>);
        } else {
            $avatarPreview = (<img className="editProfile_logo" src={this.props.user_info.avatar}/>);
        }

        return (
            <Modal isOpen={this.props.isOpen}>
                {this.state.editProfileLoading ?
                    <div className="editProfileSpinner">
                        <Spinner/>
                    </div> : ''}
                <div className="row editUserModal">
                    <div className="editUserModalHeader col-12 pt-2 d-flex flex-row">
                        <button className="closeModalButton m-1" onClick={this.closeModal}>
                            <i className="fa fa-times"/>
                        </button>
                        <span className="edit-your-info-text text-left">
                            Edit profile
                        </span>
                        <form className="edit-save-form d-flex justify-content-end" onSubmit={this.preHandleSubmit}>
                            <button className="btn btn-primary editUserSaveButton float-xs-right">
                                Save
                            </button>
                        </form>
                    </div>
                    <div
                        className="col-12 editUserModalElementsDiv d-flex flex-column mt-2">
                        <div className="editBackgroundProfileImgDiv">
                            <form onSubmit={(e) => this._handleSubmitBackground(e)}>
                                <label className="btn btn-default BackgroundImageInput">
                                    <i className="fa fa-camera"/> <input type="file"
                                                                         hidden
                                                                         onChange={(e) => this._handleBackgroundChange(e)}/>
                                </label>
                            </form>
                            {$backgroundPreview}
                        </div>
                        <div className="editProfileHeader">
                            <div className="edit_profile_logo_container">
                                <form onSubmit={(e) => this._handleSubmitAvatar(e)}>
                                    <label className="btn btn-default AvatarImageInput">
                                        <i className="fa fa-camera"/> <input type="file"
                                                                             hidden
                                                                             onChange={(e) => this._handleAvatarChange(e)}/>
                                    </label>
                                </form>
                                {$avatarPreview}
                            </div>
                        </div>

                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">Nickname</span>
                            <input value={this.state.nickname}
                                   onChange={(e) => this.nicknameChange(e)}
                                   placeholder="Add your nickname"
                                   className="textareaEditUser"
                            />
                            {this.state.hasError ?
                                <span className="errorNicknameAlreadyExists">Nickname Already Exist</span> : ''}
                        </div>
                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">First Name</span>
                            <input value={this.state.firstName}
                                   onChange={(e) => this.firstNameChange(e)}
                                   placeholder="Add your first name"
                                   className="textareaEditUser"/>
                        </div>
                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">Last Name</span>
                            <input value={this.state.lastName}
                                   onChange={(e) => this.lastNameChange(e)}
                                   placeholder="Add your last name"
                                   className="textareaEditUser"/>
                        </div>
                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">City</span>
                            <input value={this.state.city}
                                   onChange={(e) => this.cityNameChange(e)}
                                   placeholder="Add your city"
                                   className="textareaEditUser"/>
                        </div>
                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">Country</span>
                            <input value={this.state.country}
                                   onChange={(e) => this.countryNameChange(e)}
                                   placeholder="Add your country"
                                   className="textareaEditUser"/>
                        </div>
                        <div className="editUserModalElement d-flex flex-column">
                            <span className="textareaEditUserUpper">Planet</span>
                            <input value={this.state.planet}
                                   onChange={(e) => this.planetNameChange(e)}
                                   placeholder="Add your planet"
                                   className="textareaEditUser"/>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
    user_info: state.user_info
});

const mapDispatchToProps = dispatch => ({
    addUserAuntificatedData: payload => dispatch(addUserAuntificatedData(payload)),
    addUserData: async payload => dispatch(addUserData(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalEditProfile);