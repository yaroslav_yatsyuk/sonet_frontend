import React, {Component} from 'react';
import {Container, Row, Col, TabContent, TabPane, Nav, NavItem, NavLink} from "reactstrap";
import './Profile.scss';
import Followers from './Followers'
import Following from "./Following";
import Groups from "./Groups/Groups"
import ListOfPages from '../ListOfPages'
import {connect} from "react-redux";
import API from "../../utills/API";
import addUserAuntificatedData from "../../actions/addUserAuntificatedData";
import addUserData from "../../actions/addUserData";
import PostForm from "../post/post-form/post-form";
import AllPost from "../post/all-post/all-post";
import ProfileTabs from "./profile-tabs";
import ModalEditProfile from "./modal-edit-profile";
import FollowButton from "./follow-button";
import FullScreenSpinner from "../common/full-screen-spinner";

import "./Profile.css";
import BurgerMenu from "../ListOfPages/burger-menu";
import classNames from "classnames";
import MobileMenu from "./mobile-menu";
import {getLocation} from "../../actions/getLocation";
import {Redirect} from "react-router";
import updateUserIsOnlineAndLocation from "../../actions/updateUserIsOnlineAndLocation";


class Profile extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    state = {
        activeTab: '1',
        modalIsOpen: false,
        userProfileLoaded: false,
    };

    componentDidMount() {

        API().get(`/users/${this.props.match.params.userId}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(async resp => {
            await this.props.addUserData({
                id: resp.data.id,
                firstName: resp.data.firstName,
                lastName: resp.data.lastName,
                email: resp.data.email,
                nickname: resp.data.nickname,
                avatar: resp.data.avatar.url,
                background: resp.data.background.url
            });
            this.setState({
                userProfileLoaded: true
            });
            if (this.props.user.id !== this.props.user_info.id) {
                await this.props.getLocation();
                if (this.props.user.coords.isPresent) {
                    this.refreshUserIsOnline();
                }
            }
        }).catch(error => {
            console.log("error", error);
        });
    }

    refreshUserIsOnline = () => {
        API().get(`/users/${this.props.user_info.id}/is-online`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`,
            }
        }).then(async resp => {
            console.log(resp.data);
            this.props.updateUserIsOnlineAndLocation({
                isOnline: resp.data.isOnline,
                lastTimeOnline: resp.data.lastActivity,
                distance: resp.data.distance
            });
        }).catch(error => {
            console.log("error during get user is online")
        });

        setTimeout(this.refreshUserIsOnline, 1000 * 60);
    };

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        });
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        });
    }

    render() {


        if (!this.state.userProfileLoaded) {
            return <FullScreenSpinner/>;
        }

        let FollowOrEditProfileButton;
        let isOnline;
        let lastTimeOnline;
        if (this.props.user.id === this.props.user_info.id) {
            FollowOrEditProfileButton = (
                <div className="profile-info__edit-profile">
                    <button onClick={this.openModal} className="profile-info__edit-profile-button">
                        Edit Profile <i className="fa fa-pencil profile-info__icon"></i>
                    </button>
                </div>
            )
        } else {
            FollowOrEditProfileButton = (
                <div className="profile-info__follow">
                    <FollowButton/>
                </div>
            )
            isOnline = <div
                className={classNames("profile-info__is-online", {"online": this.props.user_info.isOnline})}></div>;
            if (!this.props.user_info.isOnline && this.props.user_info.lastTimeOnline) {
                const data = new Date(this.props.user_info.lastTimeOnline);
                const lastTimeOnlineString = data.getFullYear() + "-" + data.getMonth() + " " + data.getHours() + ":" + data.getMinutes();

                lastTimeOnline =
                    <div className="profile-info__last-time-online">last time online: {lastTimeOnlineString}</div>
            }
        }

        return <div className="stars profile-page">
            <div className="twinkling">
                <BurgerMenu/>
                <MobileMenu activeTab={this.state.activeTab} toggle={this.toggle}/>
                <Container className="profile-header">
                    <div className="profile-header__background">
                        <img className="profile-header__background-img"
                             src={this.props.user_info.background}
                             alt=""/>
                    </div>
                    <div className="profile-header__profile-info profile-info">
                        <div className="profile-info__logo">
                            <img src={this.props.user_info.avatar} className="profile-info__logo-img"/>
                        </div>
                        {isOnline}
                        {lastTimeOnline}
                        <div className="profile-info__content">
                            <div className="profile-info__name">
                                <div className="profile-info__username">
                                    {this.props.user_info.firstName} {this.props.user_info.lastName}
                                </div>
                                <div className="profile-info__nickname">
                                    @{this.props.user_info.nickname}
                                </div>
                            </div>
                            {FollowOrEditProfileButton}
                            <div className="profile-info__tabs">
                                <ProfileTabs activeTab={this.state.activeTab} toggle={this.toggle}/>
                            </div>
                        </div>
                    </div>

                </Container>
                <ModalEditProfile isOpen={this.state.modalIsOpen} onClose={this.closeModal}/>
                <Container style={{display: "flex", flexDirection: "column"}}>
                    <Row>
                        <div className="col-9 profileContent">
                            <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                    {this.state.activeTab === '1' ? <div>
                                        {this.props.user_info.id === this.props.user.id ?
                                            <div>
                                                <PostForm/>
                                                <AllPost user_id={this.props.user_info.id}/>
                                            </div> :
                                            this.state.userProfileLoaded &&
                                            <AllPost user_id={this.props.user_info.id}/>}
                                    </div> : ''}
                                </TabPane>
                                <TabPane tabId="2">
                                    {this.state.activeTab === '2' ? <Followers/> : ''}
                                </TabPane>
                                <TabPane tabId="3">
                                    {this.state.activeTab === '3' ? <Following/> : ''}
                                </TabPane>
                                <TabPane tabId="4">
                                    {this.state.activeTab === '4' ? <Groups/> : ''}
                                </TabPane>
                            </TabContent>
                        </div>
                        <div className="offset-1 col-2 profile-menu">
                            <ListOfPages/>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>;
    }
}

const mapStateToProps = state => ({
    user: state.user,
    user_info: state.user_info,
    isDeniedPermission: state.user.coords.isDeniedPermission
});

const mapDispatchToProps = dispatch => ({
    addUserAuntificatedData: payload => dispatch(addUserAuntificatedData(payload)),
    addUserData: async payload => dispatch(addUserData(payload)),
    getLocation: async () => dispatch(getLocation()),
    updateUserIsOnlineAndLocation: payload => dispatch(updateUserIsOnlineAndLocation(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
