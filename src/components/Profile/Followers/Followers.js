import React, {Component} from "react";
import './Followerss.css';
import API from "../../../utills/API";
import {connect} from "react-redux";
import SearchUserItem from '../../search-user-item'
import EmptyList from "../../common/empty-list";
import Spinner from "../../common/spinner";

class Followers extends Component {

    state = {
        users: [],
        loading: false
    };


    componentDidMount() {
        this.setState({
            loading: true
        });
        API().get(`/users/followers/${this.props.user_info.id}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.setState({
                users: resp.data,
                loading: false
            });
        }).catch(error => {
            console.log("error", error);
            this.setState({
               loading: false
            });
        });
    }

    render() {
        if (this.state.loading) {
            return <Spinner/>;
        }

        if (!this.state.users.length) {
            return (
                <EmptyList>
                    You have no Followers
                </EmptyList>
            )
        }

        return <div className="container">
            {/*{this.componentDidMount()}*/}
            <div className="row">
                <div className="col-12 searchContent pt-4">
                    {this.state.users.map((user) => {
                        return (
                            <SearchUserItem user={user}/>
                        )
                    })}
                </div>
            </div>
        </div>
    }
}


const
    mapStateToProps = state => ({
        user_info: state.user_info,
        user: state.user,
    });

export default connect(mapStateToProps, null)(Followers);


