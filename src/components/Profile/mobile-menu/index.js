import React from "react";
import classNames from "classnames";
import "./mobile-menu.css";

const MobileMenu = (props) => {
    return (
        <div className="profile-mobile-menu">
            <div className={classNames("profile-mobile-menu__item",{"profile-mobile-menu__item--active": props.activeTab === '1'})}
                 onClick={() => {
                props.toggle('1');
            }}>
                Posts
            </div>
            <div className={classNames("profile-mobile-menu__item",{"profile-mobile-menu__item--active": props.activeTab === '2'})}
                 onClick={() => {
                props.toggle('2');
            }}>
                Following
            </div>
            <div className={classNames("profile-mobile-menu__item",{"profile-mobile-menu__item--active": props.activeTab === '3'})}
                 onClick={() => {
                props.toggle('3');
            }}>
                Followers
            </div>
        </div>
    )
};

export default MobileMenu;