import React, { Component } from "react";
import { Container, Row, Col, Media } from "reactstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchAllGroups } from "../../../actions/fetchAllGroups";
import { spinnerForGroup } from "../../../actions/spinnerForGroups";
import "./Groups.scss";
import API from "../../../utills/API";
import { Redirect } from "react-router";
import Spinner from "../../common/spinner";

class AllGroups extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      currentPage: 0,
      todos: [],
      term: "",
      redirectToCreateGroup: false
    };
  }

  componentDidMount() {
    this.props.spinnerForGroup(true);
    API()
      .get(
        `/groups/users/${this.props.user_info.id}?page=${this.state.currentPage}&size=6`,
        {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        }
      )
      .then(async res => {
        const groupsPage = res.data;
        await this.props.fetchAllGroups(groupsPage);
        this.setState({
          groups: this.props.groupsPage.groups,
          pageSize: this.props.groupsPage.pageSize,
          totalGroupsCount: this.props.groupsPage.totalGroupsCount
        });
      })
      .catch(error => {
        console.log("api/groups error", error);
      });
  }

  componentWillReceiveProps(nextProps) {
    this._isMounted = false;
    if (this.props.groupsPage !== nextProps.groupsPage) {
      this._isMounted &&
        this.setState({
          groups: this.props.groupsPage.groups
        });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.groupsPage !== this.props.groupsPage ||
      nextState.redirectToCreateGroup !== this.state.redirectToCreateGroup
    );
  }

  handleCreate = () => {
    this.setState({
      redirectToCreateGroup: true
    });
    console.log("clicked!");
    console.log(this.state.redirectToCreateGroup);
  };

  onPageChanged = async pageNumber => {
    console.log(pageNumber);
    await this.setState({
      currentPage: pageNumber - 1
    });
    console.log("Current page: " + this.state.currentPage);
    API()
      .get(
        `/groups/users/${this.props.user_info.id}?page=${this.state.currentPage}&size=6`,
        {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        }
      )
      .then(async res => {
        const groupsPage = res.data;
        this.props.fetchAllGroups(groupsPage);
        await this.setState({
          groups: this.props.groupsPage.groups,
          pageSize: this.props.groupsPage.pageSize,
          totalGroupsCount: this.props.groupsPage.totalGroupsCount
        });
      })
      .catch(error => {
        console.log("api/groups error", error);
      });
  };

  render() {
    if (!this.props.groupsLoading) {
      return <Spinner />;
    }
    if (this.state.redirectToCreateGroup) {
      return <Redirect push to="groups/create" />;
    }
    let pagesCount = this.props.groupsPage.total_pages;
    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
      pages.push(i);
    }
    return (
      <Container className="groupList-page">
        <div className="groups_content">
          <Row>
            <Col>
              <div>
                {pages.map(p => {
                  return (
                    <button
                      key={p}
                      className={
                        this.state.currentPage + 1 === p
                          ? "selectedPage"
                          : "page_button "
                      }
                      onClick={() => this.onPageChanged(p)}
                    >
                      {p}
                    </button>
                  );
                })}
              </div>
              {this.props.groupsPage.groups.map(group => (
                <div key={group.id}>
                  <Link to={"/groups/" + group.id} className="group-link">
                    <div className="group">
                      <div className="groupArea1  mt-3 p-3">
                        <div className="border_bottom">
                          <img className="groupImgDiv" src={group.avatar_url} />
                          <div className="title">{group.name}</div>
                        </div>
                        <div className="group-content">
                          <p className="description">{group.description}</p>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              ))}
            </Col>
          </Row>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    user_info: state.user_info,
    groupsPage: state.groupsPage,
    groupsLoading: state.groupsLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchAllGroups: payload => dispatch(fetchAllGroups(payload)),
    spinnerForGroup: payload => dispatch(spinnerForGroup(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllGroups);
