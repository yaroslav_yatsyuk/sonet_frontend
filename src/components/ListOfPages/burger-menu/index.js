import React from "react";
import { slide as Menu } from 'react-burger-menu'
import "./burger-menu.css";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class BurgerMenu extends React.Component {

    showSettings (event) {
        event.preventDefault();
    }

    render () {
        // NOTE: You also need to provide styles, see https://github.com/negomi/react-burger-menu#styling
        return (
            <Menu>
                <Link to="/home" className="s-sidebar__nav-link burger-menu-link" href="#0">
                    <i className="fa fa-home" />
                    <em>Home</em>
                </Link>
                <Link
                    to={`/profileRedirect/${this.props.user.id}`}
                    className="s-sidebar__nav-link burger-menu-link"
                    href="#0"
                >
                    <i className="fa fa-user" />
                    <em>My Profile</em>
                </Link>
                <Link to="/groups" className="s-sidebar__nav-link burger-menu-link" href="#0">
                    <i className="fa fa-users" />
                    <em>Groups</em>
                </Link>
                <Link to="/search" className="s-sidebar__nav-link burger-menu-link" href="#0">
                    <i className="fa fa-search " />
                    <em>Search</em>
                </Link>
                <Link to="/gallery" className="s-sidebar__nav-link burger-menu-link">
                    <i className="fa fa-picture-o" />
                    <em>Gallery</em>
                </Link>
                <Link
                    to="/conversations/"
                    className="s-sidebar__nav-link burger-menu-link"
                    href="#0"
                >
                    <i className="fa fa-envelope" />
                    <em>Conversations</em>
                </Link>
                <Link to="/logout" className="s-sidebar__nav-link burger-menu-link" href="#0">
                    <i className="fa fa-sign-out" />
                    <em>Log out</em>
                </Link>
            </Menu>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

export default connect(
    mapStateToProps,
    null
)(BurgerMenu);