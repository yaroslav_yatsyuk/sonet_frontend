import "./LogoUpload.scss";
import React, { Fragment } from "react";
import { connect } from "react-redux";
import API from "../../../utills/API";
import { addGroup } from "../../../actions/addGroup";

class LogoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      name: "Choose file"
    };
    console.log(this.props.user.id);
    console.log("hello" + this.state.file);
  }

  _handleSubmit(e) {
    e.preventDefault();
    let formData = new FormData();
    formData.append("file", this.state.file);

    API()
      .post("/users/" + this.props.user.id + "/files", formData, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(res => {
        const newPhoto = res.data;
        this.props.newGroup.avatar_url = newPhoto.message;
        console.log(this.props.newGroup.avatar_url, "upload backgroud success");
      })
      .catch(function(error) {
        console.log(error.response);
      });
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        name: file.name
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div className="uploadLogo">
        <input
          className="fileInput"
          name="fileInput"
          id="fileInput"
          type="file"
          onChange={e => this._handleImageChange(e)}
        />
        <label className="uploadPhoto" htmlFor="fileInput">
          {this.state.name}
        </label>
        <button
          className="submitButton"
          type="submit"
          onClick={e => this._handleSubmit(e)}
        >
          Upload Image
        </button>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user,
    newGroup: state.newGroup
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addGroup: payload => dispatch(addGroup(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoUpload);
