import "./BackgroundUpload.scss";
import React, { Fragment } from "react";
import axios from "axios";
import { connect } from "react-redux";
import API from "../../../utills/API";
import { addGroup } from "../../../actions/addGroup";

class BackgroundUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file1: "",
      imagePreviewUrl1: "",
      name1: "Choose file"
    };
  }

  _handleSubmit(e) {
    e.preventDefault();
    let formData = new FormData();
    formData.append("file", this.state.file1);
    formData.append("name", this.state.file1.name1);

    API()
      .post("/users/" + this.props.user.id + "/files", formData)
      .then(res => {
        const newPhoto = res.data;
        this.props.newGroup.backgroung_url = newPhoto.message;
        console.log(
          this.props.newGroup.backgroung_url,
          "upload backgroud success"
        );
      })
      .catch(function(error) {
        console.log(error.response);
      });
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file1: file,
        name1: file.name
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div className="upload">
        <input
          className="fileInput1"
          name="fileInput1"
          id="fileInput1"
          type="file"
          onChange={e => this._handleImageChange(e)}
        />
        <label className="uploadPhoto" htmlFor="fileInput1">
          {this.state.name1}
        </label>
        <button
          className="submitButton"
          type="submit"
          onClick={e => this._handleSubmit(e)}
        >
          Upload Image
        </button>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user,
    newGroup: state.newGroup
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addGroup: payload => dispatch(addGroup(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BackgroundUpload);
