import React, {Component} from "react";
import {Container, Row, Col} from "reactstrap";
import "./newGroup.scss";
import ListOfPages from "../ListOfPages/ListOfPages";
import LogoUpload from "./UploadPhoto/LogoUpload";
import BackgroundUpload from "./UploadPhoto/BackgroundUpload";
import {connect} from "react-redux";
import {addGroup} from "../../actions/addGroup";
import {fetchGroup} from "../../actions/fetchGroup";
import API from "../../utills/API";

class newGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToGroupList: false
        };
        /*  console.log(initialState.newGroup);*/
        this.onNameChange = this.onNameChange.bind(this);
        this.onNicknameChange = this.onNicknameChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            name: props.newGroup ? props.newGroup.name : "",
            nickname: props.newGroup ? props.newGroup.nickname : "",
            description: props.newGroup ? props.newGroup.description : "",
            creatorId: props.newGroup ? this.props.user.id : "",
            avatar_url: props.newGroup ? props.newGroup.avatar_url : "",
            background_url: props.newGroup ? props.newGroup.background_url : ""
        };
    }

    fileSelectedHandler = event => {
        console.log(event);
    };

    onNameChange = e => {
        const name = e.target.value;
        this.props.newGroup.name = name;
    };

    onNicknameChange = e => {
        const nickname = e.target.value;
        this.props.newGroup.nickname = nickname;
    };

    onDescriptionChange = e => {
        const description = e.target.value;
        this.props.newGroup.description = description;
    };

    onSubmit(e) {
        e.preventDefault();
        console.log(this.props.user.id);

        const newGroup = {
            name: this.props.newGroup.name,
            nickname: this.props.newGroup.nickname,
            description: this.props.newGroup.description,
            creator_id: this.props.user.id,
            avatar_url: this.props.newGroup.avatar_url,
            background_url: this.props.newGroup.background_url
        };
        console.log(newGroup);
        API()
            .post("/groups/create", newGroup, {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(res => {
                const groupToCreate = res.data;
                console.log(groupToCreate, "groupToCreate");
                this.props.addGroup(groupToCreate);
            });
    }

    render() {
        console.log(this.props.user);
        return (
            <div className="stars">
                <div className="twinkling">
                    <div className="newGroup">
                        <Container style={{display: "flex", flexDirection: "column"}}>
                            <div className="backgroundProfileImgDiv">
                                <img
                                    className="backgroundProfileImg"
                                    src="https://sonet-social-bucket.s3.amazonaws.com/17/ca2ce75dd4b344f2b06da0e2a4d6c084"
                                    alt=""
                                />
                            </div>
                            <div className="profileHeader">
                                <Row className="profileHeaderGrid">
                                    <Col xs="6" sm="4" md="2" className="profile_logo_col">
                                        <div className="profile_logo_container">
                                            <img
                                                src="https://sonet-social-bucket.s3.amazonaws.com/17/0a4638dbdd384f10b1453feb0094faec"
                                                className="profile_avatar"
                                                alt="Avatar"
                                            />
                                        </div>
                                    </Col>
                                    <Col xs={{sise: 12, offset: 1}} sm="7" md="9">
                                        <Row>
                                            <Col xs="12" lg="7">
                                                <div className="profileNames">
                                                    <span>{this.props.user.firstName}</span>
                                                    <span>{this.props.user.lastName}</span>
                                                    <br/>
                                                    <span>{this.props.user.nickname}</span>
                                                </div>
                                            </Col>
                                            <Col xs="3" lg="2"/>
                                            <Col xs="9" lg="3"/>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                            <Row>
                                <Col xs="10" lg="9">
                                    <div className="form_style">
                                        <span className="new_group">New group</span>
                                        <form
                                            onSubmit={this.onSubmit}
                                            ref={input => (this.textInput = input)}
                                            className="group_form"
                                        >
                                            <span className="group_name">Name:</span>
                                            <input
                                                className="group_input"
                                                type="text"
                                                onChange={this.onNameChange}
                                            />
                                            <br/>
                                            <span className="group_nickname">Nickname:</span>
                                            <input
                                                className="group_input"
                                                type="text"
                                                onChange={this.onNicknameChange}
                                            />
                                            <br/>
                                            <span className="group_description">Description:</span>
                                            <input
                                                className="group_input"
                                                type="text"
                                                onChange={this.onDescriptionChange}
                                            />
                                            <span className="group_logo">Logo image:</span>
                                            <LogoUpload/>
                                            <span className="group_background">
                        Background image:
                      </span>
                                            <div>
                                                <BackgroundUpload/>
                                            </div>
                                            <button type="submit" className="create">
                                                SUBMIT
                                            </button>
                                        </form>
                                        <br/>
                                    </div>
                                </Col>
                                <Col xs="2" lg="3">
                                    <div className="listOfPages">
                                        <ListOfPages/>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        newGroup: state.newGroup
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addGroup: payload => dispatch(addGroup(payload)),
        fetchGroup: payload => dispatch(fetchGroup(payload))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(newGroup);
