import React, {Component} from 'react';
import './search-user-item.css';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import API from "../../utills/API";
import classNames from "classnames";

class SearchUserItem extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        isFollowed: false
    };

    componentDidMount() {
        API().get(`/users/follow?followerId=${this.props.logged_user.id}&followingId=${this.props.user.id}`, {
            headers: {
                Authorization: `Bearer_${this.props.logged_user.token}`
            }
        }).then(resp => {
            this.setState(
                {isFollowed: true})
        })
            .catch(error => {
                console.log("error", error);
            });
    }

    follow = () => {
        API().post(`/users/follow?`, {
            followerId: this.props.logged_user.id,
            followingId: this.props.user.id
        }, {
            headers: {
                Authorization: `Bearer_${this.props.logged_user.token}`
            }
        }).then(resp => {
            this.setState(
                {isFollowed: true})
        })
            .catch(error => {
                console.log("error", error);
            });
    };


    unfollow = () => {
        API().delete(`/users/unfollow`,
            {
                data: {
                    followerId: this.props.logged_user.id,
                    followingId: this.props.user.id
                },
                headers: {
                    Authorization: `Bearer_${this.props.logged_user.token}`
                }
            }).then(resp => {
            this.setState(
                {isFollowed: false})
        })
            .catch(error => {
                console.log("error", error);
            });
    };

    render() {
        let distance;
        if (this.props.user.distance) {
            distance =  (
                <span className="user-distance">
                    {this.props.user.distance} m
                </span>
            );
        }
        return (
              <div className="searchUserItemStyle mt-2">
                    <div className="d-flex justify-content-start ">
                        <div className="userDiv container">
                            <div className="row userText d-flex flex-row">
                                <div className="col-3 col-md-2 col-lg-1 icon-profile">
                                    <Link to={`/profileRedirect/${this.props.user.id}`} className="" href="#0">
                                        <img src={this.props.user.avatar.url} alt=""/>
                                    </Link>
                                </div>
                                <div className="col-6">
                                    <Link to={`/profileRedirect/${this.props.user.id}`} className="" href="#0">
                                        <span className="UserName">@{this.props.user.nickname} </span>
                                        <span className={classNames("user-is-online", {"user-is-online__online": this.props.user.isOnline})}></span>
                                        {distance}
                                        <br/>
                                        <span
                                            className="UserName">{this.props.user.firstName} {this.props.user.lastName}</span>
                                    </Link>
                                    <br/>
                                </div>
                                <div className="offset-lg-2 col-2">
                                    {this.props.logged_user.id !== this.props.user.id ? this.state.isFollowed ? <button className="btn btn-info searchUserButton"
                                                                                                                         onClick={this.unfollow}>Unfollow</button> :
                                            <button className="btn btn-info searchUserButton"
                                                    onClick={this.follow}>Follow</button> : ''}


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        )
    }

}

const mapStateToProps = state => ({
    logged_user: state.user
});


export default connect(mapStateToProps, null)(SearchUserItem);
