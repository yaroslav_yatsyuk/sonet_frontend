import React from 'react';
import './edit-post.css';
import {getDate} from "../../../utills/date";
import {makeStyleToDiv} from "../../../utills/images-utils";
import ImagesEdit from "../images-edit/images-edit";
import ImageUpload from "../image-upload";
import API from "../../../utills/API";
import {updatePost} from "../../../actions/updatePost";
import connect from "react-redux/es/connect/connect";
import Exception from "../exceptions";
import updateNews from "../../../actions/updateNews";
import updateTopPost from "../../../actions/updateTop";
import PollList from "../../poll/poll-list/poll-list";
import fetchAllPolls from "../../../actions/fetchAllPolls";


class EditPost extends React.Component {

    state = {
        images_files: [],
        prev_images: this.props.post.images,
        urls: this.props.post.images,
        text: this.props.post.text,
        checkPostNotEmpty : true,
        moreThenTenPhotos: false,
        polls: []
    };


    componentWillMount() {
        API().get(`/polls/posts/${this.props.post.id}`,
            {headers: {
                    Authorization : `Bearer_${this.props.user.token}`
                }
            }) .then(resp => {
                console.log("eeeeeeeee" , resp.data);
                const payload = {
                    post_id: this.props.post.id,
                    polls: resp.data
                };
                this.props.fetchAllPolls(resp.data);
                this.setState({
                    polls: resp.data
                });
                console.log("state",this.state)
            }
        );
    }


    handleEdit = async(e) => {
        e.preventDefault();
        if((this.state.text.replace(/\s/g, '') === '') && (this.state.prev_images.length === 0)) {
            await this.setState({
                ...this.state,
                checkPostNotEmpty: false
            });
        }
        if (this.state.checkPostNotEmpty === true) {
            if (this.state.images_files.length === 0) {
                const data = {
                    id: this.props.post.id,
                    text: this.state.text,
                    images: this.state.urls
                };
                this.makePost(data);
            } else {
                this.processImages(this.state.images_files, 0);
            }
        }
    };


    processImages(images, index) {
        if (index < images.length - 1) {
            let formData = new FormData();
            formData.append('file', images[index].file);
            let self = this;
            let callback = function (imageUrl) {
                self.setState(prevState => ({
                    urls: [...prevState.urls, imageUrl]
                }));
                self.processImages(images, ++index);
            };
            this.saveImage(formData, callback);
        } else if (index === (images.length - 1)) {
            let formData = new FormData();
            formData.append('file', images[index].file);
            let self = this;
            let callback = function (imageUrl) {
                self.setState(prevState => ({
                    urls: [...prevState.urls, imageUrl]
                }));
                const data = {
                    id: self.props.post.id,
                    text: self.state.text,
                    images: self.state.urls,
                };
                self.makePost(data);
            };
            this.saveImage(formData, callback);
        }
    }

    saveImage(formData, callback) {
        API().post(`/users/${this.props.user.id}/files`, formData, { headers: {
                Authorization : `Bearer_${this.props.user.token}`
            }} )
            .then(resp => {
                console.log(resp.data.message);
                if (callback) {
                    callback(resp.data.message);
                }
            });
    }



    makePost = (data) => {
        API().put('/posts', data, { headers: {
                Authorization : `Bearer_${this.props.user.token}`
            }} )
            .then(resp => {
                switch (this.props.current) {
                    case "news":
                    {
                        this.props.updateNews(resp.data);
                    }
                    case "all-post": {
                        this.props.updatePost(resp.data);
                    }
                    case "top-news": {
                        this.props.updateTopPost(resp.data);
                    }
                }
            })
    };


    upload = (file) => {
        this.setState(({
            images_files: this.state.images_files.concat(file),
            prev_images: this.state.prev_images.concat(file.imagePreviewUrl),
            checkPostNotEmpty: true
        }));
    };


    deletePhoto = async (url) => {
        await this.setState({
            images_files: this.state.images_files.filter((image_file)=>
                image_file.imagePreviewUrl !== url),
            prev_images: this.state.prev_images.filter((prev_image)=>
                prev_image !== url),
            urls: this.state.urls.filter((url_img)=>
                url_img !== url),
            moreThenTenPhotos: false,
        });
    };


    onChangeText = (e) => {
        this.setState({
                ...this.state,
                checkPostNotEmpty: true,
                text: e.target.value
            }
        )
    };


    checkPhoto = (check) => {
        this.setState({
                ...this.state,
                moreThenTenPhotos: check
            }
        )
    };



    render() {
        return (
            <form className="form" onSubmit={this.handleSubmit}>
                <div className="new-post">
                    <div className="logo">
                        <div className="logo-user">
                            <img src={this.props.post.avatar_url}/>
                        </div>
                    </div>
                    <div className="info">
                        <div className="info-about-user">
                            <div className="info-nickname">
                                <p className="nickname p-maryna">@{this.props.post.nickname}</p>
                                <p className="name-surname p-maryna">{this.props.post.first_name} {this.props.post.last_name}</p>
                            </div>
                            <div className="date">
                                <p className="p-maryna">{getDate(new Date(this.props.post.creation_time))}</p>
                            </div>
                        </div>
                        <div className="post">
                           <textarea className="textarea" rows="5" ref={(input)=>this.getText = input} cols="28"
                                     placeholder="Hey, how you doing? " defaultValue={this.props.post.text}
                                     onChange={this.onChangeText}
                           />
                            {this.state.checkPostNotEmpty ?  <div> </div> : <Exception message={"Enter some message"} /> }
                            <div className="images">
                                <div className={makeStyleToDiv(this.state.prev_images.length)}>
                                    <ImagesEdit
                                        images={this.state.prev_images} deletePhoto = {this.deletePhoto}/>
                                    {this.state.moreThenTenPhotos ? <Exception message={"You can`t apply more then 10 photo in one post"}
                                    /> : <div> </div>}
                                </div>
                            </div>
                            <div className="polls-post">
                                <PollList post={this.props.post} polls = {this.state.polls} editing={true} />
                            <br />
                            </div>
                        </div>

                        <div className="add-post-func">
                            <div className="button-add-photo">
                                <div className="form-group">
                                    <ImageUpload upload = {this.upload} size = {this.state.prev_images.length} checkPhoto = {this.checkPhoto}/>
                                </div>
                            </div>
                            <div className="make-poll">
                                <img src={require('./poll.png')} onClick={this.makePoll} />
                            </div>
                            <div className="make-post">
                                <button id = "post-button" className="square_btn update-btn" onClick={this.handleEdit}>
                                    UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return{
        updatePost: (payload) => dispatch(updatePost(payload)),
        updateNews: (payload) => dispatch(updateNews(payload)),
        updateTopPost: (payload) => dispatch(updateTopPost(payload)),
        fetchAllPolls: (payload) => dispatch(fetchAllPolls(payload))
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(EditPost);
