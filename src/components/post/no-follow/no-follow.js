import React from 'react';
import './no-follow.css';


const NoFollow = () => {
    return (
        <div className="no-follow-news">
            <p>You don`t follow anyone</p>
            <div className="no-follow-news-image">
                <img src={require('./no-follow.png')}/>
            </div>
        </div>
    )
};


export default NoFollow;
