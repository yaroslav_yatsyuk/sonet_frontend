import React from 'react';
import {getDate} from "../../../utills/date";
import API from "../../../utills/API";
import {makeStyleToDiv} from "../../../utills/images-utils";
import ImagesEdit from "../images-edit/images-edit";
import addPost from "../../../actions/addPost";
import {connect} from "react-redux";
import ImageUpload from "../image-upload";
import './post-form.css';
import PollForm from "../../poll/poll-form/poll-form";
import Exception from '../exceptions/exception';
import Spinner from "../../common/spinner";
import {Button, Form, Input} from "reactstrap";
import addPoll from "../../../actions/addPoll";


class PostForm extends React.Component {

    state = {
        images_files: [],
        prev_images: [],
        urls: [],
        text: '',
        poll: false,
        checkPostNotEmpty: true,
        moreThenTenPhotos: false,
        post_id: null,
        question: '',
        inputs: ['input-0'],
        variants: []
    };

    handleSubmit = async (e) => {
        e.preventDefault();
        //this.props.user.post.loadingAddPosts = true;
        if ((this.state.text.replace(/\s/g, '') === '') && (this.state.prev_images.length === 0)) {
            await this.setState({
                ...this.state,
                text: '',
                checkPostNotEmpty: false,
            });
            this.props.user.post.loadingAddPosts = false;
        }
        if (this.state.checkPostNotEmpty === true) {
            this.props.user.post.loadingAddPosts = true;
            if (this.state.images_files.length === 0) {
                const data = {
                    creator_id: this.props.user.id,
                    owner_id: this.props.user.id,
                    text: this.state.text,
                    images: [],
                };
                this.makePost(data);
                this.setState({
                    ...this.state,
                    text: ''
                });
            } else {
                this.processImages(this.state.images_files, 0);
            }
        }
    };

    appendInput() {
        let newInput = `input-${this.state.inputs.length}`;
        this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
    }

    processImages(images, index) {
        if (index < images.length - 1) {
            let formData = new FormData();
            formData.append('file', images[index].file);
            let self = this;
            let callback = function (imageUrl) {
                self.setState(prevState => ({
                    urls: [...prevState.urls, imageUrl]
                }));
                self.processImages(images, ++index);
            };
            this.saveImage(formData, callback);
        } else if (index === (images.length - 1)) {
            let formData = new FormData();
            formData.append('file', images[index].file);
            let self = this;
            let callback = function (imageUrl) {
                self.setState(prevState => ({
                    urls: [...prevState.urls, imageUrl]
                }));
                const data = {
                    creator_id: self.props.user.id,
                    owner_id: self.props.user.id,
                    text: self.state.text,
                    images: self.state.urls,
                };
                self.makePost(data);
            };
            this.saveImage(formData, callback);
        }
    }

    saveImage(formData, callback) {
        API().post(`/users/${this.props.user.id}/files`, formData,{ headers: {
                Authorization : `Bearer_${this.props.user.token}`
            }} )
            .then(resp => {
                console.log(resp.data.message);
                if (callback) {
                    callback(resp.data.message);
                }
            });
    }


    makePost = async (data) => {
        let d;
        await API().post('/posts', data,
            { headers: {
                    Authorization : `Bearer_${this.props.user.token}`
                }} )
            .then(resp => {
               /* this.props.addPost({
                    id: resp.data.id,
                    creator_id: resp.data.creator_id,
                    text: resp.data.text,
                    nickname: resp.data.nickname,
                    first_name: resp.data.first_name,
                    last_name: resp.data.last_name,
                    avatar_url: resp.data.avatar_url,
                    images: resp.data.images,
                    update_time: resp.data.update_time,
                    creation_time: resp.data.creation_time,
                    editing: false,
                    comments: []
                });*/
                d = {
                   id: resp.data.id,
                   creator_id: resp.data.creator_id,
                   text: resp.data.text,
                   nickname: resp.data.nickname,
                   first_name: resp.data.first_name,
                   last_name: resp.data.last_name,
                   avatar_url: resp.data.avatar_url,
                   images: resp.data.images,
                   update_time: resp.data.update_time,
                   creation_time: resp.data.creation_time,
                   editing: false,
                   comments: [],
                   polls: []
               }
                this.setState({
                    post_id: resp.data.id
                });
            });
        this.setState({
                images_files: [],
                prev_images: [],
                urls: [],
                text: '',
                poll: false,
                checkPostNotEmpty: true,
                moreThenTenPhotos: false,
            }
        );
            const datapolls = {
                question: this.state.question,
                creator_id: this.props.user.id,
                post_id: this.state.post_id,
                choices: this.state.variants
            };
            if(datapolls.question !== '') {
                console.log("create CREATE CREATE");
                API().post('/polls', datapolls,
                    {
                        headers: {
                            Authorization: `Bearer_${this.props.user.token}`
                        }
                    })
                    .then(resp => {
                        const payload = {
                            polls: resp.data,
                            post_id: this.state.post_id
                        };
                        console.log("psyp", payload);
                        this.props.addPost(d);
                        this.props.addPoll(payload);
                        this.setState({
                            variants: []
                        });
                    });
            }else{
                this.props.addPost(d);
            }
    };


    upload = (file) => {
        this.setState(({
            ...this.state,
            images_files: this.state.images_files.concat(file),
            prev_images: this.state.prev_images.concat(file.imagePreviewUrl),
            checkPostNotEmpty: true
        }));
    };


    deletePhoto = (url) => {
        this.setState({
            ...this.state,
            images_files: this.state.images_files.filter((image_file) =>
                image_file.imagePreviewUrl !== url),
            prev_images: this.state.prev_images.filter((prev_image) =>
                prev_image !== url),
            moreThenTenPhotos: false
        });
    };



    makePoll = () => {
        this.setState({
                ...this.state,
                poll: true
            }
        )
    };


    onChangeText = (e) => {
        this.setState({
            ...this.state,
            checkPostNotEmpty: true,
            text: e.target.value
            }
        )
    };

    onChangeVariant = (e) => {

        console.log(e.target.value);
        if(e.target.value.length === 1) {
            this.setState( {
                variants: this.state.variants.concat(e.target.value)
            })
        }

        else {
            const str = e.target.value.slice(0, e.target.value.length - 1);
            let index = this.state.variants.indexOf(str);
            if (index !== -1) {
                this.setState({
                    variants: this.state.variants.map(variant => {
                        if (variant === str) {
                            variant = e.target.value;
                        }
                        return variant;
                    })
                });
            }
        }
        };


    checkPhoto = (check) => {
        this.setState({
            ...this.state,
            moreThenTenPhotos: check
            }
        )
    };

    onChangeQuetion = (e) => {
        console.log(e.target.value);
        this.setState({
            question: e.target.value
        })
    };

    deletePoll = (e) => {

        console.log("delete");
        this.setState({
            poll: false,
            checkPostNotEmpty: true,
            moreThenTenPhotos: false,
            post_id: null,
            question: '',
            inputs: ['input-0'],
            variants: []
        })
    };

    render() {

        if(this.props.user.post.loadingAddPosts) {
            return (
                <div className="spinner-container-form">
                    <div className="spinner-form">
                        <Spinner />
                    </div>
                </div>
            )
        }

        return (
            <form className="form" onSubmit={this.handleSubmit}>
    <div className="new-post">
        <div className="logo">
            <div className="logo-user">
                <img src={this.props.user_info.avatar}/>
            </div>
        </div>
        <div className="info">
            <div className="info-about-user">
                <div className="info-nickname">
                    <p className="nickname p-maryna ">@{this.props.user.nickname}</p>
                    <p className="name-surname p-maryna ">{this.props.user.firstName} {this.props.user.lastName}</p>
                </div>
                <div className="date">
                    <p className="p-maryna">{getDate(new Date())}</p>
                </div>
            </div>
            <div className="post">
                <textarea className="textarea" rows="5" ref={(input)=>this.getText = input} cols="28"
                          value={this.state.text}
                              placeholder="Hey, how you doing? " onChange={this.onChangeText}
                    />
                {this.state.checkPostNotEmpty ?  <div> </div> : <Exception message={"Enter some message"} /> }
                <div className="poll"> {
                    this.state.poll ?
                        <div className="poll-container">
                            <a onClick={this.deletePoll}> <span className="close"> </span> </a>
                            <Input placeholder="Enter question" onChange = {this.onChangeQuetion} required/>
                            <Form className="form-poll">
                                <label
                                    className="field a-field a-field_a2 page__field">
                                    <input className="field__input a-field__input" placeholder="Enter choice"
                                           required   onChange = {this.onChangeVariant} />
                                    <span className="a-field__label-wrap">
                                   <span className="a-field__label">Enter choice</span>
                                  </span>
                                </label>
                                <div id="dynamicInput">
                                    {this.state.inputs.map(input => <label
                                        className="field a-field a-field_a2 page__field">
                                        <input className="field__input a-field__input" placeholder="Enter choice"
                                               required  onChange = {this.onChangeVariant}  />
                                     <span className="a-field__label-wrap">
                                    <span className="a-field__label">Enter choice</span>
                                          </span>
                                    </label>
                                    )}
                                </div>
                            </Form>
                            <div className="button-add-choice">
                                <Button id="add-choice" onClick={ () => this.appendInput() } >+</Button>
                            </div>
                        </div>:
                        <div> </div>
                }</div>
                <div className="images">
                    <div className={makeStyleToDiv(this.state.prev_images.length)}>
                        <ImagesEdit
                            images={this.state.prev_images} deletePhoto = {this.deletePhoto}/>
                        {this.state.moreThenTenPhotos ? <Exception message={"You can`t apply more then 10 photo in one post"}
                        /> : <div> </div>}
                    </div>
                </div>
            </div>
            <div className="add-post-func">
                <div className="button-add-photo">
                    <div className="form-group">
                        <ImageUpload upload = {this.upload} size = {this.state.prev_images.length} checkPhoto = {this.checkPhoto}/>
                    </div>
                </div>
                <div className="make-poll">
                    <img src={require('./poll.png')} onClick={this.makePoll} />
                </div>
            <div className="make-post">
                <button id = "post-button" className="square_btn">
                    POST</button>
            </div>
            </div>
        </div>
    </div>
            </form>
        )
    }

}

    const mapStateToProps = (state) => {
        return {
        user: state.user,
        user_info: state.user_info
    };
    };

    const mapDispatchToProps = (dispatch) => {
        return{
        addPost: (payload) => dispatch(addPost(payload)),
            addPoll: (payload) => dispatch(addPoll(payload))
    }
    };

    export default connect(mapStateToProps, mapDispatchToProps)(PostForm);
