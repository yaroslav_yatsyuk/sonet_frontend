import {fetchAllNews} from "../../../actions/fetchAllNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import NoPost from "../no-post";
import Post from "../post";
import addNews from "../../../actions/addNews";
import newsLoaded from "../../../actions/newsLoaded";
import clearNews from "../../../actions/clearNews";
import clearAllNews from "../../../actions/clearAllNews";
import './news.css';


class News3 extends React.Component {

    state = {
        page: 0,
        search: this.props.searchNickname
    };

   loadPost = () => {
           API().get(`posts/users/${this.props.user.id}/news?page=${this.state.page}&size=3&sort=creationTime,desc`,
               {
                   headers: {
                       Authorization: `Bearer_${this.props.user.token}`
                   }
               })
               .then(resp => {
                       console.log(resp.data);
                       if (this.props.user.new.news.length === 0) {
                           this.props.fetchAllNews(
                               resp.data.map((post) => {
                                       post.editing = false;
                                       post.comments = [];
                                       return post;
                                   }
                               ));
                       } else {
                           if(this.props.user.new.isLoaded) {
                               console.log("hide");
                           }else {
                               if(resp.data.length === 0){
                                   this.props.newsLoaded(true);
                               }else {
                                   this.props.addNews(
                                       resp.data.map((post) => {
                                               post.editing = false;
                                               post.comments = [];
                                               return post;
                                           }
                                       ));
                               }
                           }
                       }
                   }
               );
   };

   loadPostWithText = async () => {
       await this.props.fetchAllNews([]);
       console.log("load post with text");
       console.log(`posts/find?text=${this.props.searchNickname}&page=${this.state.page}&size=3&sort=creationTime,desc`);
       API().get(`posts/find?text=${this.props.searchNickname}&page=${this.state.page}&size=3&sort=creationTime,desc`,
           {
               headers: {
                   Authorization: `Bearer_${this.props.user.token}`
               }
           })
           .then(resp => {
                   console.log(resp.data);
                   if (this.props.user.new.news.length === 0) {
                       this.props.fetchAllNews(
                           resp.data.map((post) => {
                                   post.editing = false;
                                   post.comments = [];
                                   return post;
                               }
                           ));
                   } else {
                       if(this.props.user.new.isLoaded) {
                           console.log("hide");
                       }else {
                           if(resp.data.length === 0){
                               this.props.newsLoaded(true);
                           }else {
                               this.props.addNews(
                                   resp.data.map((post) => {
                                           post.editing = false;
                                           post.comments = [];
                                           return post;
                                       }
                                   ));
                           }
                       }
                   }
               }
           );
   };

    componentDidMount() {
        if(this.props.user.new.news.length === 0) {
            this.loadPost();
        }else if(this.state.search !== '') {
         this.loadPostWithText();
        }
        else {
            this.props.clearNews(false);
            this.setState({page:1});
        }
    };

    showMore = async () => {
        if(this.state.search === '') {
            if (this.props.user.new.isLoaded === false) {
                await this.setState({
                    page: ++this.state.page
                });
                this.loadPost();
            }  else {
                this.props.clearNews(false);
                this.setState({page: 1});
            }
        }else {
            if (this.props.user.new.isLoaded === false) {
                await this.setState({
                    page: ++this.state.page
                });
                this.loadPostWithText();
            }  else {
                this.props.clearNews(false);
                this.setState({page: 1});
            }
        }
    };

    search = () => {
        console.log("search", this.state.search);
        this.setState({page:0});
        this.loadPostWithText();
    };



    render() {
        if(this.props.user.new.loadingNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }
        if(this.props.searchNickname !== this.state.search){
            this.setState({search :this.props.searchNickname});
            this.search();
        }

        console.log("news prop", this.props.searchNickname);
        console.log("news state", this.state.search);
        return (
            <div>
                {this.props.user.new.news.length === 0 ? <NoPost/> : this.props.user.new.news.map((post) => (
                    <div key={post.id}>
                            <Post post={post} key={post.id} isNews = {true} />
                    </div>
                ))}
                <div className="bottom-news">
                    <button className="btn btn-primary show-more" onClick={this.showMore}>{this.props.user.new.isLoaded ?
                        <div>Hide</div> : <div>Show more</div>}
                    </button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllNews: (data) => dispatch(fetchAllNews(data)),
        addNews: (data) => dispatch(addNews(data)),
        newsLoaded: (data) => dispatch(newsLoaded(data)),
        clearNews: (data) => dispatch(clearNews(data))/*,
        clearAllNews: (data) => dispatch(clearAllNews(data))*/
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(News3);
