import {fetchAllNews} from "../../../actions/fetchAllNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import Post from "../post";
import addNews from "../../../actions/addNews";
import newsLoaded from "../../../actions/newsLoaded";
import clearNews from "../../../actions/clearNews";
import './news.css';
import EditPost from "../edit-post";
import NoFollow from "../no-follow";


class News extends React.Component {

    state = {
        page: 0
    };

   loadPost = () => {
           API().get(`posts/users/${this.props.user.id}/news?page=${this.state.page-1}&size=3&sort=creationTime,desc`,
               {
                   headers: {
                       Authorization: `Bearer_${this.props.user.token}`
                   }
               })
               .then(resp => {
                       if (this.props.user.new.news.length === 0) {
                           this.props.fetchAllNews(
                               resp.data.map((post) => {
                                       post.editing = false;
                                       post.comments = [];
                                       return post;
                                   }
                               ));
                       } else {
                           if(this.props.user.new.isLoaded) {
                               console.log("hide");
                           }else {
                               if(resp.data.length === 0){
                                   this.props.newsLoaded(true);
                               }else {
                                   this.props.addNews(
                                       resp.data.map((post) => {
                                               post.editing = false;
                                               post.comments = [];
                                               return post;
                                           }
                                       ));
                               }
                           }
                       }
                   }
               );
   };

    componentDidMount() {
        if(this.props.user.new.news.length === 0) {
            this.loadPost();
        }
        else {
            if(this.props.afterSearch) {
                this.props.fetchAllNews([]);
                this.loadPost();
            }else {
                this.props.clearNews(false);
                this.setState({page: 1});
            }
        }
    };


    componentWillMount() {
            this.loadPost();
    }

    showMore = async () => {
            if (this.props.user.new.isLoaded === false) {
                await this.setState({
                    page: ++this.state.page
                });
                this.loadPost();
            }  else {
                this.props.clearNews(false);
                this.setState({page: 1});
            }
    };


    render() {
        if(this.props.user.new.loadingNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }


        return (
            <div>
                {this.props.user.new.news.length === 0 ? <NoFollow/> : this.props.user.new.news.map((post) => (
                    <div key={post.id}>
                        {post.editing ? <EditPost post={post} key={post.id}/> :
                            <Post post={post} key={post.id} current = {"news"} isNews={false}/>
                        }
                    </div>
                ))}
                {this.props.user.new.news.length === 0 ? <div> </div> :
               <div className="bottom-news">
                    <button className="btn btn-primary show-more" onClick={this.showMore}>{this.props.user.new.isLoaded ?
                        <div>Hide</div> : <div>Show more</div>}
                    </button>
                </div> }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllNews: (data) => dispatch(fetchAllNews(data)),
        addNews: (data) => dispatch(addNews(data)),
        newsLoaded: (data) => dispatch(newsLoaded(data)),
        clearNews: (data) => dispatch(clearNews(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(News);
