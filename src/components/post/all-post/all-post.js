import React, {Component} from 'react';
import './all-post.css';
import Post from '../post';
import '../edit-post/edit-post.css';
import EditPost from "../edit-post";
import NoPost from "../no-post";
import connect from "react-redux/es/connect/connect";
import Spinner from "../../common/spinner";
import {fetchAllPost} from "../../../actions/fetchAllPost";
import API from "../../../utills/API";
import postsLoaded from "../../../actions/postsLoaded";
import clearPosts from "../../../actions/clearPosts";
import addPosts from "../../../actions/addPosts";
import loadedPosts from "../../../actions/loadedPost";
import PollList from "../../poll/poll-list/poll-list";



class AllPost extends Component {


    componentDidMount() {
        /*if(this.state.id === this.props.user_id){
            this.setState({
                id: this.props.user_id
            })
        }
        console.log("ccc");*/
        API().get(`/posts/users/${this.props.user_id}?sort=creationTime,desc`, {headers: {
                Authorization : `Bearer_${this.props.user.token}`
            }
        })
            .then(resp => {
                    console.log(resp.data);
                    this.props.fetchAllPost(
                        resp.data.map((post) => {
                                post.editing = false;
                                post.polls = [];
                                post.comments = [];
                                return post;
                            }
                        ));
                }
            );
    };


    render() {

        if(this.props.user.post.loadingAllPosts) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }

        return (
            <div>
                {this.props.user.post.posts.length === 0 ? <NoPost/> : this.props.user.post.posts.map((post) => (
                    <div key={post.id}>
                        { post.editing ?
                            <EditPost post={post} key={post.id} current = {"all-post"}/> :
                            <Post post={post} key={post.id} current = {"all-post"} isNews = {false} /> }
                    </div>
                ))}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllPost: (data) => dispatch(fetchAllPost(data)),
        loadedPosts : (data) => dispatch(loadedPosts(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(AllPost);
