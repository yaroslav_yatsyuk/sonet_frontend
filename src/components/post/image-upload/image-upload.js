import React from 'react';
import './image-upload.css';


export default class ImageUpload extends React.Component {

    handleImageChange  (e) {
    e.preventDefault();
    if(this.props.size < 10) {
        let files = e.target.files;
        for (let i = 0; i < files.length; i++) {
            if(this.props.size + i < 10) {
                let reader = new FileReader();
                reader.onloadend = () => {
                    const temp = {
                        file: files[i],
                        imagePreviewUrl: reader.result
                    };
                    this.props.upload(temp);
                };

                reader.readAsDataURL(files[i]);
            }
            else {
               /* let audio = new Audio("https://sonet-social-bucket.s3.amazonaws.com/1/darth_vader.mp3");
                audio.play();*/
                this.props.checkPhoto(true);
                break;
            }
        }
    }
    else {
        e.target.files = null;
    }
}


showMessage = () => {
    /*let audio = new Audio("https://sonet-social-bucket.s3.amazonaws.com/1/darth_vader.mp3");
    audio.play();*/
    this.props.checkPhoto(true);
};

render() {

    return (
        <div className="previewComponent">
            <label  className="add-photo">
                {this.props.size < 10 ?
                    <div>
                <input type="file" name="file" className="input-file"
                       onChange={(e)=>this.handleImageChange(e)} multiple accept="image/x-png,image/gif,image/jpeg" />
                    <img src={require('./camera.png')} />
                    </div> : <a onClick={this.showMessage}><img src={require('./camera.png')} /> </a>}
            </label>
        </div>
    )
}
}
