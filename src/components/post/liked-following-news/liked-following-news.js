import fetchLikedFollowingNews from "../../../actions/fetchLikedFollowingNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import NoPost from "../no-post";
import Post from "../post";
import addLikedFollowingNews from "../../../actions/addLikedFollowingNews";
import newsLikedFollowingLoaded from "../../../actions/newsLikedFollowingLoaded";
import clearLikedFollowingNews from "../../../actions/clearLikedFollowingNews";
import EditPost from "../edit-post";


class LikedFollowingNews extends React.Component {

    state = {
        page: 0
    };

    loadPost = () => {
        API().get(`posts/${this.props.user.id}/following/liked`,
            {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(resp => {
                        this.props.fetchLikedFollowingNews(
                            resp.data.map((post) => {
                                    post.editing = false;
                                    post.comments = [];
                                    return post;
                                }
                            ));
                            if(resp.data.length === 0){
                                this.props.newsLikedFollowingLoaded(true);
                            }
                                this.props.newsLikedFollowingLoaded(true);
                }
            );
    };

    componentDidMount() {
            this.loadPost();
            this.setState({page:1});
    };

    render() {
        if(this.props.user.likedFollowing.loadingLikedFollowingNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }
        return (
            <div>
                {this.props.user.likedFollowing.liked_following_news.length === 0 ? <NoPost/> : this.props.user.likedFollowing.liked_following_news.map((post) => (
                    <div key={post.id}>
                        { post.editing ?
                            <EditPost post={post} key={post.id} current = {"liked-news"} /> :
                            <Post post={post} key={post.id} current = {"liked-following-news"} isNews = {false} /> }
                    </div>
                ))}
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchLikedFollowingNews: (data) => dispatch(fetchLikedFollowingNews(data)),
        addLikedFollowingNews: (data) => dispatch(addLikedFollowingNews(data)),
        newsLikedFollowingLoaded: (data) => dispatch(newsLikedFollowingLoaded(data)),
        clearLikedFollowingNews: (data) => dispatch(clearLikedFollowingNews(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(LikedFollowingNews);
