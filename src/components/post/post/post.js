import React, { Component } from 'react';
import './post.css';
import {getDate} from "../../../utills/date";
import {connect} from 'react-redux';
import {deletePost} from "../../../actions/deletePost";
import {editPost} from "../../../actions/editPost";
import API from "../../../utills/API";
import Images from "../images/images";
import CommentList from "../../comments/commentList/CommentList";
import Likes from "../../likes/Likes";
import Highlighter from "react-highlight-words";
import deleteNews from "../../../actions/deleteNews";
import editNews from "../../../actions/editNews";
import deleteTop from "../../../actions/deleteTop";
import editTop from "../../../actions/editTop";
import Repost from "../../repost/repost";
import {Link} from "react-router-dom";
import PollList from "../../poll/poll-list/poll-list";
import fetchAllPolls from "../../../actions/fetchAllPolls";


class Post extends React.Component {

    state = {
      showComments: false,
        polls: []
    };

    onDeleted = () => {
        API().delete(`/posts/${this.props.post.id}`,
           /* this.props.post.id,*/
        { headers: {
            Authorization : `Bearer_${this.props.user.token}`
        }} )
            .then(resp => {
                switch (this.props.current) {
                    case "news":
                    {
                        this.props.deleteNews(
                            this.props.post.id
                        )
                    }
                    case "all-post": {
                        this.props.deletePost(
                            this.props.post.id
                        )
                    }
                    case "top-news": {
                        this.props.deleteTop(
                            this.props.post.id
                        )
                    }
                }
                }
            );
    };


    onEdit = () => {
        switch (this.props.current) {
            case "news":
                {
                    this.props.editNews({
                        id: this.props.post.id
                    });
                }
            case "all-post": {
                this.props.editPost({
                    id: this.props.post.id
                });
            }
            case "top-news": {
                this.props.editTop({
                    id: this.props.post.id
                });
            }
        }
    };

    componentWillMount() {
        API().get(`/polls/posts/${this.props.post.id}`,
            {headers: {
                    Authorization : `Bearer_${this.props.user.token}`
                }
            }) .then(resp => {
                console.log("eeeeeeeee" , resp.data);
                const payload = {
                    post_id: this.props.post.id,
                    polls: resp.data
                };
                this.props.fetchAllPolls(resp.data);
                this.setState({
                    polls: resp.data
                });
                console.log("state",this.state)
            }
        );
    }

    showComments = () => {
        console.log("show comments");
        this.setState({
            showComments: !this.state.showComments
            }
        )

    };

    render() {
        const {post, isNews, search} = this.props;

        return (
            <div className="new-post">
                <div className="logo">
                <div className="logo-user">
                    <Link to={`/profileRedirect/${post.creator_id}`} className="" href="#0">
                        <img src={post.avatar_url} alt=""/>
                    </Link>
                </div>
                </div>
                <div className="info">
                <div className="info-about-user">
                    <div className="info-nickname">
                    <p className="nickname p-maryna">@{post.nickname}</p>
                    <p className="name-surname p-maryna">{post.first_name} {post.last_name}</p>
                    </div>
                    <div className="date">
                        <p className="p-maryna">{getDate(new Date(post.creation_time))}</p>
                    </div>
                </div>
                    <div className="post">
                        <div className="text">
                            <p className="p-maryna">
                                <Highlighter
                                    highlightClassName="YourHighlightClass"
                                    searchWords={[search]}
                                    autoEscape={true}
                                    textToHighlight={post.text === '' ? '' : post.text}
                                />
                            </p>
                        </div>
                        <div className="images">
                            <Images images={post.images} />
                        </div>
                        <div className="polls-post">
                        <PollList post={this.props.post} polls = {this.state.polls} />
                        </div>
                        <div className="bottom">
                            <div className="like-comment">
                            <div className="like"><Likes post ={post} /></div>
                            <div className="comment">
                                    <img src={require('./show_comments.png')} onClick={this.showComments} />
                            </div>
                            <div className="repost"><Repost post ={post} /></div>
                            </div>
                            <div className="functions-button">
                            {this.props.user.id === post.creator_id ?
                                <div className="functions-button">
                                    <button className="func" onClick={this.onDeleted}>Delete</button>
                                    <button className="func" onClick={this.onEdit}>Edit</button>
                                </div>
                                : <div> </div>
                            }
                            </div>
                        </div>
                        </div>
                    </div>
                <div className="update-time">Last update {getDate(new Date(post.update_time))}</div>
                {this.state.showComments ?
                    <div style={{width : "100%"}}><CommentList post={post} isNews={this.props.current}/></div> :
                    <div> </div>
                }
                </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return{
        deletePost: (id) =>  dispatch(deletePost(id)),
        editPost: (id) =>  dispatch(editPost(id)),
        deleteNews: (id) => dispatch(deleteNews(id)),
        editNews: (id) => dispatch(editNews(id)),
        deleteTop: (id) => dispatch(deleteTop(id)),
        editTop: (id) => dispatch(editTop(id)),
        fetchAllPolls: (payload) => dispatch(fetchAllPolls(payload))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Post);
