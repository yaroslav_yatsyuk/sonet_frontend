import React from 'react';
import './no-post.css';

const NoPost = () => {

    return (
        <div className="no-post">
            <p className="no-post-text">
                You have no posts
            </p>
            <div className="no-post-photo">
                <img src = {require('./no_post.png')}  />
            </div>
        </div>
    )
};

export default NoPost;
