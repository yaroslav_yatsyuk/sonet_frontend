import {fetchAllNews} from "../../../actions/fetchAllNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import Post from "../post";
import addNews from "../../../actions/addNews";
import newsLoaded from "../../../actions/newsLoaded";
import clearNews from "../../../actions/clearNews";
import EditPost from "../edit-post";


class SearchNews extends React.Component {

    state = {
        page: 0,
        search: '',
    };

    loadPostWithText = () => {
        console.log("start load wth post text");
        if (this.state.page === 0) {
            console.log("page", this.state.page);
            this.props.fetchAllNews([]);
        console.log("cleare with text");
    }
        console.log("load post with text");
        console.log(`posts/find?text=${this.props.searchNickname}&page=${this.state.page}&size=3&sort=creationTime,desc`);
        API().get(`posts/find?text=${this.props.searchNickname}&page=${this.state.page}&size=3&sort=creationTime,desc`,
            {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(resp => {
                    console.log("data response ",resp.data);
                    if (this.props.user.new.news.length === 0) {
                        this.props.fetchAllNews(
                            resp.data.map((post) => {
                                    post.editing = false;
                                    post.comments = [];
                                    return post;
                                }
                            ));
                    } else {
                        if(this.props.user.new.isLoaded) {
                            console.log("hide");
                        }else {
                            if(resp.data.length === 0){
                                this.props.newsLoaded(true);
                            }else {
                                this.props.addNews(
                                    resp.data.map((post) => {
                                            post.editing = false;
                                            post.comments = [];
                                            return post;
                                        }
                                    ));
                            }
                        }
                    }
                }
            );
    };

    componentDidMount() {
        console.log("did mount")
        if(this.props.user.new.news.length === 0) {
            console.log("component load")
            this.loadPostWithText();
        }
        else {
            console.log("clear ddid mount search");
            this.props.fetchAllNews([]);
            this.props.clearNews(false);
            //this.setState({page: 1});
        }
    };

    showMore = async () => {
            if (this.props.user.new.isLoaded === false) {
                await this.setState({
                    page: ++this.state.page
                });
                this.loadPostWithText();
            }  else {
                this.props.clearNews(false);
                this.setState({page: 1});
            }
    };

    search = () => {
        this.setState({search : this.props.searchNickname, page: 0});
        console.log("search", this.state.search);
        //this.setState({page:0});
        this.loadPostWithText();
    };



    render() {
        if(this.props.user.new.loadingNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }
        if(this.props.searchNickname !== this.state.search){
            this.search();
        }
        console.log("news prop", this.props.searchNickname);
        console.log("news state", this.state.search);
        return (
            <div>
                {this.props.user.new.news.length === 0 ? <div> </div> : this.props.user.new.news.map((post) => (
                    <div key={post.id}>
                        { post.editing ?
                            <EditPost post={post} key={post.id} current = {"news"} /> :
                            <Post post={post} key={post.id} current = {"news"} isNews = {false} search = {this.state.search}/>
                        }
                    </div>
                ))}
                <div className="bottom-news">
                    <button className="btn btn-primary show-more" onClick={this.showMore}>{this.props.user.new.isLoaded ?
                        <div>Hide</div> : <div>Show more</div>}
                    </button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllNews: (data) => dispatch(fetchAllNews(data)),
        addNews: (data) => dispatch(addNews(data)),
        newsLoaded: (data) => dispatch(newsLoaded(data)),
        clearNews: (data) => dispatch(clearNews(data))/*,
        clearAllNews: (data) => dispatch(clearAllNews(data))*/
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(SearchNews);
