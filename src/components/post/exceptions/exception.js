import React from 'react';
import './exception.css';


const Exception = ({message}) => {
    return (
        <p className="message">{message}</p>
    )
};


export default Exception;
