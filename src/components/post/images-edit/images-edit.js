import {makeStyleToDiv, makeStyleToImage} from "../../../utills/images-utils";
import React, {Fragment} from "react";
import './images-edit.css';



class ImagesEdit extends React.Component {

    delete = (url) => {
        this.props.deletePhoto(url);
    };

    render() {
        return (
            <Fragment>
                {this.props.images.map((image, i) => (
                    <div key={i} className="element">
                        <div className="heading">
                            <a className="delete-image" onClick={() => this.delete(image)}>×</a>
                        </div>
                        <div className={makeStyleToImage(i + 1)}>
                            <img className="image" src={image}/>
                        </div>
                    </div>
                ))}
            </Fragment>
        );
    };
}

export default ImagesEdit;
