import fetchAllTopNews from "../../../actions/fetchAllTopNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import NoPost from "../no-post";
import Post from "../post";
import addTopNews from "../../../actions/addTopNews";
import newsTopLoaded from "../../../actions/newsTopLoaded";
import clearTopNews from "../../../actions/clearTopNews";
import EditPost from "../edit-post";


class TopNews extends React.Component {

    state = {
        page: 0
    };

    loadPost = () => {
        API().get(`posts/top?page=${this.state.page}&size=3`,
            {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(resp => {
                    if (this.props.user.top.top_news.length === 0) {
                        this.props.fetchAllTopNews(
                            resp.data.map((post) => {
                                    post.editing = false;
                                    post.comments = [];
                                    return post;
                                }
                            ));
                    } else {
                        if(this.props.user.top.isLoadedTopNews) {
                            console.log("hide");
                        }else {
                            if(resp.data.length === 0){
                                this.props.newsTopLoaded(true);
                            }else {
                                this.props.addTopNews(
                                    resp.data.map((post) => {
                                            post.editing = false;
                                            post.comments = [];
                                            return post;
                                        }
                                    ));
                            }
                        }
                    }
                }
            );
    };

    componentDidMount() {
        if(this.props.user.top.top_news.length === 0) {
            this.loadPost();
        }else {
            this.props.clearTopNews(false);
            this.setState({page:1});
        }
    };

    showMore = async () => {
        if(this.props.user.top.isLoadedTopNews === false) {
            await this.setState({
                page: ++this.state.page
            });
            this.loadPost();
        }else {
            this.props.clearTopNews(false);
            this.setState({page:1});
        }
    };



    render() {
        if(this.props.user.top.loadingTopNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }
        return (
            <div>
                {this.props.user.top.top_news.length === 0 ? <NoPost/> : this.props.user.top.top_news.map((post) => (
                    <div key={post.id}>
                        { post.editing ?
                            <EditPost post={post} key={post.id} current = {"top-news"} /> :
                            <Post post={post} key={post.id} current = {"top-news"} isNews = {false} /> }
                    </div>
                ))}
                <div className="bottom-news">
                    <button className="btn btn-primary show-more" onClick={this.showMore}>{this.props.user.top.isLoadedTopNews ?
                        <div>Hide</div> : <div>Show more</div>}
                    </button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllTopNews: (data) => dispatch(fetchAllTopNews(data)),
        addTopNews: (data) => dispatch(addTopNews(data)),
        newsTopLoaded: (data) => dispatch(newsTopLoaded(data)),
        clearTopNews: (data) => dispatch(clearTopNews(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(TopNews);
