import fetchLikedNews from "../../../actions/fetchLikedNews";
import {connect} from "react-redux";
import API from "../../../utills/API";
import Spinner from "../../common/spinner";
import React from "react";
import NoPost from "../no-post";
import Post from "../post";
import addLikedNews from "../../../actions/addLikedNews";
import newsLikedLoaded from "../../../actions/newsLikedLoaded";
import clearLikedNews from "../../../actions/clearLikedNews";
import EditPost from "../edit-post";


class LikedNews extends React.Component {

    state = {
        page: 0
    };

    loadPost = () => {
        API().get(`posts/${this.props.user.id}/liked`,
            {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(resp => {
                        this.props.fetchLikedNews(
                            resp.data.map((post) => {
                                    post.editing = false;
                                    post.comments = [];
                                    return post;
                                }
                            ));
                            if(resp.data.length === 0){
                                this.props.newsTopLoaded(true);
                            }
                                this.props.newsLikedLoaded(true);
                }
            );
    };

    componentDidMount() {
            this.loadPost();
            this.setState({page:1});
    };

    render() {
        if(this.props.user.liked.loadingLikedNews) {
            return <div className="spinner-container">
                <div className="spinner">
                    <Spinner />
                </div>
            </div>
        }
        return (
            <div>
                {this.props.user.liked.liked_news.length === 0 ? <NoPost/> : this.props.user.liked.liked_news.map((post) => (
                    <div key={post.id}>
                        { post.editing ?
                            <EditPost post={post} key={post.id} current = {"liked-news"} /> :
                            <Post post={post} key={post.id} current = {"liked-news"} isNews = {false} /> }
                    </div>
                ))}
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



const mapDispatchToProps =(dispatch) => {
    return {
        fetchLikedNews: (data) => dispatch(fetchLikedNews(data)),
        addLikedNews: (data) => dispatch(addLikedNews(data)),
        newsLikedLoaded: (data) => dispatch(newsLikedLoaded(data)),
        clearLikedNews: (data) => dispatch(clearLikedNews(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(LikedNews);
