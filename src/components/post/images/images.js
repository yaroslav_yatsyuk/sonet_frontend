import React from 'react';
import {makeStyleToDiv, makeStyleToImage} from "../../../utills/images-utils";
import './images.css';
//import './images.scss';


const Images = ({images}) => {
        return (
            <div className={makeStyleToDiv(images.length)}>
                {images.map((image, i) => (
                    <div key={i} className="element">
                        <div className={makeStyleToImage(i+1)}>
                            <img src ={image}/>
                        </div>
                    </div>
                ))}
            </div>
        );
};

export default Images;
