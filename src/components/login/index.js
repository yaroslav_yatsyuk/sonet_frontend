import React, {Fragment} from "react";
import Logo from "../common/logo";
import ErrorExplanation from "../register/error-explanation";
import Input from "../common/input";
import Button from "../common/button";
import {passwordValidator, validateEmail} from "../../utills";
import AlertEmailConfirmation from "./alertEmailConfirmation";
import {Redirect} from "react-router-dom";
import API from "../../utills/API";

//import FullScreenSpinner from "../../common/full-screen-spinner";

class Login extends React.Component {

    state = {
        redirect: false,
        validate: false,
        alert: false,
        alertTimeout: null,
        loading: false
    };

    handleSubmit = e => {
        e.preventDefault();
    };

    showAlert() {
        const alertTimeoutFunc = () => {
            this.setState({
                alert: false
            });
        };
        clearTimeout(this.state.alertTimeout);

        this.setState({
            alert: true,
            alertTimeout: setTimeout(alertTimeoutFunc, 5000)
        });
    };

    hideAlert() {
        this.setState({
            alert: false
        });
    }

    componentWillMount() {
        if (this.props.errors.email != null || this.props.errors.password != null) {
            this.setState({
                validate: true
            });
            this.showAlert();
        }

        if (this.props.errors.blocked) {
            this.closeBlockedErrorTimeout();
        }
    }

    validate() {
        if (this.state.validate) {
            this.props.addUserLoginErrors({
                email: validateEmail(this.props.data.email),
                password: passwordValidator(this.props.data.password)
            });
            this.showAlert();
        } else {
            this.props.addUserLoginErrors({
                email: null,
                password: null
            });
        }
    };

    errorCount() {
        let count = 0;
        if (this.props.errors.email != null) count++;
        if (this.props.errors.password != null) count++;
        return count;
    }

    haveError() {
        return this.errorCount() !== 0;
    }

    handleInputChange = e => {
        this.setInputState(
            e.target.name,
            e.target.value
        )
    };

    setInputState = async (name, value) => {
        await this.props.addUserLoginData({
            ...this.props.data,
            [name]: value
        });
        this.validate();
    };

    closeBlockedErrorTimeout = () => {
        setTimeout(() => {
            this.props.changeUserLoginBlocked(false);
        }, 10000);
    };

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            validate: true
        });
        if (this.props.errors.password == null && this.props.errors.email == null) {
            API({security: false}).post("/auth/login", this.props.data)
                .then(resp => {
                    console.log("resp", resp);
                    this.props.addUserAuntificatedData({
                        id: resp.data.id,
                        firstName: resp.data.firstName,
                        lastName: resp.data.lastName,
                        email: resp.data.email,
                        nickname: resp.data.nickname,
                        avatar: resp.data.avatar.url,
                        background: resp.data.background.url,
                        token: resp.data.token,
                    });
                    this.props.addUserLoginData({
                        email: "",
                        password: ""
                    });
                    this.setState({
                        redirect: true
                    });
                    console.log("resp", resp);
                }).catch(error => {
                console.log("error response", error.response);
                console.log("error", error);
                if (error.response === undefined) {
                    console.log("error response is undefined");
                    this.props.addUserLoginErrors({
                        email: "Invalid Email or password",
                        password: "Invalid Email or password"
                    });
                    this.showAlert();
                } else {
                    if (error.response.status === 403) {
                        this.props.changeUserLoginBlocked(true);
                        this.closeBlockedErrorTimeout();
                    } else {
                        if (error.response.message !== "Email not verified") {
                            this.props.addUserLoginErrors({
                                emailNotVerified: true
                            });
                            this.props.changeAlertEmailConfirmation(false);
                        } else {
                            this.props.addUserLoginErrors({
                                email: "Invalid Email or password",
                                password: "Invalid Email or password"
                            });
                            this.showAlert();
                        }
                    }
                }
            });
        } else {
            //alert("form not valid");
        }
    };

    render() {
        if (this.state.redirect) {
            return <Redirect push to="/home"/>
        }

        let formContent = '';
        if (this.props.errors.blocked) {
            formContent = (
                <div className='flash-message alert' id='error_explanation'>
                    <strong>Too many attempts. You have been banned for one minute</strong>
                </div>
            );
        } else if (this.props.errors.emailNotVerified) {
            formContent = (
                <div className='flash-message alert' id='error_explanation'>
                    <strong>
                        You cannot login because your email is not confirmed!
                        A message with a confirmation link has been sent to your email address. Please follow the link
                        to activate your account. Please check your spam folder if you didn't receive this email.
                    </strong>
                </div>
            );
        } else {
            formContent = (
                <Fragment>
                    <div className='fields-group'>
                        <Input
                            name="email"
                            type="email"
                            handleInputChange={this.handleInputChange}
                            value={this.props.data.email}
                            error={this.props.errors.email}
                            label="E-mail address"
                        />
                    </div>
                    <div className='fields-group'>
                        <Input
                            name="password"
                            type="password"
                            handleInputChange={this.handleInputChange}
                            value={this.props.data.password}
                            error={this.props.errors.password}
                            label="Password"
                        />
                    </div>
                    <Button text="log in"/>
                </Fragment>);
        }

        return (
            <div className="stars">
                <div className="twinkling">
                    <div className='container-alt'>
                        <Logo/>
                        <div className='form-container' style={{"paddingTop": 0}}>
                            {!this.props.errors.blocked ?
                                (<form className="simple_form new_user" id="new_user" onSubmit={this.handleSubmit}>
                                    {this.props.alertEmailConfirmation && <AlertEmailConfirmation/>}
                                    {(this.state.alert && (this.props.errors.email != null || this.props.errors.password)) &&
                                    <ErrorExplanation count={this.errorCount()}/>}
                                    {formContent}
                                </form>) :
                                (<form className="simple_form new_user" id="new_user" onSubmit={this.handleSubmit}>
                                    {formContent}
                                </form>)
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Login;