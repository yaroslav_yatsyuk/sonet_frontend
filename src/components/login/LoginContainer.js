import {connect} from "react-redux";
import Login from "./";
import addUserLoginData from "../../actions/addUserLoginData";
import addUserLoginErrors from "../../actions/addUserLoginErrors";
import addUserAuntificatedData from "../../actions/addUserAuntificatedData";
import changeUserLoginBlocked from "../../actions/changeUserLoginBlocked";
import changeAlertEmailConfirmation from "../../actions/changeAlertEmailConfirmation";

const mapStateToProps = state => ({
    data: state.auth.login.data,
    errors: state.auth.login.errors,
    alertEmailConfirmation: state.alertEmailConfirmation
});

const mapDispatchToProps = dispatch => ({
    addUserLoginData: async payload => dispatch(addUserLoginData(payload)),
    addUserLoginErrors: payload => dispatch(addUserLoginErrors(payload)),
    addUserAuntificatedData: payload => dispatch(addUserAuntificatedData(payload)),
    changeUserLoginBlocked: payload => dispatch(changeUserLoginBlocked(payload)),
    changeAlertEmailConfirmation: payload => dispatch(changeAlertEmailConfirmation(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

