import React, { Component } from "react";
import { Container, Row, Col, Media } from "reactstrap";
import "./Followers.scss";
import { connect } from "react-redux";
import { fetchGroup } from "../../../actions/fetchGroup";
import { fetchFollowers } from "../../../actions/fetchFollowers";
import API from "../../../utills/API";
import changeGroupIsSubscribed from "../../../actions/changeGroupIsSubscribed";
import { spinnerForGroup } from "../../../actions/spinnerForGroups";
import FullScreenSpinner from "../../common/full-screen-spinner";
import EmptyList from "../../common/empty-list";

class Followers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      members: [],
      term: "",
      redirectToCreateGroup: false,
      groupId: 0
    };
  }

  componentDidMount() {
    console.log(this.state.group_id);
    API()
      .get(`/groups/followers/` + this.props.group.id, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(async res => {
        const members = res.data.members;
        await this.props.fetchFollowers(members);
        console.log(members);
        this.setState({
          members: this.props.group.members
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidUpdate(prevProps) {
    if (this.props.group.members !== prevProps.group.members) {
      this.props.fetchGroup(this.props.group);
    }
  }

  render() {
    console.log(this.props.group_id);
    return (
      <Container className="group_followers">
        <div className="follows">
          <Row>
            {this.props.group.members &&
              this.props.group.members.map(member => (
                <div key={member.id}>
                  <Media className="followerArea mt-3 p-3">
                    <Media left top className="postImgDiv mr-3 ml-3">
                      <Media
                        className="followerImg1"
                        object
                        src="https://sonet-social-bucket.s3.amazonaws.com/17/0a4638dbdd384f10b1453feb0094faec"
                      />
                    </Media>
                    <Media body>
                      <Media heading>
                        <span className="firstName">{member.firstName} </span>
                        <span className="lastName">{member.lastName}</span>
                        <div className="nickname">@{member.nickname}</div>
                      </Media>
                    </Media>
                  </Media>
                </div>
              ))}
          </Row>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    group: state.group,
    user: state.user,
    groupsLoading: state.groupsLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchFollowers: async payload => dispatch(fetchFollowers(payload)),
    fetchGroup: payload => dispatch(fetchGroup(payload)),
    changeGroupIsSubscribed: payload =>
      dispatch(changeGroupIsSubscribed(payload)),
    spinnerForGroup: payload => dispatch(spinnerForGroup(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Followers);
