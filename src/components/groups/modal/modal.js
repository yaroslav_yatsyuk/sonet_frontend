import React, { Fragment, useState } from "react";
import Spinner from "../../common/spinner";
import { Modal, Input } from "reactstrap";
import { spinnerForGroup } from "../../../actions/spinnerForGroups";
import editGroup from "../../../actions/editGroup";
import { fetchGroup } from "../../../actions/fetchGroup";
import changeGroupIsSubscribed from "../../../actions/changeGroupIsSubscribed";
import { connect } from "react-redux";
import API from "../../../utills/API";
import "./modal.scss";

class EditModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      name: "",
      nickname: "",
      description: "",
      hasError: false,
      background: props.group.background_url,
      backgroundFile: "",
      backgroundPreviewUrl: "",
      backgroundLoaded: false,
      avatar: props.group.avatar_url,
      avatarFile: "",
      avatarPreviewUrl: "",
      avatarLoaded: false,
      editGroupLoading: false,
      isCreator: false,
      redirectToGroup: false
    };
    this.toggle = this.toggle.bind(this);
  }

  async toggle() {
    if (this.state.modal === false) {
      await this.setState(prevState => ({
        modal: !prevState.modal,
        name: this.props.group.name,
        nickname: this.props.group.nickname,
        description: this.props.group.description,
        avatar: this.props.group.avatar_url,
        background: this.props.group.background_url
      }));
    } else
      await this.setState(prevState => ({
        modal: !prevState.modal
      }));
  }

  saveBackground() {
    let formData = new FormData();
    formData.append("file", this.state.backgroundFile);

    API()
      .post(`/users/${this.props.user.id}/files`, formData, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(resp => {
        this.setState({
          backgroundLoaded: true,
          background: resp.data.message
        });
        if (this.state.avatarLoaded) {
          this.handleSubmit();
        }
      });
  }

  _handleBackgroundChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let background = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        backgroundFile: background,
        backgroundPreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(background);
  }

  saveAvatar() {
    let formData = new FormData();
    formData.append("file", this.state.avatarFile);

    API()
      .post(`/users/${this.props.user.id}/files`, formData, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(resp => {
        this.setState({
          avatarLoaded: true,
          avatar: resp.data.message
        });
        if (this.state.backgroundLoaded) {
          this.handleSubmit();
        }
      });
  }

  _handleAvatarChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let avatar = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        avatarFile: avatar,
        avatarPreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(avatar);
  }

  nameChange(e) {
    this.setState({
      name: e.target.value,
      hasError: false
    });
  }

  nicknameChange(e) {
    this.setState({
      nickname: e.target.value,
      hasError: false
    });
  }

  descriptionChange(e) {
    this.setState({
      description: e.target.value
    });
  }

  preHandleSubmit = event => {
    event.preventDefault();
    this.setState({
      editGroupLoading: true
    });
    this.props.spinnerForGroup(true);
    this.setState({ editGroupLoading: true });
    if (this.state.avatarFile !== "" && this.state.backgroundFile !== "") {
      this.saveAvatar();
      this.saveBackground();
    } else if (
      this.state.avatarFile !== "" &&
      this.state.backgroundFile === ""
    ) {
      this.setState({ backgroundLoaded: true });
      this.saveAvatar();
    } else if (
      this.state.avatarFile === "" &&
      this.state.backgroundFile !== ""
    ) {
      this.setState({ avatarLoaded: true });
      this.saveBackground();
    } else {
      this.handleSubmit();
    }
  };

  handleSubmit = () => {
    API()
      .put(
        `/groups/${this.props.group.id}`,
        {
          name: this.state.name,
          nickname: this.state.nickname,
          description: this.state.description,
          avatar_url: this.state.avatar,
          background_url: this.state.background
        },
        {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        }
      )
      .then(async resp => {
        this.props.spinnerForGroup(false);
        this.setState({
          editGroupLoading: false,
          redirectToGroup: true
        });
        const group = resp.data;
        await this.props
          .editGroup(group)
          .then(data => {
            this.toggle();
          })
          .catch(error => {
            console.log(error);
            console.log(error.response);
            this.setState({
              editGroupLoading: false,
              hasError: true
            });
          });
      });
  };

  render() {
    let { backgroundPreviewUrl, avatarPreviewUrl } = this.state;
    let $backgroundPreview = null;
    if (backgroundPreviewUrl) {
      $backgroundPreview = (
        <img className="editBackgroundProfileImg" src={backgroundPreviewUrl} />
      );
    } else {
      $backgroundPreview = (
        <img
          className="editBackgroundProfileImg"
          src={this.props.group.background_url}
        />
      );
    }

    let $avatarPreview = null;
    if (avatarPreviewUrl) {
      $avatarPreview = (
        <img className="editProfile_logo" src={avatarPreviewUrl} />
      );
    } else {
      $avatarPreview = (
        <img className="editProfile_logo" src={this.props.group.avatar_url} />
      );
    }
    return (
      <Fragment>
        <button
          className={
            this.props.group.isCreator ? "edit_button" : "hidden_button"
          }
          onClick={this.toggle}
        >
          Edit Group <i className="fa fa-pencil"></i>
        </button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <div className="row editUserModal">
            <div className="editUserModalHeader col-12 pt-2 d-flex flex-row">
              <button className="closeModalButton m-1" onClick={this.toggle}>
                <i className="fa fa-times" />
              </button>
              <span className="edit-your-info-text text-left">Edit group</span>
              {this.state.editGroupLoading ? (
                <div className="editGroupSpinner">
                  <Spinner />
                </div>
              ) : (
                ""
              )}
              <form
                className="edit-save-form d-flex justify-content-end"
                onSubmit={this.preHandleSubmit}
              >
                <button className="btn btn-primary editUserSaveButton float-xs-right">
                  Save
                </button>
              </form>
            </div>
            <div className="col-12 editUserModalElementsDiv d-flex flex-column mt-2">
              <div className="editBackgroundProfileImgDiv">
                <form onSubmit={e => this._handleSubmitBackground(e)}>
                  <label className="btn btn-default BackgroundImageInput1">
                    <i className="fa fa-camera" />{" "}
                    <input
                      type="file"
                      hidden
                      onChange={e => this._handleBackgroundChange(e)}
                    />
                  </label>
                </form>
                {$backgroundPreview}
              </div>
              <div className="editProfileHeader">
                <div className="edit_profile_logo_container">
                  <form onSubmit={e => this._handleSubmitAvatar(e)}>
                    <label className="btn btn-default AvatarImageInput1">
                      <i className="fa fa-camera" />{" "}
                      <input
                        type="file"
                        hidden
                        onChange={e => this._handleAvatarChange(e)}
                      />
                    </label>
                  </form>
                  {$avatarPreview}
                </div>
              </div>

              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Name</span>
                <input
                  type="text"
                  value={this.state.name}
                  onChange={e => this.nameChange(e)}
                  placeholder="Add group name"
                  className="textareaEditUser"
                />
                {this.state.hasError ? (
                  <span className="errorNicknameAlreadyExists">
                    Name Already Exist
                  </span>
                ) : (
                  ""
                )}
              </div>
              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Nickname</span>
                <input
                  value={this.state.nickname}
                  onChange={e => this.nicknameChange(e)}
                  placeholder="Add group nickname"
                  className="textareaEditUser"
                />
              </div>
              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Description</span>
                <input
                  value={this.state.description}
                  onChange={e => this.descriptionChange(e)}
                  placeholder="Add group description"
                  className="textareaEditUser"
                />
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user,
  group: state.group,
  groupsLoading: state.groupsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchGroup: async payload => dispatch(fetchGroup(payload)),
  editGroup: async payload => dispatch(editGroup(payload)),
  changeGroupIsSubscribed: payload =>
    dispatch(changeGroupIsSubscribed(payload)),
  spinnerForGroup: payload => dispatch(spinnerForGroup(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditModal);
