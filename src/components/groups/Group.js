import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import "./Group.scss";
import classnames from "classnames";
import { Media } from "reactstrap";
import Followers from "./Followers";
import ListOfPages from "../ListOfPages/ListOfPages";
import FullScreenSpinner from "../common/full-screen-spinner";
import { connect } from "react-redux";
import { fetchGroup } from "../../actions/fetchGroup";
import API from "../../utills/API";
import AllPost from "../post/all-post/all-post";
import { spinnerForGroup } from "../../actions/spinnerForGroups";
import PostForm from "../post/post-form/post-form";
import addUserData from "../../actions/addUserData";
import changeGroupIsSubscribed from "../../actions/changeGroupIsSubscribed";
import changeGroupIsCreator from "../../actions/changeGroupIsCreator";
import { fetchFollowers } from "../../actions/fetchFollowers";
import { fetchAllGroups } from "../../actions/fetchAllGroups";
import EmptyList from "../common/empty-list";
import EditModal from "./modal/modal";
import "../groups/Followers/Followers.scss";

class Group extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);

    this.state = {
      activeTab: "1",

      group: props.group,
      followers: props.group.followers,
      groupId: this.props.match.params.id,
      groupLoaded: false,
      isSubscribed: false
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  getMembers() {
    API()
      .get(`/groups/followers/` + this.props.group.id, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(async res => {
        const members = res.data.members;
        this.props.spinnerForGroup(false);
        await this.props.fetchFollowers(members);
      });
  }

  isGroupFollowed() {
    API()
      .get(
        `/groups/member?followerId=${this.props.user.id}&groupId=${this.props.group.id}`,
        {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        }
      )
      .then(async res => {
        await this.props.changeGroupIsSubscribed(res.data);
      });
  }

  toggleFollowing = async () => {
    this.getUser();
    console.log(this.props.user_info.avatar);
    const followGroup = {
      followerId: this.props.user.id,
      groupId: this.props.group.id
    };
    if (this.props.group.isSubscribed) {
      await this.props.changeGroupIsSubscribed(false);
      API()
        .delete(
          "/groups/unfollow?userId=" +
            this.props.user.id +
            "&groupId=" +
            this.props.group.id,
          {
            headers: {
              Authorization: `Bearer_${this.props.user.token}`
            }
          }
        )
        .catch(err => {
          console.log(err);
        });
      const newFollowersList = this.props.group.members.filter(member => {
        return member.id !== this.props.user.id;
      });
      this.props.fetchFollowers(newFollowersList);
    } else {
      await this.props.changeGroupIsSubscribed(true);

      API()
        .post("/groups/follow", followGroup, {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        })
        .catch(err => {
          console.log(err, "error during follow group");
        });
      console.log(this.props.user_info.avatar);
      const newFollowersList = this.props.group.members.concat({
        id: this.props.user.id,
        nickname: this.props.user.nickname,
        firstName: this.props.user.firstName,
        lastName: this.props.user.lastName,
        avatar: this.props.user_info.avatar
      });
      this.props.fetchFollowers(newFollowersList);
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.group !== this.props.group ||
      nextState.activeTab !== this.state.activeTab ||
      nextProps.group.isCreator !== this.props.group.isCreator ||
      nextProps.group.isSubscribed !== this.props.group.isSubscribed ||
      nextProps.group.members !== this.props.group.members
    );
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState({
      groupLoaded: true
    });

    API()
      .get(`/groups/` + id, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(async res => {
        const group = res.data;
        await this.props.fetchGroup(group);
        this.setState({
          group: this.props.group,
          members: this.props.group.members
        });
        this.isGroupFollowed();
        this.checkIsCreator();
      });
    this.setState({
      isSubscribed: this.props.group.isSubscribed
    });
    this.getUser();
  }

  getUser() {
    API()
      .get(`/users/${this.props.user.id}`, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(async resp => {
        await this.props.addUserData({
          id: resp.data.id,
          firstName: resp.data.firstName,
          lastName: resp.data.lastName,
          email: resp.data.email,
          nickname: resp.data.nickname,
          avatar: resp.data.avatar.url,
          background: resp.data.background.url
        });
      });
  }

  checkIsCreator = () => {
    API()
      .get(
        `/groups/creator?userId=${this.props.user.id}&groupId=${this.props.group.id}`,
        {
          headers: {
            Authorization: `Bearer_${this.props.user.token}`
          }
        }
      )
      .then(async resp => {
        const isCreator = resp.data;
        this.props.changeGroupIsCreator(isCreator);
      });
  };

  render() {
    if (!this.state.groupLoaded) {
      return <FullScreenSpinner />;
    }
    const { group } = this.props;
    this.setState({
      isSubscribed: this.props.group.isSubscribed
    });
    return (
      <div className="stars">
        <div className="twinkling">
          <Container
            className="group-page"
            style={{ display: "flex", flexDirection: "column" }}
          >
            <div className="groupHeight">
              <div className="backgroundProfileImgDiv">
                <img
                  className="backgroundProfileImg"
                  src={group.background_url}
                  alt=""
                />
              </div>
              <div className="profileHeader">
                <Row className="profileHeaderGrid">
                  <Col xs="6" sm="4" md="2" className="profile_logo_col">
                    <div className="group_avatar_container">
                      <img
                        src={group.avatar_url}
                        className="group_logo"
                        alt="Logo"
                      />
                    </div>
                  </Col>
                  <Col xs={{ sise: 12, offset: 1 }} sm="7" md="9">
                    <Row>
                      <Col xs="12" lg="7">
                        <div className="name">{group.name}</div>
                        <div className="group_nickname">@{group.nickname}</div>
                        <div className="group_description">
                          <p>{group.description}</p>
                        </div>
                      </Col>

                      <Col xs="12" lg="5">
                        <button
                          className="btn follow btn-info mt-4"
                          onClick={this.toggleFollowing}
                        >
                          {this.state.isSubscribed === true
                            ? "Unfollow"
                            : "Follow"}
                        </button>
                        <div>
                          <EditModal />
                        </div>
                      </Col>

                      <Col>
                        <div className="postsToggle1 mt-2">
                          <Nav tabs>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.activeTab === "1"
                                })}
                                onClick={() => {
                                  this.toggle("1");
                                }}
                              >
                                <span className="navText">Posts</span>
                              </NavLink>
                            </NavItem>
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: this.state.activeTab === "2"
                                })}
                                onClick={() => {
                                  this.toggle("2");
                                }}
                              >
                                <span className="navText">Followers</span>
                              </NavLink>
                            </NavItem>
                          </Nav>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
              <Row>
                <Col xs="10" lg="9">
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <PostForm />
                      <AllPost user_id={group.id} />
                    </TabPane>
                    <TabPane tabId="2">
                      {this.props.group.members.length === 0 ? (
                        <div className="emptyList">
                          <EmptyList>No Followers</EmptyList>
                        </div>
                      ) : (
                        <div className="follows">
                          <Row>
                            {this.props.group.members &&
                              this.props.group.members.map(member => (
                                <div key={member.id}>
                                  <Media className="followerArea mt-3 p-3">
                                    <Media
                                      left
                                      top
                                      className="postImgDiv mr-3 ml-3"
                                    >
                                      <Media
                                        className="followerImg1"
                                        object
                                        src={member.avatar.url}
                                      />
                                    </Media>
                                    <Media body>
                                      <Media heading>
                                        <span className="firstName">
                                          {member.firstName}{" "}
                                        </span>
                                        <span className="lastName">
                                          {member.lastName}
                                        </span>
                                        <div className="nickname">
                                          @{member.nickname}
                                        </div>
                                      </Media>
                                    </Media>
                                  </Media>
                                </div>
                              ))}
                          </Row>
                        </div>
                      )}
                    </TabPane>
                  </TabContent>
                </Col>
                <Col xs="2" lg="3">
                  <div className="navigation">
                    <ListOfPages />
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    group: state.group,
    user: state.user,
    user_info: state.user_info,
    followed: state.followed,
    groupsLoading: state.groupsLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchGroup: async payload => dispatch(fetchGroup(payload)),
    fetchFollowers: payload => dispatch(fetchFollowers(payload)),
    changeGroupIsSubscribed: payload =>
      dispatch(changeGroupIsSubscribed(payload)),
    changeGroupIsCreator: payload => dispatch(changeGroupIsCreator(payload)),
    fetchAllGroups: payload => dispatch(fetchAllGroups(payload)),
    spinnerForGroup: payload => dispatch(spinnerForGroup(payload)),
    addUserData: async payload => dispatch(addUserData(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Group);
