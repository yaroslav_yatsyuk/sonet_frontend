import React, { useState, useEffect } from "react";
import {
  hideModal,
  fetchConversations,
  conversationsRequested
} from "../../../actions";
import { connect } from "react-redux";
import withSonetService from "../hoc/with-sonet-service";
import { withRouter } from "react-router";
import FriendList from "../friend-list/friend-list";
import ConversationNameInput from "../conversation-name-input/conversation-name-input";
import SearchFriendsBox from "../search-friends-box/search-friends-box";
import "./create-conversation-modal.css";

const CreateConversationModal = ({
  showModal,
  hideModal,
  sonetService,
  fetchConversations,
  conversationsRequested,
  conversationNameLabel,
  conversations,
  friends,
  userId,
  history,
  token
}) => {
  const [hasError, setHasError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [createButtonText, setCreateButtonText] = useState("");
  const [createDisabled, setCreateDisabled] = useState(false);

  useEffect(() => {
    if (friends.filter(friend => friend.checked).length < 1) {
      setCreateDisabled(true);
    } else {
      setCreateDisabled(false);
    }

    if (friends.filter(friend => friend.checked).length < 2) {
      setCreateButtonText("Create dialog");
    } else {
      setCreateButtonText("Create conversation");
    }
  }, [friends]);

  const showHideClassName =
    "form-group modal display-" + (showModal ? "block" : "none");

  const onCreate = () => {
    let checkedFriends = [];
    friends.forEach(friend => {
      if (friend.checked) {
        checkedFriends.push(friend.id);
      }
    });
    let redirected = false;
    if (checkedFriends.length === 1) {
      const selectedFriend = friends.filter(
        friend => friend.id === checkedFriends[0]
      );
      conversations.forEach(conversation => {
        if (
          conversation.title ===
            selectedFriend[0].firstName + " " + selectedFriend[0].lastName &&
          !redirected
        ) {
          history.push(`${conversation.id}`);
          redirected = true;
        }
      });
    }
    if (redirected) {
      hideModal();
      return;
    }
    if (conversationNameLabel.trim() === "" && checkedFriends.length > 1) {
      setHasError(true);
      setErrorMessage("Enter proper conversation name");
    } else if (
      conversations.filter(
        conversation => conversation.title === conversationNameLabel.trim()
      ).length > 0
    ) {
      setHasError(true);
      setErrorMessage("Conversation with such name already exist");
    } else if (conversationNameLabel.length > 50) {
      setHasError(true);
      setErrorMessage("Conversation name maximum size is 50");
    } else {
      sonetService
        .createConversation(
          {
            conversationNameLabel: conversationNameLabel.trim(),
            checkedFriends
          },
          userId,
          token
        )
        .then(() => {
          fetchConversations(userId, token);
        });
      conversationsRequested();
      hideModal();
    }
  };

  let errorAlert = null;
  if (hasError) {
    errorAlert = (
      <div className="alert alert-primary channel-alert" role="alert">
        {errorMessage}
      </div>
    );
  }
  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <SearchFriendsBox />
        <FriendList />
        <ConversationNameInput />
        <div className="m-2 text-right">
          <button
            className="col-sm-5 btn mr-2 btn-secondary create-button"
            onClick={onCreate}
            disabled={createDisabled}
          >
            {createButtonText}
          </button>
          <button
            className="col-sm-3 btn btn-secondary create-button"
            onClick={hideModal}
          >
            Cancel
          </button>
        </div>
        {errorAlert}
      </section>
    </div>
  );
};

const mapStateToProps = ({
  chat: { showModal, conversationNameLabel, friends, userId, conversations },
  user: { token }
}) => {
  return {
    showModal,
    conversationNameLabel,
    friends,
    userId,
    conversations,
    token
  };
};

const mapDispatchToProps = (dispatch, { sonetService }) => {
  return {
    fetchConversations: (userId, token) =>
      fetchConversations(sonetService, dispatch, userId, token)(),
    hideModal: () => dispatch(hideModal()),
    conversationsRequested: () => dispatch(conversationsRequested())
  };
};

export default withSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withRouter(CreateConversationModal))
);
