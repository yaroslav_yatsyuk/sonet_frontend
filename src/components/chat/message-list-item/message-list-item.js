import React from "react";
import "./message-list-item.css";
import { connect } from "react-redux";
import avatar from "../../../images/avatar.png";
import { Link } from "react-router-dom";

const MessageListItem = ({ message, userId }) => {
  const { content, sender, sendTime, senderId, avatar } = message;
  const sendTimeDate = new Date(sendTime);
  if (userId === senderId) {
    return (
      <div className="d-flex w-100 justify-content-end">
        <div>
          <Link
            to={`/profile/${senderId}`}
            style={{ textDecoration: "none", color: "white" }}
          >
            <span className="font-weight-bold">{`${sender} `}</span>
          </Link>
          <small>{sendTimeDate.toLocaleString()}</small>
          <div className="p-2 message-content-reverse text-right">
            {content}
          </div>
        </div>
        <Link to={`/profile/${senderId}`}>
          <img src={avatar} className="avatar-reverse" alt="avatar" />
        </Link>
      </div>
    );
  }
  return (
    <div className="d-flex w-100">
      <Link to={`/profile/${senderId}`}>
        <img src={avatar} className="avatar" alt="avatar" />
      </Link>
      <div>
        <Link
          to={`/profile/${senderId}`}
          style={{ textDecoration: "none", color: "white" }}
        >
          <span className="font-weight-bold">{`${sender} `}</span>
        </Link>
        <small>{sendTimeDate.toLocaleString()}</small>
        <div className="p-2 message-content">{content}</div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ user: { id: userId } }) => {
  return { userId };
};

export default connect(mapStateToProps)(MessageListItem);
