import React, { useState, useEffect } from "react";
import { conversationNameInputChange } from "../../../actions";
import { connect } from "react-redux";
import "./conversation-name-input.css";

const ConversationNameInput = ({
  conversationNameLabel,
  onLabelChange,
  friends
}) => {
  const [disabled, setDisabled] = useState(true);
  const [placeholder, setPlaceholder] = useState("Enter conversation name");
  useEffect(() => {
    if (friends.filter(friend => friend.checked).length > 1) {
      setDisabled(false);
      setPlaceholder("Enter conversation name");
    } else {
      setDisabled(true);
      onLabelChange("");
      setPlaceholder("Select atleast 2 people to specify conversation name");
    }
  }, [friends]);

  return (
    <div className="p-2">
      <input
        className="w-100 p-2 form-control chat-input"
        value={conversationNameLabel}
        type="text"
        onChange={event => onLabelChange(event.target.value)}
        placeholder={placeholder}
        disabled={disabled}
      />
    </div>
  );
};

const mapStateToProps = ({ chat: { conversationNameLabel, friends } }) => {
  return { conversationNameLabel, friends };
};

const mapDispatchToProps = dispatch => {
  return {
    onLabelChange: symbol => dispatch(conversationNameInputChange(symbol))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConversationNameInput);
