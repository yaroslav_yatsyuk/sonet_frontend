import React from "react";
import "./conversation-list-item.css";
import avatar from "../../../images/avatar.png";

const ConversationListItem = ({ conversation }) => {
  const { title, lastMessage } = conversation;
  let lastMessageContainer;
  if (lastMessage) {
    let { content } = lastMessage;
    if (content.length > 40) {
      content = content.substring(0, 40) + "...";
    }
    const sendTimeDate = new Date(lastMessage.sendTime);
    lastMessageContainer = (
      <div title={lastMessage.content}>
        {`${lastMessage.sender}: ${content} `}
        <small>{`(${sendTimeDate.toLocaleString()})`}</small>
      </div>
    );
  } else {
    lastMessageContainer = <small>No messages</small>;
  }
  let trunTitle = title;
  if (title.length > 30) {
    trunTitle = trunTitle.substring(0, 30) + "...";
  }
  const conversationImage = lastMessage ? lastMessage.avatar : avatar;
  return (
    <div className="d-flex conversation p-2">
      <img
        src={conversationImage}
        className="avatar conversationAvatar"
        alt="avatar"
      />
      <div>
        <div className="font-weight-bold" title={title}>
          {trunTitle}
        </div>
        <div className="d-flex">{lastMessageContainer}</div>
      </div>
    </div>
  );
};

export default ConversationListItem;
