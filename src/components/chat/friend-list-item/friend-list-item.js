import React from "react";
import "./friend-list-item.css";
import avatar from "../../../images/avatar.png";

const FriendListItem = ({ friend }) => {
  const friendClass = "p-2 d-flex" + (friend.checked ? " checked" : "");
  return (
    <div className={friendClass}>
      <img
        src={friend.avatar ? friend.avatar : avatar}
        className="friend-avatar"
        alt="avatar"
      />
      <div className="d-inline-block m-2">{`${friend.firstName} ${
        friend.lastName
      }`}</div>
    </div>
  );
};

export default FriendListItem;
