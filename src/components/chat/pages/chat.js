import React from "react";
import ChatContent from "../chat-content/chat-content";
import ChannelList from "../channel-list/channel-list";
import ListOfPages from "../../ListOfPages/ListOfPages";
import "./chat.css";
import ModalEnableGeoapiPermission from "../../common/modal-enable-geoapi-permission";
import {connect} from "react-redux";
import {getLocation} from "../../../actions/getLocation";

class Chat extends React.Component {


    componentDidMount = async () => {
        await this.props.getLocation();
    };


    render(){
        const { id } = this.props.match.params;

        return (
            <div className="stars">
                <div className="twinkling row chat">
                    <ChannelList id={id} />
                    <ChatContent id={id} />
                    <ListOfPages />
                </div>
            </div>
        );
    }

}

const mapDispatchToProps = dispatch => ({
    getLocation: async () => dispatch(getLocation())
});

const mapStateToProps = state => ({
   user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
