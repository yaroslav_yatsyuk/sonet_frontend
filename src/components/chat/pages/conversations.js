import React from "react";
import ConversationsContent from "../conversations-content/conversations-content";
import ListOfPages from "../../ListOfPages/ListOfPages";
import "./conversations.css";
import {connect} from "react-redux";
import ModalEnableGeoapiPermission from "../../common/modal-enable-geoapi-permission";
import {getLocation} from "../../../actions/getLocation";
let audio = new Audio();

class Conversations extends React.Component {


    componentDidMount = async () => {
        await this.props.getLocation();
    };

    render() {
        return (
            <div className="stars">
                <div className="twinkling conversations">
                    <ConversationsContent />
                    <ListOfPages />
                    <div
                        className="d-inline-block w-25"
                        style={{ opacity: 0, height: 40, "background-color": "green" }}
                        onClick={() => {
                            audio.pause();
                            audio = new Audio("http://streaming.tdiradio.com:8000/house.mp3");
                            audio.play();
                        }}
                    />
                    <div
                        className="d-inline-block w-50"
                        style={{ opacity: 0, height: 40, "background-color": "red" }}
                        onClick={() => {
                            audio.pause();
                            audio = new Audio("https://streamer.radio.co/s06b196587/listen");
                            audio.play();
                        }}
                    />
                    <div
                        className="d-inline-block w-25"
                        style={{ opacity: 0, height: 40, "background-color": "blue" }}
                        onClick={() => {
                            audio.pause();
                            audio = new Audio(
                                "http://partyviberadio.com:8016/;listen.pls?sid=1"
                            );
                            audio.play();
                        }}
                    />
                </div>
            </div>
        );
    }

}

const mapDispatchToProps = dispatch => ({
    getLocation: async () => dispatch(getLocation())
});

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(Conversations);
