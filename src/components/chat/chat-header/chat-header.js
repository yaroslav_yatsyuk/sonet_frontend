import React, { Component } from "react";
import withSonetService from "../hoc/with-sonet-service";
import {
  fetchConversations,
  channelNameInputChange,
  fetchChannels,
  currentChannelIdChange
} from "../../../actions";
import { connect } from "react-redux";
import Spinner from "../../common/spinner";
import "./chat-header.css";
import API from "../../../utills/API";
import classNames from "classnames";

class ChatHeader extends Component {
  state = {
    hasError: false,
    errorMessage: "",
    inCreateChannel: false,
    conversationName: "",
    isOnline: false
  };

  refreshUserIsOnline = () => {
    API().get(`/users/${this.props.userId}/is-online`, {
      headers: {
        Authorization: `Bearer_${this.props.token}`,
      }
    }).then(async resp => {
      console.log("get user is online", resp.data);
      this.setState({
        isOnline: resp.data.isOnline
      });
    }).catch(error => {
      console.log("error during get user is online")
    });

    setTimeout(this.refreshUserIsOnline, 1000 * 60);
  };

  componentDidMount() {
    const { fetchConversations, userId, token, id } = this.props;
    fetchConversations(userId, token).then(conversations => {
      conversations.forEach(conversation => {
        if (conversation.id == id) {
          this.setState({ conversationName: conversation.title });
        }
      });
    });
    this.refreshUserIsOnline();

  }

  createChannel = () => {
    const {
      sonetService,
      channelNameLabel,
      token,
      id,
      fetchChannels,
      channels,
      currentChannelIdChange,
      userId
    } = this.props;

    if (channelNameLabel.trim() === "") {
      this.setState({
        hasError: true,
        errorMessage: "Enter proper channel name"
      });
    } else if (
      channels.filter(
        channel =>
          channel.name.toUpperCase() === channelNameLabel.trim().toUpperCase()
      ).length > 0
    ) {
      this.setState({
        hasError: true,
        errorMessage: "Channel with such name already exist"
      });
    } else if (channelNameLabel.length > 50) {
      this.setState({
        hasError: true,
        errorMessage: "Channel name maximum size is 50"
      });
    } else {
      sonetService
        .createChannel(
          { name: channelNameLabel.trim(), chatId: id, type: "normal" },
          token
        )
        .then(() => {
          fetchChannels(id, token).then(channels => {
            currentChannelIdChange(
              channels[channels.length - 1].id,
              token,
              userId
            );
          });
        });
      this.setState({
        hasError: false,
        errorMessage: "",
        inCreateChannel: false
      });
    }
  };

  getConversationName = () => {
    const { conversations, id } = this.props;
    if (conversations.length > 0) {
      conversations.forEach(conversation => {
        if (conversation.id == id) {
          this.setState({ conversationName: conversation.title });
        }
      });
    }
  };

  render() {
    const { onLabelChange, channelNameLabel } = this.props;
    let title, trunTitle;
    if (this.state.conversationName !== "") {
      const maxLength = this.state.inCreateChannel ? 15 : 30;
      title = this.state.conversationName;
      if (title.length > maxLength) {
        trunTitle = title.substring(0, maxLength) + "...";
      } else {
        trunTitle = title;
      }
    } else {
      trunTitle = <Spinner />;
      title = "spinner";
    }
    let inputClass = "form-control create-channel d-inline-block chat-input";
    let errorMessage = null;
    if (this.state.hasError) {
      inputClass += " error";
      errorMessage = (
        <div class="alert alert-primary channel-alert" role="alert">
          {this.state.errorMessage}
        </div>
      );
    }
    let createChannel;
    if (this.state.inCreateChannel) {
      createChannel = (
        <div className="d-flex m-3 andrii-chat-header">
          <input
            onChange={event => onLabelChange(event.target.value)}
            value={channelNameLabel}
            type="text"
            className={inputClass}
            placeholder="Channel name"
          />
          <button
            className="btn btn-dark header-button ml-1"
            title="Create"
            onClick={this.createChannel}
          >
            &#10004;
          </button>
          <button
            className="btn btn-dark header-button ml-1"
            title="Cancel"
            onClick={() =>
              this.setState({
                inCreateChannel: false,
                hasError: false,
                errorMessage: ""
              })
            }
          >
            &#10006;
          </button>
        </div>
      );
    } else {
      createChannel = (
        <div
          className="d-flex m-3"
          onClick={() => {
            this.setState({ inCreateChannel: true });
          }}
        >
          <button className="btn btn-dark header-button">Create channel</button>
        </div>
      );
    }
    return (
      <div>
        <div className="d-flex justify-content-center position-relative">
          <div className="pt-4 position-absolute">
            <h4 className="h4" title={title}>
              {trunTitle}
              <div className={classNames("conversation-is-online",{"online":this.state.isOnline})}></div>
            </h4>
          </div>
        </div>
        {createChannel}
        <hr className="m-0" />
        {errorMessage}
      </div>
    );
  }
}

const mapStateToProps = ({
  chat: { conversations, userId, channelNameLabel, channels },
  user: { token }
}) => {
  return { conversations, userId, channelNameLabel, token, channels };
};

const mapDispatchToProps = (dispatch, { sonetService }) => {
  return {
    fetchConversations: (userId, token) =>
      fetchConversations(sonetService, dispatch, userId, token)(),
    fetchChannels: (chatId, token) =>
      fetchChannels(sonetService, dispatch, chatId, token)(),
    onLabelChange: symbol => dispatch(channelNameInputChange(symbol)),
    currentChannelIdChange: (channelId, token, userId) =>
      currentChannelIdChange(sonetService, dispatch, channelId, token, userId)
  };
};

export default withSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChatHeader)
);
