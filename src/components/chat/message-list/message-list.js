import React, { Component } from "react";

import MessageListItem from "../message-list-item/message-list-item";
import { fetchMessages, messageAdded } from "../../../actions";

import { connect } from "react-redux";
import WithSonetService from "../hoc/with-sonet-service";

import Spinner from "../../common/spinner/Spinner";

import "./message-list.css";

class MessageList extends Component {
  messagesEndRef = React.createRef();

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    if (this.messagesEndRef.current !== null) {
      this.messagesEndRef.current.scrollIntoView();
    }
  };

  render() {
    const { messages, loadingMessages } = this.props;
    if (loadingMessages || !messages.length) {
      if (!loadingMessages && messages.length < 1) {
        return (
          <div className="p-2 fixed-height" style={{ opacity: 0.2 }}>
            Empty
          </div>
        );
      }
      return (
        <center className="fixed-height">
          <Spinner />
        </center>
      );
    }
    return (
      <ul className="list-unstyled messages">
        {messages.map(message => {
          return (
            <li className="p-2 m-2" key={message.id}>
              <MessageListItem message={message} />
            </li>
          );
        })}
        <div ref={this.messagesEndRef} />
      </ul>
    );
  }
}

const mapStateToProps = ({
  chat: { messages, loadingMessages, userId, currentChannelId }
}) => {
  return { messages, loadingMessages, userId, currentChannelId };
};

const mapDispatchToProps = (dispatch, { sonetService }) => {
  return {
    addMessage: (content, sender, sendTime, senderId, avatar, id) =>
      dispatch(messageAdded(content, sender, sendTime, senderId, avatar, id))
  };
};

export default WithSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MessageList)
);
