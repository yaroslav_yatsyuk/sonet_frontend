import React, { Component } from "react";
import ConversationListItem from "../conversation-list-item/conversation-list-item";
import { connect } from "react-redux";
import WithSonetService from "../hoc/with-sonet-service";
import { fetchConversations } from "../../../actions";
import Spinner from "../../common/spinner/Spinner";
import { Link } from "react-router-dom";
import "./conversation-list.css";

const ConversationList = ({ conversations }) => {
  return (
    <ul className="conversation-list">
      {conversations.map(conversation => {
        return (
          <li key={conversation.id}>
            <Link
              to={`${conversation.id}`}
              style={{ textDecoration: "none", color: "white" }}
            >
              <ConversationListItem conversation={conversation} />
            </Link>
            <hr className="m-0" />
          </li>
        );
      })}
    </ul>
  );
};

class ConversationListContainer extends Component {
  componentDidMount() {
    const { fetchConversations, userId, token } = this.props;
    fetchConversations(userId, token);
  }

  render() {
    const { conversations, loadingConversations } = this.props;
    if (loadingConversations || !conversations.length) {
      if (!loadingConversations && !conversations.length) {
        return (
          <div className="no-conversations">
            <h1 className="p-4" style={{ opacity: 0.2 }}>
              You don't any conversations yet
            </h1>
          </div>
        );
      }
      return (
        <center>
          <Spinner />
        </center>
      );
    }
    return <ConversationList conversations={conversations} />;
  }
}

const mapStateToProps = ({
  chat: { conversations, loadingConversations, userId },
  user: { token }
}) => {
  return { conversations, loadingConversations, userId, token };
};

const mapDispatchToProps = (dispatch, { sonetService }) => {
  return {
    fetchConversations: (userId, token) =>
      fetchConversations(sonetService, dispatch, userId, token)()
  };
};

export default WithSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ConversationListContainer)
);
