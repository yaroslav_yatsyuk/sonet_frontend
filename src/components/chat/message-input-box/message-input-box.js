import React, { Fragment } from "react";
import { messageInputChange, messageInputClear } from "../../../actions";
import { connect } from "react-redux";
import WithSonetService from "../hoc/with-sonet-service";
import "./message-input-box.css";

const MessageInputBox = ({
  messageLabel,
  messageInputChange,
  messageInputClear,
  sonetService,
  currentChannelId,
  userId
}) => {
  const onSendMessage = event => {
    event.preventDefault();
    if (messageLabel.trim() !== "") {
      sonetService.socketSend(currentChannelId, messageLabel.trim(), userId);
      messageInputClear();
    }
  };
  return (
    <Fragment>
      <hr className="m-0" />
      <div className="p-3">
        <form onSubmit={onSendMessage}>
          <input
            className="d-inline-block form-control chat-input w-80"
            type="text"
            placeholder="Enter your message here"
            value={messageLabel}
            onChange={event => messageInputChange(event.target.value)}
          />
          <input
            className="col-sm-2 btn btn-secondary chat-button float-right"
            type="submit"
            value="Send"
          />
        </form>
      </div>
    </Fragment>
  );
};

const mapStateToProps = ({
  chat: { messageLabel, userId, currentChannelId },
  user: { token }
}) => {
  return { messageLabel, userId, currentChannelId, token };
};

const mapDispatchToProps = dispatch => {
  return {
    messageInputChange: symbol => dispatch(messageInputChange(symbol)),
    messageInputClear: () => dispatch(messageInputClear())
  };
};

export default WithSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MessageInputBox)
);
