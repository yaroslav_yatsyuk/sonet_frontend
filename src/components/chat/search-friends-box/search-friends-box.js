import React from "react";
import { searchFriendsInputChange } from "../../../actions";
import { connect } from "react-redux";

const SearchFriendsBox = ({ searchFriendsLabel, onLabelChange }) => {
  return (
    <div className="p-2">
      <input
        className="p-2 w-100 form-control chat-input"
        value={searchFriendsLabel}
        type="text"
        onChange={event => onLabelChange(event.target.value)}
        placeholder="Search followings"
      />
      <h1 className="ml-2 d-inline-block h4 mt-3">You are following:</h1>
    </div>
  );
};

const mapStateToProps = ({ chat: { searchFriendsLabel } }) => {
  return { searchFriendsLabel };
};

const mapDispatchToProps = dispatch => {
  return {
    onLabelChange: symbol => dispatch(searchFriendsInputChange(symbol))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchFriendsBox);
