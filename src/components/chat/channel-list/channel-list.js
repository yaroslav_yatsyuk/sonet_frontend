import React, { Component } from "react";
import withSonetService from "../hoc/with-sonet-service";
import { connect } from "react-redux";
import {
  fetchChannels,
  currentChannelIdChange,
  messageAdded,
  fetchMessages
} from "../../../actions";
import Spinner from "../../common/spinner";
import ChannelListItem from "./channel-list-item";
import "./channel-list.css";

class ChannelList extends Component {
  componentDidMount() {
    const {
      fetchChannels,
      id,
      token,
      currentChannelIdChange,
      userId
    } = this.props;
    fetchChannels(id, token).then(conversations =>
      currentChannelIdChange(conversations[0].id, token, userId)
    );
  }

  componentWillUnmount() {
    const { sonetService } = this.props;
    sonetService.socketDisconnect();
  }

  changeChannel(id) {
    const { currentChannelIdChange, token } = this.props;
    currentChannelIdChange(id, token);
  }

  render() {
    const { loadingChannels, channels, currentChannelId } = this.props;
    if (loadingChannels || !channels) {
      return <div className="offset-sm-1 col-sm-2" />;
    }
    if (channels.length < 2) {
      return <div className="offset-sm-1 col-sm-2" />;
    }
    return (
      <div className="offset-sm-1 col-sm-2 mt-2">
        <ul className="list-group">
          {channels.map(channel => {
            let liClass = "list-group-item channel-item";
            if (channel.id === currentChannelId) {
              liClass += " active";
            }
            return (
              <li
                className={liClass}
                key={channel.id}
                style={{ cursor: "pointer" }}
                onClick={() => this.changeChannel(channel.id)}
              >
                <ChannelListItem channel={channel} />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = ({
  chat: { channels, loadingChannels, currentChannelId, userId },
  user: { token }
}) => {
  return { channels, loadingChannels, currentChannelId, token, userId };
};

const mapDispatchToProps = (dispatch, { sonetService }) => {
  return {
    fetchChannels: (chatId, token) =>
      fetchChannels(sonetService, dispatch, chatId, token)(),
    fetchMessages: (channelId, token) =>
      fetchMessages(sonetService, dispatch, channelId, token)(),
    currentChannelIdChange: (channelId, token, userId) =>
      currentChannelIdChange(sonetService, dispatch, channelId, token, userId),
    addMessage: (content, sender, sendTime, senderId, avatar, id) =>
      dispatch(messageAdded(content, sender, sendTime, senderId, avatar, id))
  };
};

export default withSonetService()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChannelList)
);
