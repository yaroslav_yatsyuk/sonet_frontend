import React from "react";

const ChannelListItem = ({ channel }) => {
  let trunName;
  if (channel.name.length > 20) {
    trunName = channel.name.substring(0, 20) + "...";
  } else {
    trunName = channel.name;
  }
  return <h1 title={channel.name}>{trunName}</h1>;
};

export default ChannelListItem;
