import React from "react";
import { Button, Input, Form} from 'reactstrap';
import './poll-form.css';

class  PollForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = { inputs: ['input-0'],
            question: '',
            choices: []
        };
    }

    appendInput() {
        let newInput = `input-${this.state.inputs.length}`;
        this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
    }

    addPoll = () => {
        console.log("add poll");
    };

    onChangeQuetion = (e) => {
        console.log(e.target.value);
        this.setState({
            question: e.target.value
        })
    };


    onChangeСhoice = (e) => {

    };

    render() {
        return (
            <div className="poll-container">
                 <Input placeholder="Enter question" onChange = {this.onChangeQuetion} />
                <Form className="form-poll">
                     <Input placeholder="Enter choice" onChange = {this.onChange}/>
                    <div id="dynamicInput">
                        {this.state.inputs.map(input =>  <Input key={input} onChange = {this.onChangeСhoice}
                                                                placeholder="Enter choice" className="input-choice"/>
                                                               )}
                    </div>
                </Form>
                <div className="button-add-choice">
                <Button onClick={ () => this.appendInput() }>
                    ADD CHOICE
                </Button>
                </div>
                <Button onClick={this.addPoll} />
            </div>
        )
    }

}

export default PollForm;
