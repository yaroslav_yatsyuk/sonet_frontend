import React from 'react';
import API from "../../../utills/API";
import {connect} from "react-redux";
import fetchAllPolls from "../../../actions/fetchAllPolls";
import Poll from "../poll/poll";


class PollList extends React.Component {

    state = {
        polls: []
    };



    componentDidMount() {
        console.log(this.props.post_id);
        /*API().get(`/polls/posts/${this.props.post.id}`,
            {headers: {
                    Authorization : `Bearer_${this.props.user.token}`
                }
            }) .then(resp => {
                console.log("eeeeeeeee" , resp.data);
                const payload = {
                    post_id: this.props.post.id,
                    polls: resp.data
                };
            this.props.fetchAllPolls(resp.data);
                this.setState({
                    polls: resp.data
                });
                console.log("state",this.state)
            }
        );*/
    }

    render() {
        const {post, polls} = this.props;

        console.log("post_id" , post);
        return (
            <div>
                { polls === undefined || (polls.length === 0) ? <div> </div> : polls.map((poll) => (
                    <div key={poll.id}>
                            <Poll poll={poll} key={poll.id} editing = {this.props.editing} />
                    </div>
                ))
                }
            </div>
        )
    }

}

/*div>
                    {this.props.user.post.posts.length === 0 ? <NoPost/> : this.props.user.post.posts.map((post) => (
                        <div key={post.id}>
                            { post.editing ?
                                <EditPost post={post} key={post.id} current = {"all-post"}/> :
                                <Post post={post} key={post.id} current = {"all-post"} isNews = {false} /> }
                            <PollList post_id={post.id} />
                        </div>
                    ))}
                </div>*/

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};


const mapDispatchToProps =(dispatch) => {
    return {
        fetchAllPolls: (payload) => dispatch(fetchAllPolls(payload))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(PollList);
