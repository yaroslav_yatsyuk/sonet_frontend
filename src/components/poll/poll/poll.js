import React from "react";
import API from "../../../utills/API";
import {connect} from "react-redux";
import './poll.css';


let counter = 0;


class Poll extends React.Component {

    state = {
        isExitsChoice: null,
        current_choice: null,
        percents_choice: [],
        percents: [],
        count: 0
    };



    componentDidMount() {
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        if(this.state.isExitsChoice === null) {
            API().get(`/votes/exist?user_id=${this.props.user.id}&poll_id=${this.props.poll.id}`,
                {headers: {
                        Authorization : `Bearer_${this.props.user.token}`
                    }
                }) .then(resp => {
                    this.setState({
                        isExitsChoice: resp.data
                    });
                if (this.state.isExitsChoice === true) {
                    API().get(`/votes?user_id=${this.props.user.id}&poll_id=${this.props.poll.id}`,
                        {
                            headers: {
                                Authorization: `Bearer_${this.props.user.token}`
                            }
                        }).then(resp => {
                        this.setState({
                            current_choice: resp.data.user_choice,
                            percents_choice: resp.data.percents,
                            percents: resp.data.percents.map((per) => per.percent),
                            count: resp.data.percents.map((per) => per.countOfVoice).reduce(reducer)
                        });
                        console.log(this.state.percents_choice);
                    })
                }
                }
            );
        }
    }

    vote = (id) => {
        console.log("vote");
      console.log(id);
      console.log("current choice", this.state.current_choice);

      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      console.log(!(id === this.state.current_choice));
      if(!(id == this.state.current_choice)) {
          const data = {
              poll_id: this.props.poll.id,
              user_id: this.props.user.id,
              choice_id: id
          };
          API().post('/votes', data,
              {
                  headers: {
                      Authorization: `Bearer_${this.props.user.token}`
                  }
              })
              .then(resp => {
                  console.log("add data", resp.data);
                  const data_response = resp.data;
                  this.setState({
                      percents_choice: resp.data.percents,
                      current_choice: resp.data.user_choice,
                      percents: resp.data.percents.map((per) => per.percent),
                      isExitsChoice: true,
                      count: resp.data.percents.map((per) => per.countOfVoice).reduce(reducer)
                  });
                  console.log(this.state.current_choice);
              });
      }else {
          API().delete(`/votes?user_id=${this.props.user.id}&poll_id=${this.props.poll.id}`,
              {
                  headers: {
                      Authorization: `Bearer_${this.props.user.token}`
                  }
              })
              .then(resp => {
                  console.log("delete data");
                  const data_response = resp.data;
                  this.setState({
                      current_choice: null,
                      percents_choice: [],
                      percents: [],
                      isExitsChoice: false,
                  });
              });
      }
    };

    returnId = (choice) => {
        const index = Object.values(this.props.poll.choices).indexOf(choice);
        return Object.keys(this.props.poll.choices)[index];
    };





    returnPercent = (choice) => {
        const k = this.returnId(choice);
        const index = Object.values(this.props.poll.choices).indexOf(choice);
        const key = Object.keys(this.props.poll.choices)[index];
        const key_percent = this.state.percents_choice.map((per) => per.choiceId);
        let c = 0;
        for(let i = 0; i < key_percent.length; i++) {
            if(key == key_percent[i]) {
                c = this.state.percents[counter];
            }
        }
        return c;
    };


    deletePoll = () => {
        console.log(this.props.poll.id);
        API().delete(`/polls/${this.props.poll.id}`,
            {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
            .then(resp => {
                console.log("delete data");
                const data_response = resp.data;
                this.setState({
                    current_choice: null,
                    percents_choice: [],
                    percents: [],
                    isExitsChoice: false,
                });
            });
    };

    render() {
        const {poll, editing} = this.props;
        let counter = 0;


        if (this.state.isExitsChoice === false) {
            return (
                <div className="poll-percents">
                    {editing  ?
                    <button onClick={this.deletePoll} className="button_close_delete"> ✖ </button>
                    : <div> </div>
                    }
                        <div className="poll-quesion">
                        {poll.question}
                    </div>
                    <div className="choices-vote">
                        {
                            Object.values(poll.choices).map((choice) =>  <div className="choice-vote">
                                    <div className="button-click">
                                        <button className="button-vote" onClick={() => this.vote(this.returnId(choice))}>
                                           <input type="radio"/> {choice}
                                        </button>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            )
        }else {
            return (
                <div className="poll-percents">
                    {editing  ?
                        <button onClick={this.deletePoll} className="button_close_delete"> ✖ </button>
                        : <div> </div>
                    }
                    <div className="poll-quesion">
                     {poll.question}
                    </div>
                    <div className="choices-vote">
                    {
                        Object.values(poll.choices).map((choice) =>  <div className="choice-vote">
                            <div className="button-click">
                           <button className="button-vote" onClick={() => this.vote(this.returnId(choice))}>
                                {choice}
                            </button>
                            </div>
                            <div className="percents">
                                <div className="percent-text">{this.returnPercent(choice) === undefined ? 0 : this.returnPercent(choice)} %</div>
                                <div className="line-vote"><hr className="my-poll-line" id={counter++} style={{width: `${this.returnPercent(choice)}%`}}/>
                                </div>
                            </div>
                        </div>
                        )

                    }
                        <div className="all-vote">  All voted:  {this.state.count} </div>
                    </div>
                </div>
            )
        }
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};



export default connect(mapStateToProps, null)(Poll);
