import React, {Component} from 'react';
import ListOfPages from "../ListOfPages";
import Button from "reactstrap/es/Button";
import Input from "reactstrap/es/Input";
import Row from "reactstrap/es/Row";
import ButtonGroup from "reactstrap/es/ButtonGroup";
import '../Profile/Profile.scss';
import './UserGallery.css';
import Gallery from 'react-grid-gallery';
import {connect} from "react-redux";
import API from "../../utills/API";
import Modal from "reactstrap/es/Modal";
import Spinner from "../common/spinner";

class UserGallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            actorId: this.props.actorId,
            images: [],
            currentImage: 0,
            currentCaption: "",
            currentPage: 1,
            totalPages: 0,
            isModalOpen: false,
            imageFile: '',
            imagePreviewUrl: '',
            hasError: false,
            uploadImageCaption: '',
            loading: true
        };

        this.onCurrentImageChange = this.onCurrentImageChange.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
    }

    componentDidMount() {
        this.loadImages();
    }

    loadImages = () => {
        this.setState({
            loading: true
        });

        const {currentCaption, currentPage, actorId} = this.state;

        let request = `/images/find?actorId=` + actorId + `&page=` + (currentPage - 1) + `&size=10&sort=creation_time,desc`;

        if (currentCaption !== "") {
            request += `&caption=` + currentCaption;
        }

        API().get(request, {
            headers: {
                Authorization: `Bearer_${this.props.userToken}`
            }
        })
            .then(response => {
                this.setState({
                    loading: false,
                    images: response.data.images.map(image => this.mapImage(image)),
                    totalPages: response.data.totalPages
                });
            }).catch(e => {
            this.setState({
                loading: false
            });
        });
    };

    mapImage = (image) => ({
        id: image.id,
        src: image.url,
        thumbnail: image.url,
        caption: image.caption
    });

    onCurrentImageChange(index) {
        this.setState({ currentImage: index });
    }

    deleteImage() {
        this.setState({
            loading: true
        });

        const images = this.state.images.slice();
        const deletedImage = images.splice(this.state.currentImage, 1);

        API().delete(`/images/` + deletedImage[0].id, {
            headers: {
                Authorization: `Bearer_${this.props.userToken}`
            }
        })
            .then(() => {
                this.setState({
                    loading: false,
                    images
                });
            }).catch(e => {
            console.log(e);
            this.setState({
                loading: false
            });
        });
    }

    onCurrentCaptionChange = (e) => {
        this.setState({
            currentCaption: e.target.value
        });
    };

    onClickFindButton = () => {
        this.loadImages();
    };

    openModal = () => {
        this.setState({
            isModalOpen: true
        });
    };

    closeModal = () => {
        this.setState({
            loading: false,
            isModalOpen: false
        });
    };

    handleUploadImage = (e) => {
        e.preventDefault();
        if (this.state.uploadImageCaption === '' || this.state.imageFile === '') {
            this.setState({
                hasError: true
            })
        } else {
            this.setState({
                hasError: false
            });
            this.saveImage();
        }
    };

    saveImage() {
        this.setState({
            loading: true
        });
        let formData = new FormData();
        formData.append('file', this.state.imageFile);
        API().post(`/users/${this.props.actorId}/files`, formData, {
            headers: {
                Authorization: `Bearer_${this.props.userToken}`
            }
        }).then(resp => {
            const url = resp.data.message;

            API().post(`/images`, {
                actor_id: this.state.actorId,
                url: url,
                caption: this.state.uploadImageCaption
            }, {
                headers: {
                    Authorization: `Bearer_${this.props.userToken}`
                }
            }).then(resp => {
                if (this.state.currentCaption === '' || this.state.currentCaption.toLocaleLowerCase().includes(resp.data.caption.toLowerCase())) {
                    const images = this.state.images.slice();
                    const image = this.mapImage(resp.data);
                    images.splice(0, 0, image);

                    this.setState({
                        images: images
                    });
                }

                this.closeModal();
            }).catch(e => {
                this.setState({
                    loading: false
                });
            });
        })
            .catch(e => {
                this.setState({
                    loading: false
                });
            });
    }

    handleImageChange = (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let image = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                imageFile: image,
                imagePreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(image)
    };

    uploadImageCaptionChange = (e) => {
        this.setState({
            uploadImageCaption: e.target.value
        })
    };

    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img className="backgroundProfileImg" src={imagePreviewUrl} alt="uploaded image"/>);
        }
        return (
            <div className="stars">
                <div className="twinkling">
                    <div className="container">
                        <Row className="page">
                            <div className="col-9">
                                {
                                    this.state.loading ?
                                        <Modal isOpen={true}><Spinner/></Modal> :
                                        <div>
                                            <Row className="justify-content-between content">
                                                <ButtonGroup>
                                                    <Input value={this.state.currentCaption} type="text" onChange={this.onCurrentCaptionChange}/>
                                                    <Button onClick={this.onClickFindButton}>Find</Button>
                                                </ButtonGroup>
                                                <Button onClick={this.openModal}>Upload image</Button>
                                            </Row>
                                            <br/>
                                            <Gallery
                                                images={this.state.images}
                                                enableLightbox={true}
                                                showImageCount={false}
                                                backdropClosesModal={true}
                                                enableImageSelection={false}
                                                currentImage={this.state.currentImage}
                                                currentImageWillChange={this.onCurrentImageChange}

                                                customControls={[
                                                    <Button key="deleteImage" onClick={this.deleteImage}>Delete Image</Button>
                                                ]}
                                            />
                                            <Modal
                                                isOpen={this.state.isModalOpen}
                                                onRequestClose={this.closeModal}
                                                contentLabel="Upload image modal">
                                                <div className="row editUserModal">
                                                    <div className="editUserModalHeader col-12 pt-2 d-flex flex-row">
                                                        <button className="closeModalButton m-1"
                                                                onClick={this.closeModal}>
                                                            <i
                                                                className="fa fa-times"/></button>
                                                        <span
                                                            className="edit-your-info-text text-left">Upload image</span>
                                                        <form className="edit-save-form d-flex justify-content-end"
                                                              onSubmit={this.handleUploadImage}>
                                                            <button className="btn btn-primary editUserSaveButton float-xs-right">
                                                                Upload
                                                            </button>
                                                        </form>
                                                    </div>
                                                    <div
                                                        className="col-12 editUserModalElementsDiv d-flex flex-column mt-2">
                                                        <div className="editBackgroundProfileImgDiv">
                                                            <form>
                                                                <label className="btn btn-default BackgroundImageInput">
                                                                    <i className="fa fa-camera"/> <input type="file"
                                                                                                         hidden
                                                                                                         onChange={this.handleImageChange}/>
                                                                </label>
                                                            </form>
                                                            {$imagePreview}
                                                        </div>

                                                        <div className="editUserModalElement d-flex flex-column">
                                                            <span className="textareaEditUserUpper">Caption</span>
                                                            <input value={this.state.uploadImageCaption}
                                                                   onChange={this.uploadImageCaptionChange}
                                                                   placeholder="Type a caption"
                                                                   className="textareaEditUser"
                                                            />
                                                            {this.state.hasError ?
                                                                <span className="errorNicknameAlreadyExists">
                                                        Please, select image and enter the caption</span> : ''}
                                                        </div>
                                                    </div>
                                                </div>
                                            </Modal>
                                        </div>
                                }
                            </div>
                            <div className="col-3">
                                <ListOfPages/>
                            </div>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    actorId: state.user.id,
    userToken: state.user.token
});

export default connect(mapStateToProps)(UserGallery);