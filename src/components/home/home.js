import React, {Component} from 'react';
import {Container, Row, Col, TabContent, TabPane, Nav, NavItem, NavLink} from "reactstrap";
import ListOfPages from '../ListOfPages';
import News from '../post/news/news';
import TopNews from "../post/top-news/top-news";
import LikedNews from "../post/liked-news/liked-news";
import LikedFollowingNews from "../post/liked-following-news/liked-following-news";
import './home.scss';
import SearchNews from '../post/search-news/search-news';
import {fetchAllNews} from "../../actions/fetchAllNews";
import connect from "react-redux/es/connect/connect";
import BurgerMenu from "../ListOfPages/burger-menu";



class Home extends Component {

    state = {
        tab: "all-news",
        search_input: '',
        afterSearch: false
    };


    choseTab = (tabOne) => {
        this.setState({
            tab: tabOne,
            search_input: ''
        });
    };


    onChangeNickName = (e) => {
        const text = e.target.value;
        if (text === '') {
            this.setState({
                tab: "all-news",
                search_input: '',
                afterSearch: true
            })
        } else {
            this.setState({
                search_input: text,
                tab: "search-news",
                afterSearch: false
            });
        }
    };


    componentDidMount() {
        this.props.fetchAllNews([]);
        this.setState({
            tab: "all-news",
            search_input: '',
            afterSearch: false
        });
    }


    render() {
        let tabComponent;
        if (this.state.search_input === '') {
            if (this.state.tab === "all-news") {
                tabComponent = <News afterSearch={this.state.afterSearch}/>;
            } else if (this.state.tab === "top-news") {
                tabComponent = <TopNews/>
            }
            else if (this.state.tab === "liked-news") {
                tabComponent = <LikedNews/>
            }
            else if (this.state.tab === "liked-following-news") {
                tabComponent = <LikedFollowingNews/>
            }
        } else if (this.state.search_input !== '') {
            if (this.state.tab === "search-news" && this.state.search_input !== '') {
                tabComponent = <SearchNews searchNickname={this.state.search_input}/>
            }
        }
        return (
            <div className="stars home-page">
                <BurgerMenu/>
                <div className="mobile-menu">
                    <div className="mobile-menu_buttons">
                        <button autoFocus={true} className='mobile-menu__button'
                                onClick={() => this.choseTab("all-news")}>All news
                        </button>
                        <button className='mobile-menu__button'
                                onClick={() => this.choseTab("top-news")}>Top
                        </button>
                        <button className='mobile-menu__button'
                                onClick={() => this.choseTab("liked-news")}>Liked
                        </button>
                        <button className='mobile-menu__button'
                                onClick={() => this.choseTab("liked-following-news")}>Following Liked
                        </button>
                    </div>
                </div>
                <div className="twinkling">
                    <Container style={{display: "flex", flexDirection: "column"}} className="home-container">
                        <div className="profileHeader">
                            <Row className="profileHeaderGrid">
                                <Col xs={{sise: 12, offset: 1}} sm="7" md="9" >
                                    <Row className="home-menu-row">
                                        <Col xs="12" lg="12">
                                            <div className="topnav">
                                                <div className="buttons-news">
                                                    <button autoFocus={true} className='square_btn news-btn'
                                                            onClick={() => this.choseTab("all-news")}>All news
                                                    </button>
                                                    <button className='square_btn news-btn'
                                                            onClick={() => this.choseTab("top-news")}>Top
                                                    </button>
                                                    <button className='square_btn news-btn'
                                                            onClick={() => this.choseTab("liked-news")}>Liked
                                                    </button>
                                                    <button className='square_btn news-btn'
                                                            onClick={() => this.choseTab("liked-following-news")}>Following Liked
                                                    </button>
                                                </div>
                                                <div className="search-box-news">
                                                    <div className="search-container">
                                                        <form className="form-input top-search-form">
                                                            <input type="text" placeholder="Enter word.." name="search"
                                                                   onChange={this.onChangeNickName}/>
                                                            <button type="submit"><i className="fa fa-search"> </i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </div>
                        <Row>
                            <div className="col-9 profileContent">
                                {tabComponent}
                            </div>
                            <div className="offset-1 col-2 home-aside-menu">
                                <ListOfPages/>
                            </div>
                        </Row>

                    </Container>
                </div>
            </div>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllNews: (data) => dispatch(fetchAllNews(data)),
    }
};

export default connect(null, mapDispatchToProps)(Home);
