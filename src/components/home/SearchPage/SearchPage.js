import React from "react";
import "./SearchPage.scss";



class SearchPage extends React.Component {
    constructor() {
        super();
        this.state = {
          searchboxStyle: {
            width: '120px',
            background: '#eee'
          }
        }
        
        this._handleTextFocus = this._handleTextFocus.bind(this);
        this._handleClick = this._handleClick.bind(this);
        this._handleTextBlur = this._handleTextBlur.bind(this);
      }
      
      _handleTextFocus(e) {
        e.preventDefault();
        
        this.setState({
          searchboxStyle: {
            width: '100%',
            background: '#fff'
          }
        })
      }
      
      _handleTextBlur(e) {
        e.preventDefault();
        if (!this._searchBox.value && !this._searchBox.value.length > 0) 
          this.setState({
            searchboxStyle: {
              width: '120px',
              background: '#eee'
            }
          })
      }
      
      _handleClick(e) {
        e.preventDefault();
      }
      
      render() {
        return (
          <div>
            <form onSubmit={this._handleClick}>
              <div className="search-box"
                style={this.state.searchboxStyle}>
                
                  <input className="searchLike" type="text" 
                    ref={f => this._searchBox = f}
                    placeholder="Search"
                    autoComplete="off"
                    onFocus={this._handleTextFocus}
                    onBlur={this._handleTextBlur}
                    className="auto-expand" />
                
                <button className="search-button">
                  <span className="iconSearch"></span>
                </button>
              </div>
            </form>
          </div>
        );
      }
    }

export default SearchPage;