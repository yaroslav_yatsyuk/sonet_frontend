import React, {Component} from "react";
import './global-search.css';
import API from "../../utills/API";
import {connect} from "react-redux";
import ListOfPages from "../ListOfPages";
import SearchUserItem from '../search-user-item'
import {Link} from "react-router-dom";
import classNames from "classnames";
import ModalEnableGeoapiPermission from "../common/modal-enable-geoapi-permission";
import {getLocation} from "../../actions/getLocation";
import Spinner from "../common/spinner";
import FullScreenSpinner from "../common/full-screen-spinner";

class GlobalSearch extends Component {

    state = {
        searchString: '',
        users: [],
        activeTab: "all",
        showModalGeoApiPermissionAlert: false,
        loading: false
    };

    updateUser(id, updatedUser) {
        this.setState(prevState => {
            users: prevState.users.map(user => {
                if (user.id === id) {
                    return updatedUser;
                }
                return user;
            });
        })
    }

    refreshUsersIsOnline = () => {
        this.state.users.map(user => {
            API().get(`/users/${user.id}/is-online`, {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`,
                }
            }).then(async resp => {
                this.updateUser(user.id, {
                    ...user,
                    isOnline: resp.data.isOnline,
                    lastTimeOnline: resp.data.lastActivity,
                    distance: resp.data.distance
                });
            }).catch(error => {
                console.log("error during get user is online")
            });
        });
        setTimeout(this.refreshUserIsOnline, 1000 * 60);
    };

    onCloseModalGeoApiPermission = () => {
        this.setState({
            showModalGeoApiPermissionAlert: false
        });
    };

    featchAllUsers() {
        this.setState({
            loading: true
        });
        API().get(`/users`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            console.log("resp", resp.data);
            this.setState({
                users: resp.data,
                loading: false
            });

            setTimeout(this.refreshUserIsOnline, 1000 * 60);

        }).catch(error => {
            console.log("error", error);
        });
    }

    handleChange = (e) => {
        this.setState({searchString: e.target.value});
    };

    featchNearUsers() {
        this.setState({
            loading: true
        });
        API().get(`/locations/users/${this.props.user.id}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            console.log("resp", resp.data);
            this.setState({
                users: resp.data,
                loading: false
            });
        }).catch(error => {
            console.log("error", error);
        });
    }

    componentDidMount = async () => {
        this.featchAllUsers();
        // this.refreshUsersIsOnline()
        await this.props.getLocation();
        if (!this.props.user.coords.isPresent && this.props.user.coords.isDeniedPermission) {
            this.setState({
                showModalGeoApiPermissionAlert: true
            });
        }
    };

    toggleTab = (tab) => {
        this.setState({
            activeTab: tab
        });
        if (tab === "all") {
            this.featchAllUsers();
        } else if (tab === "near") {
            if (this.props.user.coords.isPresent) {
                this.featchNearUsers();
            }
        }
    };

    render() {
        let searchUsers = this.state.users,
            searchString = this.state.searchString.trim().toLowerCase();
        if (searchString.length > 0) {
            searchUsers = searchUsers.filter(function (user) {
                return user.nickname.toLowerCase().match(searchString);
            });
        }
        return <div className="stars">
            <div className="twinkling">
                <div className="container">
                    <div className="row">
                        <div className="col-9 searchContent mt-5 pt-3">
                            <div className="row search-users-header">
                                <div className="col-10">
                                    <input
                                        type="text"
                                        value={this.state.searchString}
                                        onChange={this.handleChange}
                                        placeholder="Search by nickname..."
                                        className="search-textarea mb-4"
                                    />
                                </div>
                                <div className="col-2">
                                    <i className="fa fa-gear search-settings-icon"/>
                                </div>
                            </div>
                            <ModalEnableGeoapiPermission isOpen={this.state.showModalGeoApiPermissionAlert}
                                                         onClose={this.onCloseModalGeoApiPermission}/>
                            <div className="search-content-tabs">
                                <div
                                    className={classNames("search-content-tabs__item", {"search-content-tabs__item--active": this.state.activeTab === "all"})}
                                    onClick={() => this.toggleTab("all")}>
                                    All users
                                </div>
                                <div className={classNames("search-content-tabs__item", {
                                    "search-content-tabs__item--active": this.state.activeTab === "near",
                                    "disabled": !this.props.user.coords.isPresent
                                })} onClick={() => this.toggleTab("near")}>Near users
                                </div>
                                {this.state.loading && <FullScreenSpinner/>}
                            </div>
                            {searchUsers.sort((user1, user2) => {
                                if (this.state.activeTab === "all") {
                                    return user1.isOnline ? -1 : 1;
                                } else if (this.state.activeTab === "near") {
                                    return user1.distance > user2.distance ? 1 : -1;
                                }
                            }).map((user) => {
                                return (
                                    <SearchUserItem user={user}/>
                                )
                            })}
                        </div>
                        <div className="offset-1 col-2 listOfPagesSearch">
                            <ListOfPages/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

const mapDispatchToProps = dispatch => ({
    getLocation: async () => dispatch(getLocation())
});

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(GlobalSearch);

