import React from "react";
import API from "../../utills/API";
import { connect } from "react-redux";
import addPostLikes from "../../actions/addPostLikes";
import "./Likes.scss";
import starwars from "./starwars.png";
import starwars2 from "./starwars2.png";
import Modal from "reactstrap/es/Modal";

class Likes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: [],
      updated: false,
      searchString: ""
    };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  updateSearch(event) {
    this.setState({ search: event.target.value.substr(0, 20) });
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  afterOpenModal() {
    this.subtitle.style.color = "#f00";
  }

  componentDidMount() {
    API()
      .get(`likes/posts/${this.props.post.id}`, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(result => {
        this.setState({ likes: result.data });
      });
  }

  handleSubmit = event => {
    event.preventDefault();
    let usrId = this.state.likes.find(
      user => user.user_id === this.props.user.id
    );
    let pstId = this.state.likes.find(
      post => post.post_id === this.props.post.id
    );
    if (usrId != null && pstId != null) {
      API()
        .delete(
          `/likes/posts/${this.props.post.id}/users/${
            this.props.user.id
          }`,
          {
            headers: {
              Authorization: `Bearer_${this.props.user.token}`
            }
          }
        )
        .then(() => {
          this.componentDidMount();
        });
    } else {
      API()
        .post(
          `/likes`,
          {
            post_id: this.props.post.id,
            user_id: this.props.user.id
          },
          {
            headers: {
              Authorization: `Bearer_${this.props.user.token}`
            }
          }
        )
        .then(() => {
          this.componentDidMount();
        });
    }
  };
  handleChange = e => {
    this.setState({ searchString: e.target.value });
  };

  render() {
    let libraries = this.state.likes,
      searchString = this.state.searchString.trim().toLowerCase();
    if (searchString.length > 0) {
      libraries = libraries.filter(function(i) {
        return i.user_name.toLowerCase().match(searchString);
      });
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="likeDiv">
            <div onClick={this.closeModal} />
            <div onClick={this.closeModal} />
            <div className="like-image-andriy">
              <input
                type="image"
                src={
                  this.state.likes.find(
                    user => user.user_id === this.props.user.id
                  ) != null &&
                  this.state.likes.find(
                    post => post.post_id === this.props.post.id
                  ) != null
                    ? "/img/like-active.png"
                    : "/img/like.png"
                }
                className="imageLord"
              />
              {this.state.likes.length}
            </div>
            <br />
          </div>
        </form>
        <div className="showingLikes">
          {" "}
          {this.state.likes.length > 0 ? (
            <button className="buttonForLike" onClick={this.openModal}>
              <p className="pForAll">All likes</p>
            </button>
          ) : (
            <p className="noLikesYet">No likes</p>
          )}
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal"
        >
          <div className="row">
            <div className="col-12 my-2 d-flex justify-content-center">
              <button
                onClick={this.closeModal}
                className="btn btn-primary float-xs-right"
              >
                Close
              </button>
            </div>
            <div className="col-8 offset-2 mt-2 d-flex justify-content-center">
              <input
                type="text"
                value={this.state.searchString}
                onChange={this.handleChange}
                placeholder="Search...."
                className="pb-cmnt-textarea"
              />
            </div>
            <div className="col-12 mt-2 d-flex justify-content-center">
              <center>
                <ul>
                  {libraries.map(function(i) {
                    return (
                      <li className="listForLikes">
                        <p className="listOfUsers">@{i.user_name}</p>{" "}
                        <a href={i.url}>{i.url}</a>
                      </li>
                    );
                  })}
                </ul>
              </center>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  addPostLike: async payload => dispatch(addPostLikes(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Likes);
