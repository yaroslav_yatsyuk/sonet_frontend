import React from "react";
import "./ko-ko-ko.css";

class KoKoKo extends React.Component {

    render() {
        return (
            <div className="ko-wrapper">
                <div className="ko-layer"></div>
                <img src="/img/petuh.png" alt="" className="ko-img"/>
                <div className="ko-text">Гнилий петух</div>
                <div className="ko-text-small">Засцав?</div>
                <a className="ko-link" href="https://support.google.com/chrome/answer/114662?co=GENIE.Platform%3DDesktop&hl=ru-RF">
                    Как включить геодание для сайта
                </a>
            </div>
        )
    }

}

export default KoKoKo;