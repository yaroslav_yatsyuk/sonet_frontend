import React from "react";
import "./empty-list.css";

const EmptyList = props => {

    return (
        <div className="empty-list">
            <p className="empty-list__text">
                {props.children}
            </p>
            {/*<div className="empty-list__photo">*/}
            {/*    <img src="/" className="empty-list__photo-img"/>*/}
            {/*</div>*/}
        </div>
    )
};

export default EmptyList;