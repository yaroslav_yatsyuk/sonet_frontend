import React from "react";
import Spinner from "../spinner";
import "./full-screen-spinner.css";

class FullScreenSpinner extends React.Component {

    render() {
        return (
          <div className="fullscreen-spinner-container">
              <Spinner/>
          </div>
        );
    }
}

export default FullScreenSpinner;