import React, {Fragment} from "react";
import FullScreenSpinner from "../full-screen-spinner";

class SpinnerProvider extends React.Component {


    state = {
        loading: false
    };

    setIsLoading = isLoading => {
        this.setState({
           loading: isLoading
        });
    };

    render() {
        return (
            <Fragment>
                {
                    React.cloneElement(this.props.children, {
                        loading: this.setIsLoading
                    })
                }
                {this.state.loading && <FullScreenSpinner/>}
            </Fragment>
        );
    }
}

export default SpinnerProvider;