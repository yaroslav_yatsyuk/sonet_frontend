import React from "react";
import {Modal,Button} from "reactstrap";
import "./modal-enable-geoapi-permission.css";

class ModalEnableGeoapiPermission extends React.Component {

    render() {
        return (
            <Modal isOpen={this.props.isOpen} onClose={() => this.props.onClose()}>
                <div className="m-enable-permission">
                    <div className="m-enable-permission__text">
                        You disabled geo data permission!
                    </div>
                    <a className="m-enable-permission__link" href="https://support.google.com/chrome/answer/114662?co=GENIE.Platform%3DDesktop&hl=ru-RF">
                        How to enable location data for your site
                    </a>

                    <div className="m-enable-permission__close">
                        <Button color="primary" onClick={() => this.props.onClose()}>Close</Button>
                    </div>
                </div>
            </Modal>
        )
    }
}

export default ModalEnableGeoapiPermission;