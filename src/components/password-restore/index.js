import React from "react";
import {Redirect} from "react-router-dom";
import API from "../../utills/API";
import Logo from "../common/logo";
import Input from "../common/input";
import Button from "../common/button";
import {validateEmail} from "../../utills";
import Notification from "./notification";
import FullScreenSpinner from "../common/full-screen-spinner";

class PasswordRestore extends React.Component {

    state = {
        success: null,
        error: null,
        email: '',
        validate: false,
        loading: false
    };

    handleInputChange = e => {
        this.setState({
            email: e.target.value,
        });

        if (this.state.validate) {
            this.setState({
                error: validateEmail(e.target.value)
            });
        }
    };


    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            validate: true,
            loading: true
        });

        API({security: false}).post("/auth/request-password-reset", this.state.email,{
            headers: {
                "Content-type" : "text/plain"
            }
        }).then(resp => {
            if (resp.data.result === "SUCCESS") {
                this.setState({
                    success: true
                });
                console.log("password restore success");
            } else if (resp.data.result === "ERROR") {
                this.setState({
                    success: false
                });
                console.log("password restore error excactly");
            }

            this.setState({
               loading: false
            });
            console.log("password restore error unknown");

        }).catch(error => {
            this.setState({
                success: false,
                loading: false
            });
            console.log("password restore error");
        });
    };

    render() {
        return (
            <div className="stars">
                <div className="twinkling">
                    { this.state.loading && <FullScreenSpinner/>}
                    <div className='container-alt'>
                        <Logo/>
                        <div className='form-container' style={{"paddingTop": 0}}>
                            <form className="simple_form new_user" id="new_user" onSubmit={this.handleSubmit}>
                                {this.state.success != null && <Notification success={this.state.success}/>}
                                <div className='fields-group'>
                                    <Input
                                        name="email"
                                        type="email"
                                        handleInputChange={this.handleInputChange}
                                        value={this.state.email}
                                        error={this.state.error}
                                        label="email"
                                    />
                                </div>
                                <Button text="reset password"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default PasswordRestore;