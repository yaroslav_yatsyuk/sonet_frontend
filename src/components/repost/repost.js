import React from "react";
import repost from "./repp2.png";
import "./repost.scss";
import API from "../../utills/API";
import addPostRepost from "../../actions/addPostRepost";
import {connect} from 'react-redux';

class Repost extends React.Component{
    handleSubmit = (event) => {
        event.preventDefault();
        API().post(`/posts`, {
            creator_id: this.props.post.creator_id,
            text: this.props.post.text,
            owner_id: this.props.user.id,
            images: this.props.post.images  
        },
        {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }
    )
    }
    render(){
        return(
            <form onSubmit={this.handleSubmit}>
            <div className = "mainDivForRepost">
                <input type="image" src={repost} alt={repost} className="imageRepost"/>
                </div>
                </form>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user
  });

  const mapDispatchToProps = dispatch => ({
    addPostRepost: async payload => dispatch(addPostRepost(payload)),
  });
export default connect(mapStateToProps, mapDispatchToProps)(Repost);