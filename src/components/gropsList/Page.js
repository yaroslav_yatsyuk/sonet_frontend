import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import "./Page.scss";

import AllGroups from "./Groups/AllGroups";
import ListOfPages from "../ListOfPages/ListOfPages";

export default class Page extends Component {
  render() {
    return (
      <div className="stars">
        <div className="twinkling">
          <Container className="groupList-page">
            <Row>
              <Col xs="10" lg="9">
                <div className="groups_area">
                  <AllGroups />
                </div>
              </Col>
              <Col xs="2" lg="3">
                <div className="listOfPages">
                  <ListOfPages />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}
