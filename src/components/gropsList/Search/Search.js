import React, { Component } from "react";
import "./Search.scss";

export default class Search extends Component {
  render() {
    return (
        <div className="groupList-page">
      <form className="search">
        <input
          type="text"
          className="plcholder"
          placeholder="Search here..."
          required
        />
        <button type="submit">Search</button>
      </form>
        </div>
    );
  }
}
