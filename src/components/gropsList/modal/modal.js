import React, { Fragment } from "react";
import Spinner from "../../common/spinner";
import { Modal, Input } from "reactstrap";
import { addGroup } from "../../../actions/addGroup";
import { fetchAllGroups } from "../../../actions/fetchAllGroups";
import { spinnerForGroup } from "../../../actions/spinnerForGroups";
import { connect } from "react-redux";
import API from "../../../utills/API";
import "./modal.scss";

class CreateModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      name: "",
      nickname: "",
      description: "",
      hasError: false,
      background: "",
      backgroundFile: "",
      backgroundPreviewUrl: "",
      backgroundLoaded: false,
      avatar: "",
      avatarFile: "",
      avatarPreviewUrl: "",
      avatarLoaded: false,
      createGroupLoading: false
    };
    this.toggle = this.toggle.bind(this);
  }

  async toggle() {
    if (this.state.modal === false) {
      await this.setState(prevState => ({
        modal: !prevState.modal
      }));
    } else
      await this.setState(prevState => ({
        modal: !prevState.modal
      }));
  }

  saveBackground() {
    let formData = new FormData();
    formData.append("file", this.state.backgroundFile);

    API()
      .post(`/users/${this.props.user.id}/files`, formData, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(resp => {
        this.setState({
          backgroundLoaded: true,
          background: resp.data.message
        });
        if (this.state.avatarLoaded) {
          this.handleSubmit();
        }
      });
  }

  _handleBackgroundChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let background = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        backgroundFile: background,
        backgroundPreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(background);
  }

  saveAvatar() {
    let formData = new FormData();
    formData.append("file", this.state.avatarFile);

    API()
      .post(`/users/${this.props.user.id}/files`, formData, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(resp => {
        this.setState({
          avatarLoaded: true,
          avatar: resp.data.message
        });
        if (this.state.backgroundLoaded) {
          this.handleSubmit();
        }
      });
  }

  _handleAvatarChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let avatar = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        avatarFile: avatar,
        avatarPreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(avatar);
  }

  nameChange(e) {
    const name = e.target.value;
    this.setState({
      name: name
    });
  }

  nicknameChange(e) {
    const nickname = e.target.value;
    this.setState({
      nickname: nickname
    });
  }

  descriptionChange(e) {
    const description = e.target.value;
    this.setState({
      description: description
    });
  }

  preHandleSubmit = event => {
    event.preventDefault();
    this.setState({
      createGroupLoading: true
    });
    this.props.spinnerForGroup(true);
    this.setState({ createGroupLoading: true });
    if (this.state.avatarFile !== "" && this.state.backgroundFile !== "") {
      this.saveAvatar();
      this.saveBackground();
    } else if (
      this.state.avatarFile !== "" &&
      this.state.backgroundFile === ""
    ) {
      this.setState({ backgroundLoaded: true });
      this.saveAvatar();
    } else if (
      this.state.avatarFile === "" &&
      this.state.backgroundFile !== ""
    ) {
      this.setState({ avatarLoaded: true });
      this.saveBackground();
    } else {
      this.handleSubmit();
    }
  };

  handleSubmit = () => {
    const newGroup = {
      name: this.state.name,
      nickname: this.state.nickname,
      description: this.state.description,
      creator_id: this.props.user.id,
      avatar_url: this.state.avatar,
      background_url: this.state.background
    };
    API()
      .post("/groups/create", newGroup, {
        headers: {
          Authorization: `Bearer_${this.props.user.token}`
        }
      })
      .then(res => {
        //     this.props.spinnerForGroup(false);
        const groupToCreate = res.data;
        this.props.addGroup(groupToCreate);
      })
      .then(data => {
        this.toggle();
      })
      .catch(error => {
        console.log(error);
        console.log(error.response);
        this.setState({
          createGroupLoading: false,
          hasError: true
        });
      });
  };

  render() {
    let { backgroundPreviewUrl, avatarPreviewUrl } = this.state;
    let $backgroundPreview = null;

    if (backgroundPreviewUrl) {
      $backgroundPreview = (
        <img className="editBackgroundGroupImg" src={backgroundPreviewUrl} />
      );
    } else {
      $backgroundPreview = (
        <img
          className="editBackgroundProfileImg"
          src="https://sonet-social-bucket.s3.amazonaws.com/17/ca2ce75dd4b344f2b06da0e2a4d6c084"
        />
      );
    }

    let $avatarPreview = null;
    if (avatarPreviewUrl) {
      $avatarPreview = (
        <img className="editGroup_logo" src={avatarPreviewUrl} />
      );
    } else {
      $avatarPreview = (
        <img
          className="editGroup_logo"
          src="https://sonet-social-bucket.s3.amazonaws.com/17/0a4638dbdd384f10b1453feb0094faec"
        />
      );
    }
    return (
      <Fragment>
        <button className="create_group" onClick={this.toggle}>
          <span className="add_text"> New group </span>
          <img
            className="plus_button"
            src="https://img.icons8.com/ios-filled/50/000000/add.png"
          />
        </button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <div className="row editUserModal">
            <div className="editUserModalHeader col-12 pt-2 d-flex flex-row">
              <button className="closeModalButton m-1" onClick={this.toggle}>
                <i className="fa fa-times" />
              </button>
              <span className="edit-your-info-text text-left">Add group</span>
              {this.state.createGroupLoading ? (
                <div className="editGroupSpinner">
                  <Spinner />
                </div>
              ) : (
                ""
              )}
              <form
                className="edit-save-form d-flex justify-content-end"
                onSubmit={this.preHandleSubmit}
              >
                <button className="btn btn-primary editUserSaveButton float-xs-right">
                  Save
                </button>
              </form>
            </div>
            <div className="col-12 editUserModalElementsDiv d-flex flex-column mt-2">
              <div className="editBackgroundProfileImgDiv">
                <form onSubmit={e => this._handleSubmitBackground(e)}>
                  <label className="btn btn-default BackgroundImageInput1">
                    <i className="fa fa-camera" />{" "}
                    <input
                      type="file"
                      hidden
                      onChange={e => this._handleBackgroundChange(e)}
                    />
                  </label>
                </form>
                {$backgroundPreview}
              </div>
              <div className="editProfileHeader">
                <div className="edit_profile_logo_container">
                  <form onSubmit={e => this._handleSubmitAvatar(e)}>
                    <label className="btn btn-default AvatarImageInput1">
                      <i className="fa fa-camera" />{" "}
                      <input
                        type="file"
                        hidden
                        onChange={e => this._handleAvatarChange(e)}
                      />
                    </label>
                  </form>
                  {$avatarPreview}
                </div>
              </div>

              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Name</span>
                <input
                  onChange={e => this.nameChange(e)}
                  placeholder="Add group name"
                  className="textareaEditUser"
                />
                {this.state.hasError ? (
                  <span className="errorNicknameAlreadyExists">
                    Name Already Exist
                  </span>
                ) : (
                  ""
                )}
              </div>
              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Nickname</span>
                <input
                  onChange={e => this.nicknameChange(e)}
                  placeholder="Add group nickname"
                  className="textareaEditUser"
                />
              </div>
              <div className="editUserModalElement d-flex flex-column">
                <span className="textareaEditUserUpper">Description</span>
                <input
                  onChange={e => this.descriptionChange(e)}
                  placeholder="Add group description"
                  className="textareaEditUser"
                />
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user,
  group: state.group,
  groupsPage: state.groupsPage,
  groupsLoading: state.groupsLoading
});

const mapDispatchToProps = dispatch => ({
  addGroup: async payload => dispatch(addGroup(payload)),
  fetchAllGroups: payload => dispatch(fetchAllGroups(payload)),
  spinnerForGroup: payload => dispatch(spinnerForGroup(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateModal);
