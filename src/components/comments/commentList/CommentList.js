import React, {Component, Fragment} from 'react';
import CommentListItem from "../commentListItem";
import {connect} from 'react-redux';
import './CommentList.css';
import API from '../../../utills/API'
import addPostComments from "../../../actions/addPostComments";
import addNewsComments from "../../../actions/addNewsComments";

class CommentList extends Component {


    constructor(props) {
        super(props);

        this.componentDidMount();
    }

    state = {
        text: '',
        pageSize: 2,
        reachedMaxPageSize: false,
        showLoadMoreButton: true,
        currentCommentAmount: 0,
        comments: []
    };


    componentDidMount() {
        const {addPostComments} = this.props;
        const {addNewsComments} = this.props;
        API().get(`/comments/posts/${this.props.post.id}?page=0&size=${this.state.pageSize}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(data => {
            this.isNews ?
                addNewsComments({
                    comments: data.data,
                    postId: this.props.post.id
                })
                : console.log(data.data);
            if (data.data.length > 2) {
                this.setState({showLoadMoreButton: true})
            }

            if (data.data.length < 2) {
                this.setState({showLoadMoreButton: false})
            }
            if (data.data.length < this.state.pageSize) {
                this.setState({reachedMaxPageSize: true});
            }
            this.state.currentCommentAmount = data.data.length;
            // this.setState({comments : data.data})
            addPostComments({
                comments: data.data,
                postId: this.props.post.id
            });
        })
    }


    CommentTextChange(e) {
        this.setState(
            {text: e.target.value}
        );
    }

    LoadMore() {
        const prevCommentAmount = this.state.currentCommentAmount;
        this.state.pageSize = this.state.pageSize + 5;
        this.componentDidMount();
        if (prevCommentAmount === this.state.currentCommentAmount) {
            this.setState({
                showLoadMoreButton: false
            })
        }
    }

    HideComments() {
        this.state.pageSize = 2;
        this.state.reachedMaxPageSize = false;
        this.componentDidMount();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log("post", this.props.post.id, "creator", this.props.user.id, "text", this.state.text);
        console.log("token", this.props.user);
        if (this.state.text !== '') {
            API().post("/comments", {
                post_id: this.props.post.id,
                creator_id: this.props.user.id,
                text: this.state.text
            }, {
                headers: {
                    Authorization: `Bearer_${this.props.user.token}`
                }
            })
                .then(() => {
                    if ((this.state.currentCommentAmount % 5) === 2) {
                        this.state.pageSize = this.state.pageSize + 5;
                    }
                    this.componentDidMount();
                    this.state.text = '';
                })
                .then(function (data) {
                    console.log('Request success: ', data);
                })
                .catch(function (error) {
                    console.log('Request failure: ', error);
                });
        }
    };

    isNews = false;


    render() {
        this.isNews = this.props.isNews;
        return (
            <Fragment>
                <ul className="ul-comments">
                    {console.log("isnews", this.isNews)}

                    {console.log(this.props.post.comments)}
                    {
                        // this.state.comments.map((comment) => {
                        this.props.post.comments.map((comment) => {
                            return (
                                <div key={comment.id}><CommentListItem pageSize={this.state.pageSize}
                                                                       postId={this.props.post.id} comment={comment}
                                                                       isNews={this.isNews}/>
                                </div>
                            )
                        })
                    }
                </ul>
                <div className="d-flex justify-content-center mb-3 loadMoreButtonDiv">
                    {this.state.showLoadMoreButton ? (this.state.reachedMaxPageSize ?
                        <button className="loadMoreButton" onClick={() => this.HideComments()}>Hide</button> :
                        <button className="loadMoreButton" onClick={() => this.LoadMore()}>Load more</button>) : ''
                    }
                </div>


                <div className="container pb-cmnt-container">
                    <div className="row mt-2">
                        <div className="col-10 col-md-8 offset-2 offset-md-3">
                            <div className="card-info">
                                <div className="card-block">
                                    {this.props.user.id !== null ?
                                        <form onSubmit={this.handleSubmit}>
                                            <div className="row">
                                                <div className="col-9">
                                                    <input value={this.state.text}
                                                           onChange={(e) => this.CommentTextChange(e)}
                                                           placeholder="Write your comment here!"
                                                           className="pb-cmnt-textarea"/>
                                                </div>
                                                <div className="col-3 pl-0 d-flex justify-content-start">
                                                    <button className="btn btn-primary float-xs-right shareComment"
                                                            type="submit">Share
                                                    </button>
                                                </div>
                                                <div className=" ">

                                                </div>
                                            </div>
                                        </form>
                                        :
                                        <div className="row">
                                            <div className="col-9">
                                        <textarea value="In order to write comments, please Log In!"
                                                  onChange={(e) => this.CommentTextChange(e)}

                                                  className="pb-cmnt-textarea unlogged"/>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )

    }
}


const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => ({
    addPostComments: async payload => dispatch(addPostComments(payload)),
    addNewsComments: async payload => dispatch(addNewsComments(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentList);


