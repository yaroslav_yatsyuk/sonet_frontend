import React, {Fragment, Component} from 'react';
import './CommentListItem.scss';
import API from "../../../utills/API";
import addPostComments from "../../../actions/addPostComments";
import {connect} from "react-redux";
import addNewsComments from "../../../actions/addNewsComments";
import {Link} from "react-router-dom";

class CommentListItem extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        text: this.props.comment.text,
        editMode: false
    };

    delete = () => {
        API().delete(`/comments/${this.props.comment.id}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(resp => {
            this.renderComments();
        }).catch(error => {
            console.log("error", error);
        });
    };

    renderComments() {
        const {addPostComments} = this.props;
        API().get(`/comments/posts/${this.props.postId}?page=0&size=${this.props.pageSize}`, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        }).then(data => {
            this.props.isNews ?
                addNewsComments({
                    comments: data.data,
                    postId: this.props.postId
                })
                : console.log(data.data);
            addPostComments({
                comments: data.data,
                postId: this.props.postId
            });
        })
    }


    CommentTextChange(e) {
        this.setState(
            {text: e.target.value}
        );
    }

    editMode = () => {
        this.setState({editMode: true})
    };

    handleSubmit = (event) => {
        event.preventDefault();
        API().patch(`/comments/${this.props.comment.id}`, {
            text: this.state.text
        }, {
            headers: {
                Authorization: `Bearer_${this.props.user.token}`
            }
        })
            .then(() => {
                this.setState({editMode: false});
                this.renderComments();
            })
            .then(function (data) {
                console.log('Request success: ', data);
            })
            .catch(function (error) {
                console.log('Request failure: ', error);
            });
    };

    render() {
        return (
            <div className="commentListItemStyle">
                <div className="d-flex justify-content-start ">

                    <div className="commentDiv">
                        <div className="row commentText d-flex flex-row">
                            <div className="col-1 icon-profile-comment">
                                {/*<img src={"https://reason.org/wp-content/uploads/2018/01/guybentley.jpg"}*/}
                                <Link to={`/profileRedirect/${this.props.user.id}`} className="" href="#0">
                                    {this.props.comment.creator.avatar !== null ?
                                        <img src={this.props.comment.creator.avatar.url} alt=""/> :
                                        <img
                                            src="https://ya-webdesign.com/transparent250_/stormtrooper-clipart-step-by-step-6.png"
                                            alt=""/>}
                                </Link>
                            </div>
                            <div className="col-8">
                                <Link to={`/profileRedirect/${this.props.user.id}`} className="" href="#0">
                                    <span className="commentUserName">@{this.props.comment.creator.nickname} </span>
                                </Link>
                                <br/>
                                {this.state.editMode ? <form onSubmit={this.handleSubmit}><input value={this.state.text}
                                                                                                 onChange={(e) => this.CommentTextChange(e)}
                                                                                                 placeholder="Edit your comment."
                                                                                                 className="textareaEditComment "/>
                                    </form> :
                                    <p className="commentTextP">{this.props.comment.text}</p>}
                            </div>
                            <div className="col-2 commentEditDeleteDiv">
                                {this.props.user.id === this.props.comment.creator.id ?
                                    !this.state.editMode ?
                                        <div className="row">
                                            <div className="col-12">
                                                <button onClick={this.editMode} className="commentEditDelete">
                                                    <i className="fa fa-pencil"/> Edit
                                                </button>
                                            </div>
                                            <div className="col-12">
                                                <button onClick={() => {
                                                    this.delete();
                                                }} className="commentEditDelete">
                                                    <i className="fa fa-trash"/> Delete
                                                </button>
                                            </div>
                                        </div> : <button onClick={this.handleSubmit}
                                                         className="btn btn-primary editCommentSaveButton float-xs-right">Save
                                        </button> : <div/>}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => ({
    addPostComments: async payload => dispatch(addPostComments(payload)),
    addNewsComments: async payload => dispatch(addNewsComments(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentListItem);
