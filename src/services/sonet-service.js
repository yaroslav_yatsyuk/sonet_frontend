import SockJS from "sockjs-client";
import Stomp from "stompjs";

class SonetService {

  AuthorizationPrefix = "Bearer_";
  socket;
  stompClient;

  constructor() {
    if(process.env.NODE_ENV === "production") {
      this.BASE_URL = "https://api.sonet-social.net/api/";
    } else {
      this.BASE_URL = process.env.REACT_APP_LOCAL_API_URL;
    }
  }

  socketConnect() {
    this.socket = new SockJS(`${this.BASE_URL}ws`);
    this.stompClient = Stomp.over(this.socket);
    this.stompClient.debug = null;
  }

  socketSubscribe(id, subscribeAction, token) {
    const Authorization = this.AuthorizationPrefix + token;
    const stompClient = this.stompClient;
    stompClient.connect({ Authorization: Authorization }, () => {
      stompClient.subscribe(`/api/topic/${id}`, message => {
        subscribeAction(
          JSON.parse(message.body).content,
          JSON.parse(message.body).sender,
          new Date(JSON.parse(message.body).sendTime),
          JSON.parse(message.body).senderId,
          JSON.parse(message.body).avatar,
          JSON.parse(message.body).id
        );
      });
    });
  }

  socketSend(id, message, senderId) {
    this.stompClient.send(
      `/api/message/${id}`,
      {},
      JSON.stringify({ content: message, senderId })
    );
  }

  socketDisconnect() {
    if (this.socket) {
      this.socket.close();
    }
  }

  getConversations(id, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}users/${id}/conversations`, {
      method: "GET",
      headers: {
        Authorization: Authorization
      }
    })
      .then(response => response.json())
      .then(data => data);
  }

  getFriends(id, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}users/${id}/following`, {
      method: "GET",
      headers: {
        Authorization: Authorization
      }
    })
      .then(response => response.json())
      .then(data => data);
  }

  getMessages(id, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}channels/${id}/messages`, {
      method: "GET",
      headers: {
        Authorization: Authorization
      }
    })
      .then(response => response.json())
      .then(messages => messages);
  }

  getChannels(chatId, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}chats/${chatId}/channels`, {
      method: "GET",
      headers: {
        Authorization: Authorization
      }
    })
      .then(response => response.json())
      .then(channels => channels);
  }

  createChannel(channel, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}channels`, {
      method: "POST",
      headers: {
        Authorization: Authorization,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(channel)
    });
  }

  createConversation(conversation, creatorId, token) {
    const Authorization = this.AuthorizationPrefix + token;
    return fetch(`${this.BASE_URL}chats`, {
      method: "POST",
      headers: {
        Authorization: Authorization,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ ...conversation, creatorId })
    });
  }
}

export default SonetService;
